//
//  ConverseAppUITests.swift
//  converseAppUITests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest

import converse

var testManager: OperationManager?

var SHORT_WAIT: UInt32 = 5 
var LONG_WAIT: UInt32 = 25

/*!
* @warning Make sure in simulator 'Hardware->Keyboard->Connect Hardware keyboard' is disabled
*/
class ConverseAppUITests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
        
        CLogStart()
    }
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        if (testManager == nil) {
            testManager = OperationManager.init()
        }
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("************************** START ******************************")
        print("---------------------------------------------------------------")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("---------------------------------------------------------------")
        print("*************************** END *******************************")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
    }

    // Triggers alertController in NetworkBaseOperation (not fully implemented)
    // Have a look at https://jigsaw.w3.org/HTTP/
    // to access it, the user is "guest" and the password is also "guest".
    func test_X_dataHTTPSBasic() {
        
        let app = XCUIApplication.init()
        tapInCell(identifier: Constants.cellHttpsBasicCellId)
        sleep(SHORT_WAIT)
        
        let authenticationAlert: XCUIElement = app.alerts["Authentication"]
        authenticationAlert.staticTexts["Please enter credentials"].tap()
        
        let collectionViewsQuery: XCUIElementQuery = authenticationAlert.collectionViews;
        let userTextField: XCUIElement = collectionViewsQuery.textFields["Username"];
        let passwordTextField: XCUIElement = collectionViewsQuery.secureTextFields["Password"];
//        let passwordTextField: XCUIElement = collectionViewsQuery.textFields["Password"];
        let submitButton: XCUIElement = app.buttons["Submit"];
        let cancelButton: XCUIElement = app.buttons["Cancel"];
        
        XCTAssertTrue(userTextField.isHittable);
        XCTAssertTrue(passwordTextField.isHittable);
        XCTAssertTrue(submitButton.isHittable);
        XCTAssertTrue(cancelButton.isHittable);
        
        userTextField.tap()
        userTextField.typeText("guest")
        
        passwordTextField.tap()
        passwordTextField.typeText("guest")
        
        submitButton.tap()
    }
    
    func test_X_dataHTTPSBasic_withDefaultProposedCredential() {
        
        let app = XCUIApplication.init()
        tapInCell(identifier: Constants.cellHttpsBasicWithCredentialCellId)
        sleep(SHORT_WAIT)
        
        // UIAlertController should not appear as we are injecting a default working credential
        let authenticationAlert: XCUIElement = app.alerts["Authentication"]
        XCTAssertEqual(authenticationAlert.exists, false);
    }
    
    func test_X_downloadJSON() {
        tapInCell(identifier: Constants.cellDownloadJSONId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadImageWithCache() {
        tapInCell(identifier: Constants.cellDownloadImageWithCacheId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadImageWithoutCache() {
        tapInCell(identifier: Constants.cellDownloadImageWithoutCacheId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadFile() {
        tapInCell(identifier: Constants.cellDownloadFileId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadFileInBg() {
        tapInCell(identifier: Constants.cellDownloadFileInBgId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadFileInBg_sendAppToBg() {
        
        let app = XCUIApplication.init()
        tapInCell(identifier: Constants.cellDownloadFileInBgId)
        sleep(SHORT_WAIT)
        
        XCUIDevice.shared.press(.home)
        sleep(SHORT_WAIT)
        app.activate()
        
        sleep(LONG_WAIT)
        XCTAssert(true);
    }
    
    func test_X_downloadFileToDocuments() {
        tapInCell(identifier: Constants.cellDownloadFileToDocumentsId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_downloadFileToDocumentsInBg() {
        tapInCell(identifier: Constants.cellDownloadFileToDocumentsInBgId)
        sleep(LONG_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_uploadFile() {
        tapInCell(identifier: Constants.cellUploadFileId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_uploadFileInBg() {
        tapInCell(identifier: Constants.cellUploadFileInBgId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_GET() {
        tapInCell(identifier: Constants.cellGETId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_HEAD() {
        tapInCell(identifier: Constants.cellHEADId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_POST() {
        tapInCell(identifier: Constants.cellPOSTId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_PUT() {
        tapInCell(identifier: Constants.cellPUTId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_PATCH() {
        tapInCell(identifier: Constants.cellPATCHId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_DELETE() {
        tapInCell(identifier: Constants.cellDELETEId)
        sleep(SHORT_WAIT)
        checkRequestSucceded()
    }
    
    func test_X_MutExAlert_Single() {
        tapInCell(identifier: Constants.cellMutExAlertSingleId)
        
        // one alert only
        
        let app = XCUIApplication.init()
        let alertButton: XCUIElement = app.alerts.buttons.firstMatch
        
        XCTAssertNotNil(alertButton)
        alertButton.tap()
        
        // no other alert should exist
        let noButton = app.alerts.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }
    
    func test_X_MutExAlert_SerialQueued() {
        tapInCell(identifier: Constants.cellMutExAlertSerialQueuedId)
        
        let app = XCUIApplication.init()
        
        // three alerts serially queued
        
        var alertButton: XCUIElement = app.alerts.buttons.firstMatch
        XCTAssertNotNil(alertButton)
        alertButton.tap()
    
        sleep(2)
        
        alertButton = app.alerts.buttons.firstMatch
        XCTAssertNotNil(alertButton)
        alertButton.tap()
        
        sleep(2)
        
        alertButton = app.alerts.buttons.firstMatch
        XCTAssertNotNil(alertButton)
        alertButton.tap()
        
        sleep(2)
        
        // no other alert should exist
        let noButton = app.alerts.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }
    
    func test_X_MutExAlert_SerialQueued_MutualExclusive() {
        tapInCell(identifier: Constants.cellMutExAlertSerialQueuedAndMutuallyExclusiveId)
        
        let app = XCUIApplication.init()
        
        // three alerts serially queued. Only one is executed, two are discarded
        
        let alertButton: XCUIElement = app.alerts.buttons.firstMatch
        XCTAssertNotNil(alertButton)
        alertButton.tap()
        
        sleep(2)
        
        // no other alert should exist
        let noButton = app.alerts.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }
    
    func test_X_MutExSheet_Single() {
        tapInCell(identifier: Constants.cellMutExSheetSingleId)
        
        // one sheet only
        
        let app = XCUIApplication.init()
        let sheetButton: XCUIElement = app.sheets.buttons.firstMatch
        
        XCTAssertNotNil(sheetButton)
        sheetButton.tap()
        
        // no other sheet should exist
        let noButton = app.sheets.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }
    
    func test_X_MutExSheet_SerialQueued() {
        tapInCell(identifier: Constants.cellMutExSheetSerialQueuedId)
        
        let app = XCUIApplication.init()
        
        // three sheet serially queued
        
        var sheetButton: XCUIElement = app.sheets.buttons.firstMatch
        XCTAssertNotNil(sheetButton)
        sheetButton.tap()
        
        sleep(2)
        
        sheetButton = app.sheets.buttons.firstMatch
        XCTAssertNotNil(sheetButton)
        sheetButton.tap()
        
        sleep(2)
        
        sheetButton = app.sheets.buttons.firstMatch
        XCTAssertNotNil(sheetButton)
        sheetButton.tap()
        
        sleep(2)
        
        // no other sheet should exist
        let noButton = app.sheets.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }
    
    func test_X_MutExSheet_SerialQueued_MutualExclusive() {
        tapInCell(identifier: Constants.cellMutExSheetSerialQueuedAndMutuallyExclusiveId)
        
        let app = XCUIApplication.init()
        
        // three sheets serially queued. Only one is executed, two are discarded
        
        let sheetButton: XCUIElement = app.sheets.buttons.firstMatch
        XCTAssertNotNil(sheetButton)
        sheetButton.tap()
        
        sleep(2)
        
        // no other sheet should exist
        let noButton = app.sheets.buttons.element(boundBy: 1)
        XCTAssertFalse(noButton.exists)
    }

    
    // MARK:- Helper Methods
    
    func tapInCell(identifier: String) {
        
        let app = XCUIApplication.init()
        let masterTable: XCUIElementQuery = app.tables.matching(identifier: Constants.masterTableId)
        let cell: XCUIElement = masterTable.cells.element(matching: .cell, identifier: identifier)
        cell.tap()
    }
    
    func checkRequestSucceded() {
        
        let app = XCUIApplication.init()
        let detailTextView: XCUIElement = app.textViews[Constants.detailTextViewId]
        
        XCTAssertTrue( (detailTextView.value as! String).contains("Status: 2") ) // Status: 2XX
    }
}
