//
//  ConverseAppTests.swift
//  converseAppTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest

@testable import converse

var testManager: OperationManager?

/*!
* @warning Make sure in simulator 'Hardware->Keyboard->Connect Hardware keyboard' is disabled
*/
class ConverseAppTests: XCTestCase {

    var groupOp: GroupBaseOperationPr?
    
    override class func setUp() {
        super.setUp()
        
        CLogStart()
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        if (testManager == nil) {
            testManager = OperationManager.init()
        }
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("************************** START ******************************")
        print("---------------------------------------------------------------")
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("---------------------------------------------------------------")
        print("*************************** END *******************************")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
    }

    //MARK:- URL TESTS
    //MARK:-- Authorization
    
    func test_authorization_sharedCredentialStorage_defaultCredential() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use URLCredentialPersistencePermanent)
        let credential = URLCredential.init(user: "guest", password: "guest", persistence: .forSession)
        
        // 1. Create Protection Space for Server
        let protectionSpace = URLProtectionSpace.init(host: "jigsaw.w3.org", port: 443, protocol: "https", realm: "test", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)

        // 2. Set DEFAULT Credential for Protection Space for the system shared credential storage
        URLCredentialStorage.shared.setDefaultCredential(credential, for: protectionSpace)
    
        // 3. HTTP Basic authorization request
        // <1MB
        groupOp = testManager?.data("https://jigsaw.w3.org/HTTP/Basic/") {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_authorization_sharedCredentialStorage_genericCredential() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use URLCredentialPersistencePermanent)
        let credential = URLCredential.init(user: "guest", password: "guest", persistence: .forSession)
        
        // 1. Create Protection Space for Server
        let protectionSpace = URLProtectionSpace.init(host: "jigsaw.w3.org", port: 443, protocol: "https", realm: "test", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        
        // 2. Set GENERIC Credential for Protection Space for the system shared credential storage
        URLCredentialStorage.shared.set(credential, for: protectionSpace)
        
        // 3. HTTP Basic authorization request
        // <1MB
        groupOp = testManager?.data("https://jigsaw.w3.org/HTTP/Basic/") {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_authorization_sharedCredentialStorage_defaultAndGenericCredential() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use URLCredentialPersistencePermanent)
        let defaultWorkingCredential = URLCredential.init(user: "guest", password: "guest", persistence: .forSession)
        let genericNotWorkingCredential = URLCredential.init(user: "wrongUser", password: "wrongPassword", persistence: .forSession)
        
        // 1. Create Protection Space for Server
        let protectionSpace = URLProtectionSpace.init(host: "jigsaw.w3.org", port: 443, protocol: "https", realm: "test", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        
        // 2. Set DEFAULT and GENERIC Credential for Protection Space for the system shared credential storage
        URLCredentialStorage.shared.setDefaultCredential(defaultWorkingCredential, for: protectionSpace)
        URLCredentialStorage.shared.set(genericNotWorkingCredential, for: protectionSpace)
        
        // 3. HTTP Basic authorization request
        // <1MB
        groupOp = testManager?.data("https://jigsaw.w3.org/HTTP/Basic/") {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
//    func test_authorization_customCredentialStorage_defaultCredential_UNFINISHED() {
//
//        cleanCaches()
//        let expectation = self.expectation(description: #function)
//
//        // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use URLCredentialPersistencePermanent)
//        let credential = URLCredential.init(user: "guest", password: "guest", persistence: .forSession)
//
//        // 1. Create Protection Space for Server
//        let protectionSpace = URLProtectionSpace.init(host: "jigsaw.w3.org", port: 443, protocol: "https", realm: "test", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
//
//        // 2. Create CUSTOM Credential Storage
//        let customCredentialStorage = URLCredentialStorage.init()
//
//        // 3. Set DEFAULT and GENERIC Credential for Protection Space for the system CUSTOM credential storage
//        customCredentialStorage.set(credential, for: protectionSpace)
//        customCredentialStorage.setDefaultCredential(credential, for: protectionSpace)
//
//        // 4. Create Session Configuration
//        let sessionConfiguration = URLSessionConfiguration.default
//
//        // 5. Set Credential Storage for session
//        sessionConfiguration.urlCredentialStorage = customCredentialStorage;
//
//        // 6. Initialize Manager with Session Configuration
//        testManager = OperationManager.initWithDefaultConfiguration(sessionConfiguration, delegate:nil);
//
//        // 3. HTTP Basic authorization request
//        // <1MB
//        groupOp = testManager?.data(withURLString: "https://jigsaw.w3.org/HTTP/Basic/") {[weak self] (result) in
//            XCTAssertNil(result.error, "error should be nil");
//            XCTAssertNotNil(result.data, "data should not be nil");
//            XCTAssertNil(result.responseObj(), "responseObj must be nil");
//            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
//
//            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
//
//            expectation.fulfill()
//        }
//
//        self.waitForExpectations(timeout: 120) {[weak self] (error) in
//            if error != nil {
//                print(error!.localizedDescription)
//            }
//            self?.groupOp?.cancel()
//        }
//    }

    // MARK:-- Download
    
    func test_downloadFile() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: false, progress: nil) {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileWithFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg", inBackground: false, toDocumentsFolder: false, progress: nil) {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileInBackground() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: false, progress: nil) {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
    
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileInBackground_withFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg", inBackground: true, toDocumentsFolder: false, progress: nil) {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileToDocuments() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: true, progress: nil) {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileToDocumentsWithFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg", inBackground: false, toDocumentsFolder: true, progress: nil) {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileToDocumentsInBackground() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: true, progress: nil) {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileToDocumentsInBackgroundWithFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg", inBackground: true, toDocumentsFolder: true, progress: nil) {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- Upload
    
    func test_uploadFile() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", inBackground: false, progress: nil) {[weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFileWithFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/NON_EXISTING_CONVERSE_FOLDER/post", inBackground: false, progress: nil) {[weak self] (result) in // 404
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFileInBackground() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", inBackground: true, progress: nil) {[weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFileInBackground_withFailure() {
        
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
                
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/NON_EXISTING_CONVERSE_FOLDER/post", inBackground: true, progress: nil) {[weak self] (result) in // 404
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            self?.groupOp?.cancel()
        }
    }
    
    
    // MARK:- Performance
    
    func test_performance_cleanCaches() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            cleanCaches()
        }
    }
    
    // MARK:- Helper Methods
    
    func cleanCaches() {
        let cacheManager = CacheManager.init();
        let caches: [Cache] = cacheManager.cache(forType: .CacheTypeALL)
        for cache in caches {
            cache.wipeNonLockedFiles(callback: nil, on: nil)
        }
    }
    
    func areOperationsDeallocated() -> Bool {
        let totalOpsInQueues: Int = testManager?.currentConcurrentOpsInQueues() ?? 0
        //        in completionBlock the last op might still be running
        print("Current operations in queues: \(totalOpsInQueues)")
        return (totalOpsInQueues <= 1)
    }
}
