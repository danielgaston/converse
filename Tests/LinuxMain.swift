import XCTest

import converseTests

var tests = [XCTestCaseEntry]()
tests += converseTests.allTests()
XCTMain(tests)