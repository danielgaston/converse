import XCTest
@testable import converse

final class converseTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(converse().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
