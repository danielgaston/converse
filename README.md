![alt text](https://imgur.com/hfcjHTV.png)

# Converse

[![Build Status](https://gitlab.com/danielgaston/converse/badges/master/pipeline.svg?style=flat)](https://gitlab.com/danielgaston/converse)
[![Version](https://img.shields.io/cocoapods/v/converse.svg?style=flat)](https://cocoapods.org/pods/Converse)
[![License](https://img.shields.io/cocoapods/l/converse.svg?style=flat)](https://cocoapods.org/pods/Converse)
[![Platform](https://img.shields.io/cocoapods/p/converse.svg?style=flat)](https://cocoapods.org/pods/Converse)
[![Coverage](https://gitlab.com/danielgaston/converse/badges/master/coverage.svg?style=flat)](https://gitlab.com/danielgaston/converse)


- [x] 📱 iOS 13.0+

## Requirements

## Installation :inbox_tray:

### CocoaPods
We are indexed on [CocoaPods](http://cocoapods.org), which can be installed using [Ruby gems](https://rubygems.org/):
```shell
$ gem install cocoapods
```
Then simply add `converse` to your `Podfile`.
```
pod 'converse'
```
Lastly let CocoaPods do its thing by running:
```shell
$ pod install
```

## Dependencies :open_file_folder:

Converse has been designed not to directly depend on any particular framework or dependency, however due to its open nature, and to make it ready-to-use, it uses:

| Project | Build | Coverage |
| ------- |:----- |:--------:|
| [SPTPersistentCache](https://github.com/spotify/SPTPersistentCache) | [![Build Status](https://api.travis-ci.org/spotify/SPTPersistentCache.svg)](https://travis-ci.org/spotify/SPTPersistentCache) | [![SPTPersistentCache Coverage](https://codecov.io/github/spotify/SPTPersistentCache/coverage.svg?branch=master)](https://codecov.io/github/spotify/SPTPersistentCache?branch=master) |

## Motivation :book:

The motivation behind creating this library is lighten a [UIViewController](https://developer.apple.com/documentation/uikit/uiviewcontroller) as much as possible by decoupling and isolating tasks that are not intrinsically related with it. 

The library heavily relies on dependency injection (injecting [OperationManager](https://gitlab.com/danielgaston/converse/blob/master/converse/Networking/Manager/OperationManager.swift)), notably reducing the coupling between components, getting rid of singletons or any [UIAppDelegate](https://developer.apple.com/documentation/uikit/uiapplicationdelegate) calls. It’s a simple concept available in pure Swift, so that third-party dependencies are no more required.

Each component logic (e.g. network) is totally isolated from any other component, following both SOLID and Clean Architecture principles. Thus, [UIViewControllers](https://developer.apple.com/documentation/uikit/uiviewcontroller) are only responsible for updating and displaying data, and injecting the operation manager to the next [UIViewController](https://developer.apple.com/documentation/uikit/uiviewcontroller) if using simple MVP architecture; or Interactors instead UIViewControllers in other architectures as VIPER or RIBs. The main concept is that in general terms, and continuing with the network example, network tasks are not accessible any more from the UIViewController, separating clearly the network layer from the UI layer. This comes with a great set of advantages in terms of testing. As the components are more decoupled and modularized, it is easier to test our application, or replace parts of them if necessary.

The library makes extensive use of [Operations](https://developer.apple.com/documentation/foundation/operation) as a wrapper for each particular network tasks (data, upload or download). But do not think it is limited only to networking tasks, because similar operation wrapping occurs in other tasks nature as caching and data conversion, which now they all are defined as a completely independent unit of work (operations).


This operation-based approach is quite helpful because, on the one hand, it defines and creates a repository of *producer operations* that can be easily reused in different projects as *consumer operations*. On the other hand it also facilitates the extension to an infinite number of operations (e.g. storage operations). For that reason this library, **is not a closed component**, I's been created as a **live** component that **comes with a predefined operations**, but **being also open to be extended**.

Let's drill down on how it works.

Each described operation is added to the corresponding [OperationQueue](https://developer.apple.com/documentation/foundation/operationqueue) to be processed. Additionally, each operation has its own [priority](https://developer.apple.com/documentation/foundation/operation/queuepriority) so that it can be reordered in the queue while it is being waiting for the turn to be executed.

Additionally, not only operations have priority, queues have it as well defined as [QoS](https://developer.apple.com/documentation/foundation/operationqueue/1417919-qualityofservice)). QoS helps to manage and assign the available iOS resources to each particular queue, depending on the task nature it holds (e.g. high priority networking tasks, low priority networking tasks, generic tasks...).

These operation & queue design gets complemented for the case of network operations, by an internal network module architecture which allows complex requests to be created in a very flexible way.

The main benefit of having everything wrapped around operations is, firstly, the flexibility and power we get from them. By adding dependencies between operations it is possible to control its executing order and race conditions.
Secondly, operations provide a lot of execution information from themselves and from the queue where they are executed. For the case of network operations and as an example, they not only report connection status or connection speed, but also real time concurrent and maximum queue operations.

All this feedback is used as queue input for dynamically modifying the queue behaviour. In other words, this makes possible to reduce the number of concurrent operations so as not to saturate the available network bandwidth with additional requests; cancel requests halfway, suspend them, or simply stop all network interactions to be further resumed. And this can be applied not only on networking operations, but on any other type of operation you can imagine.

I hope you have enjoyed as much as I've done developing this personal project.

## Usage :eyes:

You can use Converse as it is ([Basic usage](#basic-usage)), or you can extend it to fulfil your project's requirements ([Advanced usage](#advanced-usage)).

### Basic Usage

The framework provides the basic operations to fetch data, cache it, parse it, and convert it to any appropriate format. Each operation acts as an independent working unit and could be accessed individually, however the most recommendable way to work with them is via [OperationManager](https://gitlab.com/danielgaston/converse/blob/master/converse/Networking/Manager/OperationManager.swift).

We can instantiate and hold an [OperationManager](https://gitlab.com/danielgaston/converse/blob/master/converse/Networking/Manager/OperationManager.swift). A recommended way to do it is via dependency injection on each ViewController. AppDelegate may instantiate it and pass it to the first ViewController, which in turn injects it to a second one and so on. Additionally we can also hold the **groupOp** in case we want to cancel in cascade all the operations.

Following, a few simplified examples about how to access the API.

#### GET
```swift
// manager instantiation
let manager = OperationManager.init()

// GET request
testManager.GET("https://jsonplaceholder.typicode.com/posts", parameters: nil, completion: {[weak self] (result) in

})
```

#### POST
```swift
// manager instantiation
let manager = OperationManager.init()

// parameters object creation
var params = HTTPRequestParameters.init()
params.add(key: "title", value: "foo")
params.add(key: "body", value: "bar")
params.add(key: "userId", value: "1")

// POST request
testManager.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, completion: {[weak self] (result) in
   
})
```

#### Download JSON
```swift
// manager instantiation
let manager = OperationManager.init()

// JSON download
testManager?.downloadJSON("https://api.ipify.org?format=json", useCache: false, progress: nil, completion: { [weak self] (result) in

})
```

#### Download Image
```swift
// manager instantiation
let manager = OperationManager.init()

// image download
groupOp = testManager.downloadImage("https://en.wikipedia.org/wiki/Lenna#/media/File:Lenna_(test_image).png", useCache: true, progress: nil, completion: { [weak self] (result) in

})
```

#### Download File
```swift
// manager instantiation
let manager = OperationManager.init()

// file download
testManager.downloadFile("http://www.test.com/myFile.pdf", inBackground: false, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in

})
```

#### Upload File
```swift
// manager instantiation
let manager = OperationManager.init()

// file upload
testManager.upload(fileURL: "URL_TO_FILE", toURLString: "URL_TO_UPLOAD", progress: nil, completion: {[weak self] (result) in

})
```

Please check 'OperationManager' to see the complete list of methods.

### Advanced Usage

In case the provided set of methods in [OperationManager](https://gitlab.com/danielgaston/converse/blob/master/converse/Networking/Manager/OperationManager.swift) are not enough for your project's requirements, or if you simply want to include additional functionalities, the framework is prepared to be extended.

At this point it is important to understand that Converse has been designed around the idea of having multiple reusable independent Operations. Each operation (e.g. fetch data, cache data, parse JSON ...) belongs to a set of _'producer'_ operations. In a few words, what the [OperationManager](https://gitlab.com/danielgaston/converse/blob/master/converse/Networking/Manager/OperationManager.swift) does is convert those _'producer'_ operations into _'consumer'_ operations by picking and connecting the necessary ones in order to get the desired functionality.

TODO

## Contributing :mailbox_with_mail:
Contributions are welcomed, have a look at the [CONTRIBUTING.md](CONTRIBUTING.md) document for more information.

## Author 🚶

Daniel Gastón, daniel.gaston.iglesias@gmail.com

## License :memo:

Converse is available under the MIT license. See the LICENSE file for more info.
