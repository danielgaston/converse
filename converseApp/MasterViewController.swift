//
//  MasterViewController.swift
//  converseApp
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit
import converse

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var operationManager: OperationManager = OperationManager.init()
    var mutexAlertManager: AlertManager = AlertManager.init()
    var objects = [MasterDataSource]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initializePreProcessing()
        initializeDS_HTTPSBasic()
        initializeDS_HTTPSBasic_DefaultCredential()
        initializeDS_DownloadJSON()
        initializeDS_DownloadImageWithCache()
        initializeDS_DownloadImageWithoutCache()
        initializeDS_DownloadFile()
        initializeDS_DownloadFileInBg()
        initializeDS_DownloadFileToDocuments()
        initializeDS_DownloadFileToDocumentsInBg()
        initializeDS_UploadFile()
        initializeDS_UploadFileInBg()
        initializeDS_GET()
        initializeDS_HEAD()
        initializeDS_POST()
        initializeDS_PUT()
        initializeDS_PATCH()
        initializeDS_DELETE()
        initializeDS_MUTEX_ALERT()
        initializeDS_MUTEX_ALERTS()
        initializeDS_MUTEX_ALERTS_SINGLE()
        initializeDS_MUTEX_SHEET()
        initializeDS_MUTEX_SHEETS()
        initializeDS_MUTEX_SHEETS_SINGLE()
        
        tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }


    // MARK: - Initialization Methods
    
    func initializePreProcessing() {
        title = "Converse Tests"
        tableView.accessibilityIdentifier = Constants.masterTableId
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }
    
    func initializeDS_HTTPSBasic() {
        let ds = MasterDataSource.init("HTTPS Basic", Constants.cellHttpsBasicCellId) { [weak self] in
            
            self?.operationManager.data("https://jigsaw.w3.org/HTTP/Basic/", progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_HTTPSBasic_DefaultCredential() {
        //  user:guest password:guest
        let ds = MasterDataSource.init("HTTPS Basic Default Credential", Constants.cellHttpsBasicWithCredentialCellId) { [weak self] in
            
            self?.resetToDefault()
            
            // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use URLCredentialPersistencePermanent)
            let credential = URLCredential.init(user: "guest", password: "guest", persistence: .forSession)
            
            // 1. Create Protection Space for Server
            let protectionSpace = URLProtectionSpace.init(host: "jigsaw.w3.org", port: 443, protocol: "https", realm: "test", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
            
            // 2. Set DEFAULT Credential for Protection Space for the system shared credential storage
            URLCredentialStorage.shared.setDefaultCredential(credential, for: protectionSpace)
            
            // 3. Request
            self?.operationManager.data("https://jigsaw.w3.org/HTTP/Basic/", progress: { [weak self] (progress) in
            self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadJSON() {
        let ds = MasterDataSource.init("Download JSON", Constants.cellDownloadJSONId) { [weak self] in
            
            self?.operationManager.downloadJSON("https://api.ipify.org?format=json", useCache: false, progress: { [weak self] (progress) in
                    self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadImageWithCache() {
        let ds = MasterDataSource.init("Download Image (cache)", Constants.cellDownloadImageWithCacheId) { [weak self] in
            
            self?.operationManager.downloadDataImage("https://images3.alphacoders.com/847/847928.jpg", useCache: true, progress: { [weak self] (progress) in
                    self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadImageWithoutCache() {
        let ds = MasterDataSource.init("Download Image (no cache)", Constants.cellDownloadImageWithoutCacheId) { [weak self] in
            
            self?.operationManager.downloadDataImage("https://images3.alphacoders.com/847/847928.jpg", useCache: false, progress: { [weak self] (progress) in
                    self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadFile() {
        let ds = MasterDataSource.init("Download File", Constants.cellDownloadFileId) { [weak self] in
            
            self?.operationManager.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: false, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadFileInBg() {
        let ds = MasterDataSource.init("Download File in Bg", Constants.cellDownloadFileInBgId) { [weak self] in
            
            self?.operationManager.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: false, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadFileToDocuments() {
        let ds = MasterDataSource.init("Download File to /Docs", Constants.cellDownloadFileToDocumentsId) { [weak self] in
            
            self?.operationManager.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: true, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DownloadFileToDocumentsInBg() {
        let ds = MasterDataSource.init("Download File to /Docs in Bg", Constants.cellDownloadFileToDocumentsInBgId) { [weak self] in
            
            self?.operationManager.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: true, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: { [weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_UploadFile() {
        let ds = MasterDataSource.init("Upload File", Constants.cellUploadFileId) { [weak self] in
            
            guard let fileURL = Bundle.init(for: type(of: self!)).url(forResource: "converse_test", withExtension: "txt") else {
                return
            }
            
            self?.operationManager.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", inBackground: false, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_UploadFileInBg() {
        let ds = MasterDataSource.init("Upload File in Bg", Constants.cellUploadFileInBgId) { [weak self] in
            
            guard let fileURL = Bundle.init(for: type(of: self!)).url(forResource: "converse_test", withExtension: "txt") else {
                return
            }
            
            self?.operationManager.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", inBackground: true, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_GET() {
        let ds = MasterDataSource.init("GET", Constants.cellGETId) { [weak self] in
            
            self?.operationManager.GET("https://jsonplaceholder.typicode.com/posts", parameters: nil, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_HEAD() {
        let ds = MasterDataSource.init("HEAD", Constants.cellHEADId) { [weak self] in
            
            self?.operationManager.HEAD("https://jsonplaceholder.typicode.com/posts", parameters: nil, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_POST() {
        let ds = MasterDataSource.init("POST", Constants.cellPOSTId) { [weak self] in
            
            var params = NetworkRequestParameters.init()
            params.add(key: "title", value: "foo")
            params.add(key: "body", value: "bar")
            params.add(key: "userId", value: "1")
            
            self?.operationManager.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_PUT() {
        let ds = MasterDataSource.init("PUT", Constants.cellPUTId) { [weak self] in
            
            var params = NetworkRequestParameters.init()
            params.add(key: "id", value: "1")
            params.add(key: "title", value: "foo")
            params.add(key: "body", value: "bar")
            params.add(key: "userId", value: "1")
            
            self?.operationManager.PUT("https://jsonplaceholder.typicode.com/posts/1", parameters: params, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_PATCH() {
        let ds = MasterDataSource.init("PATCH", Constants.cellPATCHId) { [weak self] in
            
            var params = NetworkRequestParameters.init()
            params.add(key: "title", value: "foo")
            
            self?.operationManager.PATCH("https://jsonplaceholder.typicode.com/posts/1", parameters: params, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    func initializeDS_DELETE() {
        let ds = MasterDataSource.init("DELETE", Constants.cellDELETEId) { [weak self] in
            
            var params = NetworkRequestParameters.init()
            params.add(key: "title", value: "foo")
            
            self?.operationManager.DELETE("https://jsonplaceholder.typicode.com/posts/1", parameters: params, progress: { [weak self] (progress) in
                self?.progressProcessing(progress: progress)
                }, completion: {[weak self] (result) in
                    self?.completionProcessing(result: result)
            })
        }
        
        objects.append(ds)
    }
    
    // MARK: - Mutual Exclusive
    
    func initializeDS_MUTEX_ALERT() {
        let ds = MasterDataSource.init("MutEx Alert", Constants.cellMutExAlertSingleId) { [weak self] in
            
            let title = "Single Alert"
            let message = "Single Alert Op is queued and executed"
            
            var mutExAlertData = MutExAlertData.init(title: title, message: message)
            mutExAlertData.addButton(withTitle: "Close Alert", handler: {
                
            })
            
            self?.mutexAlertManager.alert(withData: mutExAlertData)
        }
        
        objects.append(ds)
    }
    
    func initializeDS_MUTEX_ALERTS() {
        let ds = MasterDataSource.init("MutEx Alerts", Constants.cellMutExAlertSerialQueuedId) { [weak self] in
            
            var mutexAlertDatas = [MutExAlertData]()
            let title = "Serial Alert"
            let message = "3 Alert Ops are queued and executed serially"
            
            var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
            mutexAlertData1.addButton(withTitle: "Finish 1st Alert Op, 2nd & 3rd are pending", handler: {
                
            })
            var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
            mutexAlertData2.addButton(withTitle: "Finish 2nd Alert Op, 3rd is pending", handler: {
                
            })
            var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
            mutexAlertData3.addButton(withTitle: "Finish 3rd Alert Op", handler: {
                
            })
            
            mutexAlertDatas.append(mutexAlertData1)
            mutexAlertDatas.append(mutexAlertData2)
            mutexAlertDatas.append(mutexAlertData3)
            
            self?.mutexAlertManager.alerts(withData: mutexAlertDatas)
        }
        
        objects.append(ds)
    }
    
    func initializeDS_MUTEX_ALERTS_SINGLE() {
        let ds = MasterDataSource.init("MutEx Alerts Single", Constants.cellMutExAlertSerialQueuedAndMutuallyExclusiveId) { [weak self] in
            
            let title = "Serial Mutual Exclusive Alert"
            let message = "3 MutEx Alert Ops are queued, but since one is being executed, the rest are discarded"
            
            var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
            mutexAlertData1.addButton(withTitle: "Finish 1st Alert Op, 2nd & 3rd have been discarded", handler: {
                
            })
            var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
            mutexAlertData2.addButton(withTitle: "it will never be executed", handler: {
                
            })
            var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
            mutexAlertData3.addButton(withTitle: "it will never be executed", handler: {
                
            })
            
            self?.mutexAlertManager.alertSingleInstance(withData: mutexAlertData1)
            self?.mutexAlertManager.alertSingleInstance(withData: mutexAlertData2)
            self?.mutexAlertManager.alertSingleInstance(withData: mutexAlertData3)
        }
        
        objects.append(ds)
    }
    
    func initializeDS_MUTEX_SHEET() {
        let ds = MasterDataSource.init("MutEx Sheet", Constants.cellMutExSheetSingleId) { [weak self] in
            
            let title = "Single Sheet"
            let message = "Single Sheet Op is queued and executed"
            var mutExAlertData = MutExAlertData.init(title: title, message: message)
            
            mutExAlertData.addButton(withTitle: "Close Sheet", handler: {
                
            })
            
            self?.mutexAlertManager.sheet(withData: mutExAlertData)
        }
        
        objects.append(ds)
    }
    
    func initializeDS_MUTEX_SHEETS() {
        let ds = MasterDataSource.init("MutEx Sheets", Constants.cellMutExSheetSerialQueuedId) { [weak self] in
            
            var mutexAlertDatas = [MutExAlertData]()
            let title = "Serial Sheet"
            let message = "3 Sheet Ops are queued and executed serially"
            
            var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
            mutexAlertData1.addButton(withTitle: "Finish 1st Sheet Op, 2nd & 3rd are pending", handler: {
                
            })
            var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
            mutexAlertData2.addButton(withTitle: "Finish 2nd Sheet Op, 3rd is pending", handler: {
                
            })
            var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
            mutexAlertData3.addButton(withTitle: "Finish 3rd Sheet Op", handler: {
                
            })
            
            mutexAlertDatas.append(mutexAlertData1)
            mutexAlertDatas.append(mutexAlertData2)
            mutexAlertDatas.append(mutexAlertData3)
            
            self?.mutexAlertManager.sheets(withData: mutexAlertDatas)
        }
        
        objects.append(ds)
    }
    
    func initializeDS_MUTEX_SHEETS_SINGLE() {
        let ds = MasterDataSource.init("MutEx Sheets single", Constants.cellMutExSheetSerialQueuedAndMutuallyExclusiveId) { [weak self] in
            
            let title = "Serial Mutual Exclusive Sheet"
            let message = "3 MutEx Sheet Ops are queued, but since one is being executed, the rest are discarded"
            
            var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
            mutexAlertData1.addButton(withTitle: "Finish 1st Sheet Op, 2nd & 3rd have been discarded", handler: {
                
            })
            var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
            mutexAlertData2.addButton(withTitle: "it will never be executed", handler: {
                
            })
            var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
            mutexAlertData3.addButton(withTitle: "it will never be executed", handler: {
                
            })
            
            self?.mutexAlertManager.sheetSingleInstance(withData: mutexAlertData1)
            self?.mutexAlertManager.sheetSingleInstance(withData: mutexAlertData2)
            self?.mutexAlertManager.sheetSingleInstance(withData: mutexAlertData3)
        }
        
        objects.append(ds)
    }

    
    // MARK: - Helper Methods
    
    func resetToDefault() {
        URLCache.shared.removeAllCachedResponses()
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
    
        // Removes all Credentials from the sharedCredentialStorage
        let allCredentials: [URLProtectionSpace : [String : URLCredential]] = URLCredentialStorage.shared.allCredentials
        
        if allCredentials.count != 0 {
            var protectionSpaceSequence = allCredentials.keys.enumerated().makeIterator()
            
            while let protectionSpace = protectionSpaceSequence.next()?.element {
                
                var userNameSequence = allCredentials[protectionSpace]?.keys.makeIterator()
    
                while let userName = userNameSequence?.next() {
                    
                    let credential = allCredentials[protectionSpace]![userName]
                    if credential != nil {
                        URLCredentialStorage.shared.remove(credential!, for: protectionSpace)
                    }
                }
            }
        }
    }
    
    func progressProcessing(progress: Progress) {
        self.detailViewController?.progressLabel?.text = progress.localizedDescription
        self.detailViewController?.additionalProgressLabel?.text = progress.localizedAdditionalDescription
        self.detailViewController?.progressView?.setProgress(Float(progress.fractionCompleted), animated: true)
    }
    
    func completionProcessing<OR: OperationResult>(result: OR) {
        
        let extraInfoNetworkResult = result.extraInfo?[NetworkOperationResultExtraInfo.key()] as? NetworkOperationResultExtraInfo
        
        if result.error != nil {
            detailViewController?.descriptionTextView?.textColor = UIColor.init(red: 0.44, green: 0.1, blue: 0.01, alpha: 1)
            detailViewController?.descriptionTextView?.text = result.error!.localizedDescription
        } else {
            detailViewController?.descriptionTextView?.textColor = UIColor.init(red: 0.01, green: 0.44, blue: 0.01, alpha: 1)
            
            guard let httpURLResponse = extraInfoNetworkResult?.responseURL as? HTTPURLResponse else {
                detailViewController?.descriptionTextView?.text = "Status: Unknown \n\n " + (extraInfoNetworkResult?.responseURL?.url?.absoluteString ?? "")
                return
            }
            detailViewController?.descriptionTextView?.text = "Status: \(httpURLResponse.statusCode) \n\n " + httpURLResponse.debugDescription
        }
    }


    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let ds = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = ds
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        }
    }

    // MARK: - UITableView & UITableViewDataSource Delegate Methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let ds = objects[indexPath.row]
        cell.textLabel!.text = ds.name
        cell.accessibilityIdentifier = ds.accessId
        return cell
    }
}

