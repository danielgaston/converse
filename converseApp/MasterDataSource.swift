//
//  MasterDataSource.swift
//  converseApp
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct MasterDataSource {
    var name: String
    var accessId: String?
    var completionBlock: (() -> Void)
    
    init(_ name: String, _ accessId: String?, completionBlock: @escaping (() -> Void)) {
        self.name = name
        self.accessId = accessId
        self.completionBlock = completionBlock
    }
}
