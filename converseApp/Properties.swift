//
//  Properties.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public enum Constants {
    static let masterTableId = "MainTableId"
    static let detailTextViewId = "DetailTextViewId"
    
    static let cellHttpsBasicCellId = "CellHttpsBasicId"
    static let cellHttpsBasicWithCredentialCellId = "CellHttpsBasicWithCredentialId"
    static let cellDownloadJSONId = "CellDownloadJSONId"
    static let cellDownloadImageWithCacheId = "CellDownloadImageWithCacheId"
    static let cellDownloadImageWithoutCacheId = "CellDownloadImageWithoutCacheId"
    static let cellDownloadFileId = "CellDownloadFileId"
    static let cellDownloadFileInBgId = "CellDownloadFileInBgId"
    static let cellDownloadFileToDocumentsId = "CellDownloadFileToDocumentsId"
    static let cellDownloadFileToDocumentsInBgId = "CellDownloadFileToDocumentsInBgId"
    static let cellUploadFileId = "CellUploadFileId"
    static let cellUploadFileInBgId = "CellUploadFileInBgId"
    static let cellGETId = "CellGETId"
    static let cellHEADId = "CellHEADId"
    static let cellPOSTId = "CellPOSTId"
    static let cellPUTId = "CellPUTId"
    static let cellPATCHId = "CellPATCHId"
    static let cellDELETEId = "CellDELETEId"
    static let cellMutExAlertSingleId = "CellMutExAlertSingleId"
    static let cellMutExAlertSerialQueuedId = "CellMutExAlertSerialQueuedId"
    static let cellMutExAlertSerialQueuedAndMutuallyExclusiveId = "CellMutExAlertSerialQueuedAndMutuallyExclusiveId"
    static let cellMutExSheetSingleId = "CellMutExSheetSingleId"
    static let cellMutExSheetSerialQueuedId = "CellMutExSheetSerialQueuedId"
    static let cellMutExSheetSerialQueuedAndMutuallyExclusiveId = "CellMutExSheetSerialQueuedAndMutuallyExclusiveId"
}
