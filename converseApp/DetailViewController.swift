//
//  DetailViewController.swift
//  converseApp
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var detailItem: MasterDataSource? = nil {
        didSet {
            self.configureView()
        }
    }
    
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var additionalProgressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        descriptionTextView?.accessibilityIdentifier = Constants.detailTextViewId
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            title = detail.name
            progressLabel?.text = ""
            additionalProgressLabel?.text = ""
            progressView?.setProgress(0, animated: false)
            descriptionTextView?.text = "Waiting Response..."
            descriptionTextView?.textColor = UIColor.darkText
            detail.completionBlock()
        }
    }
}

