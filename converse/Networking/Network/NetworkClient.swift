//
//  NetworkClient.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import Foundation

extension URLRequest {
    static func urlRequest(withRequest request: NetworkRequest) throws -> URLRequest {
        return try request.urlRequest()
    }
}


// MARK: -

extension URLSession {
    
    func operationDataTask(request: NetworkDataRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) throws -> URLSessionDataTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = dataTask(with: urlRequest, completionHandler: completionHandler)
        return task
    }
    
    func operationDownloadTask(request: NetworkDownloadRequest, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) throws -> URLSessionDownloadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = downloadTask(with: urlRequest, completionHandler: completionHandler)
        return task
    }
    
    func operationUploadTaskFromFileURL(request: NetworkUploadRequest,
    completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) throws -> URLSessionUploadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        guard let fileURL = request.uploadFileURL else {
            throw NetworkRequestError.emptyUploadFileURL
        }
        let task = uploadTask(with: urlRequest, fromFile: fileURL, completionHandler: completionHandler)
        return task
    }
    
    func operationUploadTaskFromData(request: NetworkUploadRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) throws -> URLSessionUploadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        guard let data = request.uploadData else {
            throw NetworkRequestError.emptyUploadData
        }
        let task = uploadTask(with: urlRequest, from: data, completionHandler: completionHandler)
        return task
    }
}


// MARK: -

extension URLSession {
    
    func operationDataTask(request: NetworkDataRequest) throws -> URLSessionDataTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = dataTask(with: urlRequest)
        return task
    }
    
    func operationDownloadTask(request: NetworkDownloadRequest) throws -> URLSessionDownloadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = downloadTask(with: urlRequest)
        return task
    }
    
    func operationUploadTask(request: NetworkUploadRequest, fileURL: URL) throws -> URLSessionUploadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = uploadTask(with: urlRequest, fromFile: fileURL)
        return task
    }
    
    func operationUploadTask(request: NetworkUploadRequest, bodyData: Data) throws -> URLSessionUploadTask {
        let urlRequest = try URLRequest.urlRequest(withRequest: request)
        let task = uploadTask(with: urlRequest, from: bodyData)
        return task
    }
}
