//
//  NetworkRequestParameter.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

// MARK:-

public protocol CodableParameter: Codable {

}

public protocol RequestParameters: Codable {
    func toDict() -> [String : String]
    mutating func add(key: String, value: String)
    mutating func remove(key: String)
}

// MARK:-

public struct NetworkRequestParameter: CodableParameter {
    
    var key: String
    var value: String
    
    init(key: String, value: String) {
        self.key = key
        self.value = value
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)
        try values.encode(key, forKey: .key)
        try values.encode(value, forKey: .value)
    }
    
    public init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decode(String.self, forKey: .key)
        value = try values.decode(String.self, forKey: .value)
    }
    
    enum CodingKeys: String, CodingKey {
        case key = "key"
        case value = "value"
    }
}


// MARK:-

public struct NetworkRequestParameters: CodableParameter, RequestParameters {
    
    var params = [NetworkRequestParameter]()
    
    /// Create an empty instance.
    public init() { }
    
    init(withParameters parameters:[NetworkRequestParameter]) {
        self.init()
        params.removeAll()
        params = parameters
    }
    
    // MARK: RequestParameters Protocol Methods
    
    public mutating func add(key: String, value: String) {
        params.append(NetworkRequestParameter(key: key, value: value))
    }
    
    public mutating func remove(key: String) {
        if let index = params.firstIndex(where: { $0.key == key }) {
            params.remove(at: index)
        }
    }
    
    public func toDict() -> [String : String] {
        var dict = [String : String]()
        for parameter in params {
            dict[parameter.key] = parameter.value
        }
        return dict
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)
        try values.encode(params.map{ $0 }, forKey: CodingKeys.params)
    }
    
    public init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        params = try values.decode([NetworkRequestParameter].self, forKey: CodingKeys.params).map { $0 }
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
    }
}
