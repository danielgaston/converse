//
//  NetworkResponse.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public protocol NetworkResponse: AnyObject {
    
    var task: URLSessionTask { get }
    var response: URLResponse { get }
    var data: Data? { get }
    var error: Error? { get }
    var location: URL? { get }
    var serializer: ResponseSerialization { get }
    
    init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?)
}

open class AnyNetworkResponse: NetworkResponse {
    public var task: URLSessionTask
    public var serializer: ResponseSerialization
    
    public var response: URLResponse
    public var data: Data?
    public var error: Error?
    public var location: URL?
    
    fileprivate init() {
         fatalError("We should not never reach this initializer. Please check implementation")
    }
    
    public required convenience init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        self.init(withTask: _task, response: _response, data: _data, serializer: HTTPResponseSerializer.init(), error: _error)
    }
    
    init<TaskType: URLSessionTask, SerializerType: ResponseSerialization>(withTask _task: TaskType, response _response: URLResponse, data _data: Data?, serializer _serializer: SerializerType, error _error: Error?) {
        task = _task
        response = _response
        data = _data
        serializer = _serializer
        error = _error
    }
}

// MARK:-
// MARK: COMPOSED RESPONSES

open class DataNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        
        super.init(withTask: _task, response: _response, data: _data, serializer: HTTPResponseSerializer.init(), error: _error)
    }
}

// MARK:-

open class JSONDataNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        
        super.init(withTask: _task, response: _response, data: _data, serializer: JSONResponseSerializer.init(), error: _error)
    }
}

// MARK:-

open class UploadNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        
        super.init(withTask: _task, response: _response, data: _data, serializer: HTTPResponseSerializer.init(), error: _error)
    }
}


// MARK:-

open class DownloadNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        super.init(withTask: _task, response: _response, data: _data, serializer: HTTPResponseSerializer.init(), error: _error)
    }
}

// MARK:-

open class ImageDataNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        super.init(withTask: _task, response: _response, data: _data, serializer: ImageResponseSerializer.init(), error: _error)
    }
}

// MARK:-

open class ImageDownloadNetworkResponse: AnyNetworkResponse {
    
    public required init(withTask _task: URLSessionTask, response _response: URLResponse, data _data: Data?, error _error: Error?) {
        
        let responseSerializer = ImageResponseSerializer.init()
        // missing image-download content-types are added (i.e "application/png")
        let acceptableContentTypes = NSMutableSet.init(set: responseSerializer.acceptableContentTypes!)
        acceptableContentTypes.add("application/png")
        acceptableContentTypes.add("application/jpg")
        acceptableContentTypes.add("application/jpeg")
        acceptableContentTypes.add("application/gif")
        
        responseSerializer.acceptableContentTypes = acceptableContentTypes as? Set<String>
        
        super.init(withTask: _task, response: _response, data: _data, serializer: responseSerializer, error: _error)
    }
}


