//
//  NetworkRequest.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import Foundation

public protocol NetworkRequest: AnyObject {
    
    var absoluteURL: URL? { get }
    var baseURL: URL? { get }
    var method: NetworkMethod { get }
    var path: String? { get }
    var serializer: RequestSerialization { get }
    var responseType: NetworkResponse.Type { get }
    var body: ((RequestMultipartFormData) -> Void)? { get }
    var httpBody: Data? { get set }
    var parameters: RequestParameters? { get }
    var headers: [String : String] { get set }
    
    /// Swift: it's not possible (yet) to throw from computed properties in Swift 5.
    func urlRequest() throws -> URLRequest
    
    /// Swift: it avoids using 'Equatable' protocol and the 'has Self or associated type' protocol error
    func isEqualTo(_ other: NetworkRequest) -> Bool
}

public protocol NetworkDataRequest: NetworkRequest {}

public protocol NetworkUploadRequest: NetworkDataRequest {
    
    var uploadFileURL: URL? {get}
    var uploadData: Data? {get}
    var mode: UploadMode {get}
}

public protocol NetworkDownloadRequest: NetworkRequest {}

// MARK:-

/// Extension Methods avalailable to all types that conforms the protocol
open class AnyNetworkRequest: NetworkRequest {
    
    // MARK: NetworkRequest Pr
    
    public var absoluteURL: URL?
    public var baseURL: URL? {
        return absoluteURL?.baseURL
    }
    public var method: NetworkMethod = .get
    public var path: String? {
        return absoluteURL?.path
    }
    public var serializer: RequestSerialization
    public var responseType: NetworkResponse.Type
    public var body: ((RequestMultipartFormData) -> Void)?
    public var httpBody: Data?
    public fileprivate(set) var parameters: RequestParameters?
    public var headers: [String : String]
    
    /// Swift: it's not possible (yet) to throw from computed properties in Swift 5.
    public func urlRequest() throws -> URLRequest {
        return try buildURLRequest()
    }
    
    // MARK: Initializers
    
    fileprivate init() {
        serializer = HTTPRequestSerializer.init()
        responseType = DataNetworkResponse.self
        headers = [String : String]()
    }
    
    // MARK: Helper Methods
    
    open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
        urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                                                                 urlString: url.absoluteString,
                                                                                 parameters: parameters?.toDict(),
                                                                                 error: &error) as URLRequest
        
        guard error == nil else {
            throw error!
        }
        
        return urlRequest!
    }
  
    public func isEqualTo(_ other: NetworkRequest) -> Bool {
        
        guard type(of: self) == type(of: other) else {
            return false
        }
        
        let isSameURL = self.absoluteURL == other.absoluteURL
        let isSameMethod = self.method == other.method
        let isSameSerializer = type(of: self.serializer) == type(of: other.serializer)
        let isSameResponseType = self.responseType == other.responseType
        // FIXME: ⚠️ consider body and parameters for request equality
        let isSameBody = true /* self.body == other.body */
        let isSameHttpBody = true /*self.httpBody == other.httpBody*/
        let isSameParameters = true /* self.parameters == other.parameters */
        let isSameHeaders = true /*self.headers == other.headers*/
        
        return isSameURL && isSameMethod && isSameSerializer && isSameResponseType && isSameBody && isSameHttpBody && isSameParameters && isSameHeaders
    }
}

// MARK: -

open class DataNetworkRequest: AnyNetworkRequest, NetworkDataRequest {
    
    public convenience init(withURLString _URLString: String, method _method: NetworkMethod = .get) {
        self.init(withURLString: _URLString, method: _method, parameters: nil)
    }
    
    public convenience init(withURLString _URLString: String, method _method: NetworkMethod = .get, parameters _parameters: RequestParameters?) {
        self.init(withURLString: _URLString, method: _method, parameters: _parameters, body: nil)
    }
    
    public init(withURLString _URLString: String, method _method: NetworkMethod = .get, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)?) {
        
        super.init()
        serializer = HTTPRequestSerializer.init()
        responseType = DataNetworkResponse.self
        absoluteURL = URL.init(string: _URLString)
        method = _method
        body = _body
        parameters = _parameters
    }
    
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
        
        if let body = body {
            urlRequest = (serializer as! HTTPRequestSerializer).multipartFormRequest(withMethod: method.rawValue,
                                                                 urlString: url.absoluteString,
                                                                parameters: parameters?.toDict(),
                                                      constructingBodyWith: body,
                                                      error: &error) as URLRequest
        } else {
            urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                                    urlString: url.absoluteString,
                                                   parameters: parameters?.toDict(),
                                                   error: &error) as URLRequest
        }
    
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }

        return urlRequest!
    }
}

// MARK: -

open class JSONDataNetworkRequest: DataNetworkRequest {
    
    override public init(withURLString _URLString: String, method _method: NetworkMethod = .get, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)?) {

        super.init(withURLString: _URLString, method: _method, parameters: _parameters, body: _body)
        
        serializer = JSONRequestSerializer.init()
        responseType = JSONDataNetworkResponse.self
    }
    
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! JSONRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
        
        if let body = body {
            urlRequest = (serializer as! JSONRequestSerializer).multipartFormRequest(withMethod: method.rawValue,
                                                                                                  urlString: url.absoluteString,
                                                                                                  parameters: parameters?.toDict(),
                                                                                                  constructingBodyWith: body,
                                                                                                  error: &error) as URLRequest
        } else {
            urlRequest = (serializer as! JSONRequestSerializer).request(withMethod: method.rawValue,
                                                                                     urlString: url.absoluteString,
                                                                                     parameters: parameters?.toDict(),
                                                                                     error: &error) as URLRequest
        }
        
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }
        
        return urlRequest!
    }
}

// MARK: -

public enum UploadMode: Int {
    case UploadFile
    case UploadData
}

open class UploadNetworkRequest: AnyNetworkRequest, NetworkUploadRequest {
    
    public var uploadFileURL: URL?
    public var uploadData: Data?
    public let mode: UploadMode
    
    public init(toURL _URL: URL, fromData _data: Data, method _method: NetworkMethod = .post, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)? ) {
        
        mode = .UploadData
        uploadData = _data
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = UploadNetworkResponse.self
        absoluteURL = _URL
        method = _method
        body = _body
        parameters = _parameters
    }
    
    public init(toURL _URL: URL, fromFileURL _fileURL: URL, method _method: NetworkMethod = .post, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)? ) {
        
        mode = .UploadFile
        uploadFileURL = _fileURL
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = UploadNetworkResponse.self
        absoluteURL = _URL
        method = _method
        body = _body
        parameters = _parameters
    }
    
    public init(toURLString _URLString: String, fromData _data: Data, method _method: NetworkMethod = .post, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)? ) {
        
        mode = .UploadData
        uploadData = _data
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = UploadNetworkResponse.self
        absoluteURL = URL.init(string: _URLString)
        method = _method
        body = _body
        parameters = _parameters
    }
    
    public init(toURLString _URLString: String, fromFileURL _fileURL: URL, method _method: NetworkMethod = .post, parameters _parameters: RequestParameters?, body _body: ((RequestMultipartFormData) -> Void)? ) {
        
        mode = .UploadFile
        uploadFileURL = _fileURL
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = UploadNetworkResponse.self
        absoluteURL = URL.init(string: _URLString)
        method = _method
        body = _body
        parameters = _parameters
    }
    
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?

        if let body = body {
            urlRequest = (serializer as! HTTPRequestSerializer).multipartFormRequest(withMethod: method.rawValue,
                                                                urlString: url.absoluteString,
                                                                parameters: parameters?.toDict(),
                                                                constructingBodyWith: body,
                                                                error: &error) as URLRequest
        } else {
            urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                                   urlString: url.absoluteString,
                                                   parameters: parameters?.toDict(),
                                                   error: &error) as URLRequest
        }
        
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }
        
        return urlRequest!
    }
}

// MARK: -

open class DownloadNetworkRequest: AnyNetworkRequest, NetworkDownloadRequest {
    
    //<HTTPRequestSerializer, DownloadNetworkResponse>
    
    public convenience init(withURLString _URLString: String, method _method: NetworkMethod = .get) {
        self.init(withURLString: _URLString, method: _method, parameters: nil)
    }
    
    public init(withURLString _URLString: String, method _method: NetworkMethod = .get, parameters _parameters: RequestParameters?) {
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = DownloadNetworkResponse.self
        absoluteURL = URL.init(string: _URLString)
        method = _method
        parameters = _parameters
    }
    
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
    
        urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                                   urlString: url.absoluteString,
                                                   parameters: parameters?.toDict(),
                                                   error: &error) as URLRequest
        
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }
        
        return urlRequest!
    }
}

open class ImageDataNetworkRequest: AnyNetworkRequest, NetworkDataRequest {
    
    public convenience init(withURLString URLString: String, method httpMethod: NetworkMethod = .get) {
        self.init(withURLString: URLString, method: httpMethod, parameters: nil)
    }
    
    public init(withURLString URLString: String, method httpMethod: NetworkMethod = .get, parameters params: RequestParameters?) {
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = ImageDataNetworkResponse.self
        absoluteURL = URL.init(string: URLString)
        method = httpMethod
        parameters = params
    }
    
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
        
        urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                               urlString: url.absoluteString,
                                               parameters: parameters?.toDict(),
                                               error: &error) as URLRequest
        
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }
        
        return urlRequest!
    }
}

open class ImageDownloadNetworkRequest: AnyNetworkRequest, NetworkDownloadRequest {
    
    public convenience init(withURLString URLString: String, method httpMethod: NetworkMethod = .get) {
        self.init(withURLString: URLString, method: httpMethod, parameters: nil)
    }
    
    public init(withURLString URLString: String, method httpMethod: NetworkMethod = .get, parameters params: RequestParameters?) {
        
        super.init()
        
        serializer = HTTPRequestSerializer.init()
        responseType = ImageDownloadNetworkResponse.self
        absoluteURL = URL.init(string: URLString)
        method = httpMethod
        parameters = params
    }
        
    override open func buildURLRequest() throws -> URLRequest {
        
        guard let url = absoluteURL else {
            throw NetworkRequestError.emptyURL
        }
        guard URLComponents(url: url, resolvingAgainstBaseURL: true) != nil else {
            throw NetworkRequestError.invalidBaseURL(baseURL!)
        }
        
        for (headerName, headerValue) in headers {
            (serializer as! HTTPRequestSerializer).setValue(headerValue, forHTTPHeaderField: headerName)
        }
        
        var urlRequest: URLRequest?
        var error: NSError?
    
        urlRequest = (serializer as! HTTPRequestSerializer).request(withMethod: method.rawValue,
                                                   urlString: url.absoluteString,
                                                   parameters: parameters?.toDict(),
                                                   error: &error) as URLRequest
        
        guard error == nil else {
            throw error!
        }
        
        if (httpBody != nil) {
            let httpBodyLength = String.init(httpBody!.count)
            (serializer as! HTTPRequestSerializer).setValue(httpBodyLength, forHTTPHeaderField: "Content-Length")
            urlRequest!.httpBody = httpBody!
        }
        
        return urlRequest!
    }
}
