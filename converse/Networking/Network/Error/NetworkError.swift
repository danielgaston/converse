//
//  NetworkError.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import Foundation

/// `SessionTaskError` represents an error that occurs while task for a request.
public enum SessionTaskError: Error {
    /// Error of `URLSession`.
    case connectionError(Error)
    
    /// Error while creating `URLRequest` from `Request`.
    case requestError(Error)
    
    /// Error while creating `Request.Response` from `(Data, URLResponse)`.
    case responseError(Error)
}

// MARK: --

/// `NetworkResponseError` represents a common error that occurs while getting `Request.Response`
/// from raw result tuple `(Data?, URLResponse?, Error?)`.
public enum NetworkResponseError: Error {
    /// Indicates the session adapter returned `URLResponse` that fails to down-cast to `HTTPURLResponse`.
    case nonHTTPURLResponse(URLResponse?)
    
    /// Indicates `HTTPURLResponse.statusCode` is not acceptable.
    /// In most cases, *acceptable* means the value is in `200..<300`.
    case unacceptableStatusCode(Int)
    
    /// Indicates `Any` that represents the response is unexpected.
    case unexpectedObject(Any)
}

// MARK: --

/// `NetworkRequestError` represents a common error that occurs while building `URLRequest` from `Request`.
public enum NetworkRequestError: Error {
    
    /// Indicates `URL` of a type that conforms `Request` is empty.
    case emptyURL
    
    /// Indicates `baseURL` of a type that conforms `Request` is invalid.
    case invalidBaseURL(URL)

    /// Indicates `URLRequest` built by `Request.buildURLRequest` is unexpected.
    case unexpectedURLRequest(URLRequest)
    
    /// Indicates `URL` of a file to upload is empty
    case emptyUploadFileURL
    
    /// Indicates `Data` to upload is empty
    case emptyUploadData
}


