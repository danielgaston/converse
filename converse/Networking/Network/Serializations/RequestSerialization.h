//
//  RequestSerialization.h
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TargetConditionals.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Returns a percent-escaped string following RFC 3986 for a query string key or value.
 RFC 3986 states that the following characters are "reserved" characters.
 - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
 - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
 
 In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
 query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
 should be percent-escaped in the query string.
 
 @param string The string to be percent-escaped.
 
 @return The percent-escaped string.
 */
FOUNDATION_EXPORT NSString* PercentEscapedStringFromString(NSString* string);

/**
 A helper method to generate encoded url query parameters for appending to the end of a URL.
 
 @param parameters A dictionary of key/values to be encoded.
 
 @return A url encoded query string
 */
FOUNDATION_EXPORT NSString* QueryStringFromParameters(NSDictionary* parameters);
FOUNDATION_EXPORT NSString* const ConverseRequestSerializationErrorDomain;
FOUNDATION_EXPORT NSString* const ConverseNetworkingOperationFailingURLRequestErrorKey;

@protocol RequestSerialization <NSObject, NSSecureCoding, NSCopying>

- (instancetype)init;
- (nullable NSURLRequest*)requestBySerializingRequest:(NSURLRequest*)request
									   withParameters:(nullable id)parameters
												error:(NSError* _Nullable __autoreleasing*)error;

@end

@protocol RequestMultipartFormData;

@interface HTTPRequestSerializer : NSObject <RequestSerialization>

@property (nonatomic, assign) NSStringEncoding stringEncoding;
@property (nonatomic, assign) NSURLRequestCachePolicy cachePolicy;
@property (nonatomic, assign) BOOL HTTPShouldHandleCookies;
@property (nonatomic, assign) BOOL HTTPShouldUsePipelining;
@property (nonatomic, assign) NSURLRequestNetworkServiceType networkServiceType;
@property (nonatomic, assign) NSTimeInterval timeoutInterval;

/**
 Default HTTP header field values to be applied to serialized requests. By default, these include the following:
 - `Accept-Language` with the contents of `NSLocale +preferredLanguages`
 - `User-Agent` with the contents of various bundle identifiers and OS designations
 */
@property (readonly, nonatomic, strong) NSDictionary<NSString*, NSString*>* HTTPRequestHeaders;

/**
 Creates and returns a serializer with default configuration.
 */
+ (instancetype)serializer;

- (void)setValue:(nullable NSString*)value forHTTPHeaderField:(NSString*)field;
- (nullable NSString*)valueForHTTPHeaderField:(NSString*)field;
- (void)setAuthorizationHeaderFieldWithUsername:(NSString*)username
									   password:(NSString*)password;
- (void)clearAuthorizationHeader;

///-------------------------------------------------------
/// @name Configuring Query String Parameter Serialization
///-------------------------------------------------------

/**
 HTTP methods for which serialized requests will encode parameters as a query string. `GET`, `HEAD`, and `DELETE` by default.
 */
@property (nonatomic, strong) NSSet<NSString*>* HTTPMethodsEncodingParametersInURI;
- (void)setQueryStringSerializationWithBlock:(nullable NSString* (^)(NSURLRequest* request, id parameters, NSError* __autoreleasing* error))block;

///-------------------------------
/// @name Creating Request Objects
///-------------------------------

- (NSMutableURLRequest*)requestWithMethod:(NSString*)method
								URLString:(NSString*)URLString
							   parameters:(nullable id)parameters
									error:(NSError* _Nullable __autoreleasing*)error;

- (NSMutableURLRequest*)multipartFormRequestWithMethod:(NSString*)method
											 URLString:(NSString*)URLString
											parameters:(nullable NSDictionary<NSString*, id>*)parameters
							 constructingBodyWithBlock:(nullable void (^)(id<RequestMultipartFormData> formData))block
												 error:(NSError* _Nullable __autoreleasing*)error;

- (NSMutableURLRequest*)requestWithMultipartFormRequest:(NSURLRequest*)request
							writingStreamContentsToFile:(NSURL*)fileURL
									  completionHandler:(nullable void (^)(NSError* _Nullable error))handler;

@end

#pragma mark -

@protocol RequestMultipartFormData

- (BOOL)appendPartWithFileURL:(NSURL*)fileURL
						 name:(NSString*)name
						error:(NSError* _Nullable __autoreleasing*)error;

- (BOOL)appendPartWithFileURL:(NSURL*)fileURL
						 name:(NSString*)name
					 fileName:(NSString*)fileName
					 mimeType:(NSString*)mimeType
						error:(NSError* _Nullable __autoreleasing*)error;

- (void)appendPartWithInputStream:(nullable NSInputStream*)inputStream
							 name:(NSString*)name
						 fileName:(NSString*)fileName
						   length:(int64_t)length
						 mimeType:(NSString*)mimeType;

- (void)appendPartWithFileData:(NSData*)data
						  name:(NSString*)name
					  fileName:(NSString*)fileName
					  mimeType:(NSString*)mimeType;

- (void)appendPartWithFormData:(NSData*)data
						  name:(NSString*)name;

- (void)appendPartWithHeaders:(nullable NSDictionary<NSString*, NSString*>*)headers
						 body:(NSData*)body;

@end

#pragma mark -

/**
 `JSONRequestSerializer` is a subclass of `HTTPRequestSerializer` that encodes parameters as JSON using `NSJSONSerialization`, setting the `Content-Type` of the encoded request to `application/json`.
 */
@interface JSONRequestSerializer : HTTPRequestSerializer

@property (nonatomic, assign) NSJSONWritingOptions writingOptions;

+ (instancetype)serializerWithWritingOptions:(NSJSONWritingOptions)writingOptions;

@end

#pragma mark -

/**
 `PropertyListRequestSerializer` is a subclass of `HTTPRequestSerializer` that encodes parameters as JSON using `NSPropertyListSerializer`, setting the `Content-Type` of the encoded request to `application/x-plist`.
 */
@interface PropertyListRequestSerializer : HTTPRequestSerializer

@property (nonatomic, assign) NSPropertyListFormat format;

@property (nonatomic, assign) NSPropertyListWriteOptions writeOptions;

+ (instancetype)serializerWithFormat:(NSPropertyListFormat)format writeOptions:(NSPropertyListWriteOptions)writeOptions;

@end

NS_ASSUME_NONNULL_END
