//
//  AdapterBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

final class AdapterBaseOperation: BlockOperation, BaseOperationPr {
    
    var isDiscarded: Bool
    var _fromOp: AnyObject?
    var _toOp: AnyObject?
    
    // MARK: Object Life Cycle Methods
    
    public init<R: OperationResult, S: OperationResult>(fromOperation fromOp: WrappedBaseOperation<R>, toOperation toOp: WrappedBaseOperation<S>) {
        
        isDiscarded = false
        
        // Prevents WrappedBaseOperation internal operations to be released before executing addExecutionBlock
        _fromOp = fromOp
        _toOp = toOp
        
        super.init()
        self.name = "Adapter Operation: " + (fromOp.name ?? "N/A") + " -> " + (toOp.name ?? "N/A")

        addExecutionBlock {
            [weak self, weak fromOp, weak toOp] in
            
            guard let fromOpOperation = fromOp?.operation else {
                return
            }
            
            guard let toOpOperation = toOp?.operation else {
                return
            }
            
            let fromResult: ExchangeOperationResult? = fromOpOperation.result().adaptedResponseObj()
            guard fromResult != nil else {
                return
            }
            toOpOperation.setAdapted(result: fromResult!)
        
            
            // Makes Adapter Base Operation to be deallocated while their internal operations
            self?._fromOp = nil
            self?._toOp = nil
        }
    }
    
    deinit {
        CLog(type: .operation, flag: .info, level: .level0, message: "DEALLOC " + (name ?? "N/A") + " - " + opUUID().uuidString + ".")
    }
    
    // MARK: Identifiable Protocol Methods
    
    private var _opUUID: NSUUID =  NSUUID()
    public func set(opUUID: NSUUID) {
        _opUUID = opUUID
    }
    public func opUUID() -> NSUUID {
        return _opUUID
    }
}
