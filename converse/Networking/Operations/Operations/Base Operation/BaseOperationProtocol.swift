//
//  BaseOperationProtocol.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

// MARK: Base Operation

public protocol OperationIdentifiable: AnyObject {
    
    func set(opUUID: NSUUID)
    func opUUID() -> NSUUID
}

public protocol OperationDescartable: AnyObject  {
    
    var isDiscarded: Bool {get set}
}

public protocol OperationReportable: AnyObject  {
    
    var startTime: Date? {get set}
    var endTime: Date? {get set}
    func reportResult ()
}

public protocol OperationResultable: AnyObject {
    
    associatedtype ResultType: OperationResult // will be inferred from the operation generics
    
    // Swift: Defined as methods. Defining it as variable creates conflicts since we cannot override a variable type
    func set(result: ResultType)
    func result() -> ResultType
    func set(completion: ((ResultType) -> Void)?)
    func completion() -> ((ResultType) -> Void)?
    
    func setAdapted(result: ExchangeOperationResult)
}

// ------------

public protocol BaseOperationPr: OperationIdentifiable, OperationDescartable {
    
    var name: String? { get }
}

public protocol BaseResultOperationPr: BaseOperationPr, OperationResultable {}

// ------------

public typealias BaseOperation = Operation & BaseOperationPr
public typealias BaseResultOperation = Operation & BaseResultOperationPr

// ------------

// MARK: Cache Operation

public protocol CacheBaseOperationDelegate : AnyObject {
    
    func cacheBaseOperation(_ op: CacheBaseOperation, reportsDuration duration: TimeInterval)
}


// MARK: Network Operation

public protocol NetworkBaseOperationDelegate : AnyObject {

    func networkBaseOperationRequestDefaultSession(_ op: NetworkBaseOperation) -> URLSession
    func networkBaseOperationRequestBackgroundSession(_ op: NetworkBaseOperation) -> URLSession
    func networkBaseOperation(_ op: NetworkBaseOperation, didStartWithTask task: URLSessionTask?)
    func networkBaseOperation(_ op: NetworkBaseOperation, didDiscardTask task: URLSessionTask?)
    func networkBaseOperation(_ op: NetworkBaseOperation, requiresStoreCompletion completion: ((NetworkOperationResult) -> Void)?, forTask task: URLSessionTask?)
    func networkBaseOperation(_ op: Operation, reportsKBperSecond KBperSec: Double)
}


// MARK: Parse Operation

public protocol ParseBaseOperationDelegate : AnyObject {
    
    func parseBaseOperation(_ op: BaseOperation, reportsDuration duration: TimeInterval)
}


// MARK: Conversion Operation

public protocol ConversionBaseOperationDelegate : AnyObject {
    
    func conversionBaseOperation(_ op: BaseOperation, reportsDuration duration: TimeInterval)
}
