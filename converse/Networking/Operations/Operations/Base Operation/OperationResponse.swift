//
//  OperationResponse.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public protocol OperationResult : AnyObject {
    
    // Swift: Associatedtype is a protocol generic placeholder for an unknown Concrete Type that requires concretisation on adoption at Compile time.
    associatedtype ResponseType
    
    var data: Data? { get set }
    var responseObj: ResponseType? { get set }
    var error: Error? { get set }
    var extraInfo: Dictionary<String, Any>? { get set }
        
    init()
    init(_ result: ExchangeOperationResult)
    
    func adaptedResponseObj() -> ExchangeOperationResult
    func hasData() -> Bool
    func hasDataObject() -> Bool
    func hasError() -> Bool
    
    func shallowCopy () -> Any
}

// MARK:-

open class GenericOperationResult<R> : OperationResult, NSCopying {

    // Swift: type alias gives a placeholder name to a type that is used as part of the protocol
    public typealias ResponseType = R
    
    public var data: Data?
    public var responseObj: ResponseType?
    public var error: Error?
    public var extraInfo: Dictionary<String, Any>?
    
    public required init() {
        extraInfo = [String : Any]()
    }
    
    public required init(_ result: ExchangeOperationResult) {
        // ℹ️ Assign & Cast .responseObj does not make sense since custom may be any Type. Transfer data it is stored in .data
        data = result.data
        error = result.error
        extraInfo = result.extraInfo
    }
    
    public func adaptedResponseObj() -> ExchangeOperationResult {
        let exchangeOpResult = ExchangeOperationResult.init()
        exchangeOpResult.data = data
        exchangeOpResult.responseObj = responseObj as Any
        exchangeOpResult.error = error
        exchangeOpResult.extraInfo = extraInfo
        
        return exchangeOpResult
    }
    
    // MARK: NSCopying Protocol Methods
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = type(of: self).init()
        copy.data = self.data
        copy.responseObj = self.responseObj
        copy.error = self.error
        copy.extraInfo = self.extraInfo
        
        return copy
    }
    
    // MARK: OperationResult Protocol Methods
    public func shallowCopy() -> Any {
        return self.copy()
    }
    
    public func hasData() -> Bool {
        var hasData = false
        if let resultData = data, resultData.count != 0 {
            hasData.toggle()
        }
    
        return hasData
    }
    
    public func hasDataObject() -> Bool {
        return responseObj != nil
    }
    
    public func hasError() -> Bool {
        return error != nil
    }
}

// MARK:-

open class AnyOperationResult : GenericOperationResult<Any> {}

// MARK:-

open class ExchangeOperationResult : GenericOperationResult<Any> {}

// MARK:-

open class NetworkOperationResult : GenericOperationResult<URL> {
    
    // URL is sent via responseObj so that we must serve its data to be converted if necessary adaptedResponseObj. Ex. networkOpResult -> ImageOpResult
    override public func adaptedResponseObj() -> ExchangeOperationResult {
        let exchangeOpResult = ExchangeOperationResult.init()
        
        if let resp = responseObj {
            do {
                exchangeOpResult.data = try Data.init(contentsOf: resp)
            } catch {
                exchangeOpResult.error = error
            }
        } else {
            exchangeOpResult.data = data
        }
        
        exchangeOpResult.responseObj = responseObj
        exchangeOpResult.error = error
        exchangeOpResult.extraInfo = extraInfo
        
        return exchangeOpResult
    }
}

// MARK:-

open class ImageOperationResult : GenericOperationResult<UIImage> {
}

// MARK:-

open class JSONOperationResult : ExchangeOperationResult {}

// MARK:-

public protocol OperationResultExtraInfo {
    
    static func key() -> String
}

// MARK:-

open class NetworkOperationResultExtraInfo : OperationResultExtraInfo {
    
    public var task: URLSessionTask?
    public var responseURL: URLResponse?
    
    // MARK: OperationResultExtraInfo Protocol Methods
    
    public static func key() -> String {
        return "NetworkOperationResultExtraInfo"
    }
}
