//
//  BaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

open class GenericBaseOperation<ResultType: OperationResult>: BaseResultOperation {
    
    // MARK: TypeAlias
    
    // MARK: Variables & Constants
    
    public var isDiscarded: Bool
    public var startTime: Date?
    public var endTime: Date?
    private var _result: ResultType
    private var _completion : ((ResultType) -> Void)?
    
    // MARK: Object Life Cycle Methods
    
    override public init() {
        isDiscarded = false
        _result = ResultType.init()
        super.init()
    }
    
    deinit {
        CLog(type: .operation, flag: .info, level: .level0, message: "DEALLOC " + (name ?? "N/A") + " - " + opUUID().uuidString + ".")
    }
    
    // MARK: Shadowed Operation Resultable Methods
    
    private var _opUUID: NSUUID = NSUUID()
    public func set(opUUID: NSUUID) {
        _opUUID = opUUID
    }
    public func opUUID() -> NSUUID {
        return _opUUID
    }
    
    public func set(result: ResultType) {
        _result = result
    }
    
    public func result() -> ResultType {
        return _result
    }
    
    public func set(completion: ((ResultType) -> Void)?) {
        _completion = completion
    }
    
    public func completion() -> ((ResultType) -> Void)? {
        return _completion
    }
    
    public func setAdapted(result: ExchangeOperationResult) {
        let customOpResult = ResultType.init(result)
        set(result: customOpResult)
    }
    
    
    // MARK: Public Methods
    
    /*!
     * @brief Finishes the execution of the operation
     * @warning This shouldn’t be called externally as this is used internally by subclasses. To cancel an operation use cancel instead.
     */
    open func finish () {
        endTime = Date.init()
        if self.isExecuting {
            // DG: makes the operation be removed from the queue. The queue monitors those two values using key-value observing.
            self.isExecuting = false
            self.isFinished = true
            CLog(type: .operation, flag: .info, level: .level0, message:(name ?? "N/A") + " Operation Finished. " + opUUID().uuidString)
        }
    }
    
    /*!
     * @brief Checks current operation state to cancel it as soon as possible if necessary
     */
    open func isValid () -> Bool {
        if self.isCancelled {
            return false
        } else {
            return true
        }
    }
    
    // MARK: Overriding Methods

    override open func start () {
        startTime = Date.init()
        
        if !self.isValid() {
            self.isFinished = true
        } else if !self.isExecuting {
            self.isExecuting = true
            self.isFinished = false
            startTime = Date.init()
            CLog(type: .operation, flag: .info, level: .level0, message:(name ?? "N/A") + " Operation Started. " + opUUID().uuidString)
        }
    }

    //MARK: Getters & Setters

    private var _isExecuting: Bool = false
    override open var isExecuting: Bool {
        get {
            return _isExecuting
        }
        set {
            if _isExecuting != newValue {
                willChangeValue(forKey: "isExecuting")
                _isExecuting = newValue
                didChangeValue(forKey: "isExecuting")
            }
        }
    }

    private var _isFinished: Bool = false
    override open var isFinished: Bool {
        get {
            return _isFinished
        }
        set {
            if _isFinished != newValue {
                willChangeValue(forKey: "isFinished")
                _isFinished = newValue
                didChangeValue(forKey: "isFinished")
            }
        }
    }

    private var _isAsynchronous: Bool = true
    override open var isAsynchronous: Bool {
        get {
            return _isAsynchronous
        }
        set {
            if _isAsynchronous != newValue {
                willChangeValue(forKey: "isAsynchronous")
                _isAsynchronous = newValue
                didChangeValue(forKey: "isAsynchronous")
            }
        }
    }

    //MARK: Helper Methods

    override open func cancel () {
        super.cancel()

        CLog(type: .operation, flag: .info, level: .level0, message:(name ?? "N/A") + " Operation Cancelled. " + opUUID().uuidString)
        finish()
    }

}


// MARK: -

open class _AnyBaseOperationBase<ResultType>: BaseResultOperation where ResultType: OperationResult {
    
    override init() {
        super.init()
        name = "Wrapping Helper Operation"
        guard type(of: self) != _AnyBaseOperationBase.self else {
            fatalError("_AnyBaseOperationBase<ResultType> instances can not be created; create a subclass instance instead")
        }
    }
    
    deinit {
        CLog(type: .operation, flag: .info, level: .level0, message: "DEALLOC " + (name ?? "N/A") + " - " + opUUID().uuidString + ".")
    }
    
    private var _opUUID: NSUUID =  NSUUID()
    public func set(opUUID: NSUUID) {
        fatalError("Must override")
    }
    public func opUUID() -> NSUUID {
        fatalError("Must override")
    }
    
    public var isDiscarded: Bool = false
    public var startTime: Date?
    public var endTime: Date?
    
    private var _result: ResultType = /*AnyOperationResult<Any>.init() as! ResultType*/ ResultType.init()
    public func set(result: ResultType) {
        fatalError("Must override")
    }
    public func result() -> ResultType {
        fatalError("Must override")
    }
    
    private var _completion: ((ResultType) -> Void)?
    public func set(completion: ((ResultType) -> Void)?) {
        fatalError("Must override")
    }
    public func completion() -> ((ResultType) -> Void)? {
        fatalError("Must override")
    }
    
    public func setAdapted(result: ExchangeOperationResult) {
        fatalError("Must override")
    }
}

// MARK: -

open class _AnyBaseOperation<Concrete: BaseResultOperationPr>: _AnyBaseOperationBase<Concrete.ResultType> {
    
    var concrete: Concrete
    
    init(_ concret: Concrete) {
        concrete = concret
        super.init()
        name = "Wrapping Helper: " + (concret.name ?? "N/A")
    }
    
    override public func set(opUUID: NSUUID) {
        concrete.set(opUUID: opUUID)
    }
    override public func opUUID() -> NSUUID {
        return concrete.opUUID()
    }
    
    override public func set(result: Concrete.ResultType) {
        concrete.set(result: result)
    }
    
    override public func result() -> Concrete.ResultType {
        return concrete.result()
    }
    
    override public func set(completion: ((Concrete.ResultType) -> Void)?) {
        concrete.set(completion: completion)
    }
    
    override public func completion() -> ((Concrete.ResultType) -> Void)? {
        return concrete.completion()
    }
    
    override public func setAdapted(result: ExchangeOperationResult) {
        concrete.setAdapted(result: result)
    }
}

// MARK: -

open class WrappedBaseOperation<ResultType>: BaseResultOperationPr where ResultType: OperationResult {
    
    public let operation: _AnyBaseOperationBase<ResultType>
    
    // MARK: Variables & Constants
    
    public var isDiscarded: Bool {
        get {
            return operation.isDiscarded
        }
        set(newIsDiscarded) {
            operation.isDiscarded = newIsDiscarded
        }
    }
    public var startTime: Date? {
        get {
            return operation.startTime
        }
        set(newStartTime) {
            operation.startTime = newStartTime
        }
    }
    public var endTime: Date? {
        get {
            return operation.endTime
        }
        set(newEndTime) {
            operation.endTime = newEndTime
        }
    }
    
    // MARK: Object Life Cycle Methods
    
    init<Concrete: BaseResultOperationPr>(_ concrete: Concrete) where Concrete.ResultType == ResultType {
        operation = _AnyBaseOperation(concrete)
        isDiscarded = false
    }
    
    public func set(opUUID: NSUUID) {
        operation.set(opUUID: opUUID)
    }
    public func opUUID() -> NSUUID {
        return operation.opUUID()
    }
    
    public func set(result: ResultType) {
        operation.set(result: result)
    }
    
    public func result() -> ResultType {
        return operation.result()
    }
    
    public func set(completion: ((ResultType) -> Void)?) {
        operation.set(completion: completion)
    }
    
    public func completion() -> ((ResultType) -> Void)? {
        return operation.completion()
    }
    
    public func setAdapted(result: ExchangeOperationResult) {
        operation.setAdapted(result: result)
    }
    
    // Shared Operation Method & BaseOperation Protocol
    public var name: String? { return operation.name }
    
    public func cancel() {
        operation.cancel()
    }
    
    // Operation Variables and Methods
    
    var completionBlock: (() -> Void)? { return operation.completionBlock }
    var isCancelled: Bool { return operation.isCancelled }
    var isExecuting: Bool { return operation.isExecuting }
    var isFinished: Bool { return operation.isFinished }
    var isConcurrent: Bool { return operation.isConcurrent }
    var isAsynchronous: Bool { return operation.isAsynchronous }
    var isReady: Bool { return operation.isReady }
    var dependencies: [Operation] { return operation.dependencies}
    var qualityOfService: QualityOfService { return operation.qualityOfService}
    var queuePriority: Operation.QueuePriority { return operation.queuePriority}
    
    func start() {
        operation.start()
    }
    
    func main() {
        operation.main()
    }
    
    func addDependency(_ op: Operation) {
        operation.addDependency(op)
    }
    
    func removeDependency(_ op: Operation) {
        operation.removeDependency(op)
    }
    
    func waitUntilFinished() {
        operation.waitUntilFinished()
    }
}
