//
//  BlockBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public typealias BlockBaseOperationCompletionBlock = () -> Void
public typealias BlockBaseOperationBlock = (_ completionBlock : BlockBaseOperationCompletionBlock) -> Void

final class BlockBaseOperation: GenericBaseOperation<ExchangeOperationResult> {
    
    // MARK: Variables & Constants
    
    var block : BlockBaseOperationBlock
    
    // MARK: Object Life Cycle Methods
    
    /**
     *  Initialized with a block to be executed when the operation starts
     *
     *  @param block the block to run when the operation starts.
     *
     */
    public init(withBlock baseBlock: @escaping BlockBaseOperationBlock) {
        block = baseBlock
        
        super.init()
        
        name = "API Block Operation Operation"
    }
    
    // MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        if isValid() {
            block({finish()})
        }
    }
}
