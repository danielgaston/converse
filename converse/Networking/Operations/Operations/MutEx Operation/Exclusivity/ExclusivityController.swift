//
//  ExclusivityController.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/**
 async - concurrent: the code runs on a background thread. Control returns immediately to the main thread (and UI). The block can't assume that it's the only block running on that queue
 async - serial: the code runs on a background thread. Control returns immediately to the main thread. The block can assume that it's the only block running on that queue
 sync - concurrent: the code runs on a background thread but the main thread waits for it to finish, blocking any updates to the UI. The block can't assume that it's the only block running on that queue (I could have added another block using async a few seconds previously)
 sync - serial: the code runs on a background thread but the main thread waits for it to finish, blocking any updates to the UI. The block can assume that it's the only block running on that queue
 
 
 Creating a concurrent queue
 ---------------------------
 let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
 concurrentQueue.sync {...}
 
 Create a serial queue
 ---------------------------
 let serialQueue = DispatchQueue(label: "queuename")
 serialQueue.sync {...}
 
 Get main queue asynchronously
 ---------------------------
 DispatchQueue.main.async {...}
 
 Get main queue synchronously
 ---------------------------
 DispatchQueue.main.sync {...}
 
 To get one of the background thread
 ---------------------------
 DispatchQueue.global(qos: .background).async {...}
 

 To get one of the background thread
 ---------------------------
 DispatchQueue.global(qos: .default).async {...}
 
 DispatchQueue.global().async { // qos' default value is ´DispatchQoS.QoSClass.default`}
 
 More info
 https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
 https://stackoverflow.com/questions/19179358/concurrent-vs-serial-queues-in-gcd/35810608#35810608
 */


/**
 * The exclusivity controller will guaranty that the condition that mutually
 * excludes each other will _always_ run one after the other and never in
 * parallel.
 * This class is a singleton to keep track of all mutex. However it is OK to
 * instanciate it for a local mutal exclusion
 */

class ExclusivityController {

    private let DISPATCH_QUEUE_NAME = "OperationMutexList"
    
    // Simulates ObjC + (instancetype)sharedInstance
    static let shared = ExclusivityController()
    
    // We are forced to use Reference types with NSMapTable (String & [] are value types)
    var operationsLock: NSMapTable<NSString, NSMutableArray>
    var dispatchQueue: DispatchQueue
    
    //MARK: Operation LifeCycle Methods
    
    // Simulates ObjC - (instancetype)init NS_UNAVAILABLE
    private init() {
        operationsLock = NSMapTable.strongToStrongObjects()
        // Serial queue must use sync() instead of async()
        dispatchQueue = DispatchQueue.init(label: DISPATCH_QUEUE_NAME)
    }
    // Simulates ObjC - (instancetype)initLocalInstance NS_DESIGNATED_INITIALIZER
    class func initLocalInstance() -> ExclusivityController {
        let localInstance = ExclusivityController.init()
        return localInstance
    }
    
    //MARK: Public Methods
    
    func validate(operation: MutExBaseOperation, categories: [Any]) -> Bool {
        
        var isValid = true
        
        dispatchQueue.sync {
            
            for category in categories {
                let ops = operationsLock.object(forKey: category as? NSString)
                // isValid = TRUE if no operation is queued
                if ops != nil, ops?.count != 0 {
                    let op: MutExBaseOperation = ops?.lastObject as! MutExBaseOperation
                    
                    // isValid = TRUE if there is one or more queued operations, but last one is not 'singleInstanceExclusive'
                    if op.isSingleInstanceExclusive {
                        isValid = false
                        break
                    }
                }
            }
        }
        
        // please note it is a SYNC dispatch
        return isValid
    }
    
    func add(operation: MutExBaseOperation, categories: [Any]) {
        dispatchQueue.sync {
            for category in categories {
                addMutex(onOperation: operation, toCategory: category as! String)
            }
        }
    }

    func remove(operation: MutExBaseOperation, categories: [Any]) {
        dispatchQueue.sync {
            for category in categories {
                removeMutex(onOperation: operation, toCategory: category as! String)
            }
        }
    }
    
    //MARK: Helper Methods
    
    private func addMutex(onOperation operation: MutExBaseOperation, toCategory category: String) {
    
        if operationsLock.object(forKey: category as NSString) == nil {
            operationsLock.setObject(NSMutableArray(), forKey: category as NSString)
        }

        let previousOperation: MutExBaseOperation? = operationsLock.object(forKey: category as NSString)?.lastObject as? MutExBaseOperation
        if previousOperation != nil {
            operation.addDependency(previousOperation!)
        }
        
        operationsLock.object(forKey: category as NSString)?.add(operation)
    }
    
    private func removeMutex(onOperation operation: MutExBaseOperation, toCategory category: String) {
        operationsLock.object(forKey: category as NSString)?.remove(operation)
    }
    
}
