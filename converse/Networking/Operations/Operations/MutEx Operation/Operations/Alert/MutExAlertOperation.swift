//
//  MutExAlertOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

final class MutExAlertOperation: MutExBaseOperation {

    var alertController: UIAlertController?
    var alertData: MutExAlertData
    
    // MARK: Object Lifecycle Methods

    override public init() {
        alertData = MutExAlertData.init(title: "", message: "")
        
        super.init()
        
        name = "Alert MutEx Operation"
        isSingleInstanceExclusive = true
    }
    
    /*!
     * @brief Consecutive Alert Operations are added with serial dependency.
     * @discussion 'isMutuallyExclusive' is Not editable, belongs to each MutExBaseOperation subclass (YES by default). Assures consecutive operations are serial queued. 'isSingleInstanceExclusive' is editable and assures only one operation instance is either queued or running
     * @warning if 'isMutuallyExclusive' is YES (default), a second operation will not be executed until a first one finishes. If 'isSingleInstanceExclusive' is YES, a second operation will be discarded if a first one is queued or running. Only one instance can be present at a time.
     */
    
    public convenience init(alertWithData data: MutExAlertData) {
        self.init(alertWithData: data, singleInstanceExclusive: false)
    }
    
    public convenience init(sheetWithData data: MutExAlertData) {
        self.init(sheetWithData: data, singleInstanceExclusive: false)
    }
    
    public convenience init(alertWithData data: MutExAlertData, singleInstanceExclusive: Bool) {
        self.init()
        
        name = "Alert MutEx Operation"
        isSingleInstanceExclusive = singleInstanceExclusive
        alertData = data
        initializeAlert()
    }
    
    public convenience init(sheetWithData data: MutExAlertData, singleInstanceExclusive: Bool) {
        self.init()
        
        name = "Sheet MutEx Operation"
        isSingleInstanceExclusive = singleInstanceExclusive
        alertData = data
        initializeSheet()
    }
    
    // MARK: Initialization Methods
    
    private func initializeAlert() {
        
        alertController = UIAlertController.init(title: alertData.title, message: alertData.message, preferredStyle: .alert)
        
        if alertData.actionsArray.count > 0 {
            // action buttons
            for buttonAction in alertData.actionsArray {
                alertController?.addAction(UIAlertAction.init(title: buttonAction.title, style: buttonAction.style, handler: { [weak self] (action) in
                    buttonAction.handler()
                    self?.finish()
                }))
            }
        } else {
            // default cancel button
            alertController?.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: { [weak self] (action) in
                self?.finish()
            }))
        }
    }
    
    private func initializeSheet() {
        let alertStyle: UIAlertController.Style = (UIDevice.current.userInterfaceIdiom == .pad) ? .alert : .actionSheet
        
        alertController = UIAlertController.init(title: alertData.title, message: alertData.message, preferredStyle: alertStyle)
        
        if alertData.actionsArray.count > 0 {
            // action buttons
            for buttonAction in alertData.actionsArray {
                alertController?.addAction(UIAlertAction.init(title: buttonAction.title, style: buttonAction.style, handler: { [weak self] (action) in
                    buttonAction.handler()
                    self?.finish()
                }))
            }
        } else {
            // default cancel button
            alertController?.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { [weak self] (action) in
                self?.finish()
            }))
        }
    }
    
    // MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        if isValid() {
            DispatchQueue.main.async {
                self.presentViewControllerAnimated()
            }
        } else {
            super.cancel()
        }
    }
    
    
    // MARK: Helper Methods
    
    private func presentViewControllerAnimated() {
        if alertController != nil {
            
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            
            if let tabViewController: UITabBarController = keyWindow?.rootViewController as? UITabBarController {
                
                tabViewController.viewControllers?.first?.present(alertController!, animated: true, completion: nil)
                
            } else if let splitViewController: UISplitViewController = keyWindow?.rootViewController as? UISplitViewController {
                
                splitViewController.viewControllers.first?.present(alertController!, animated: true, completion: nil)
                
            } else if let viewController: UIViewController = keyWindow?.rootViewController {
                
                viewController.navigationController?.present(alertController!, animated: true, completion: nil)
            }
        }
    }
}
