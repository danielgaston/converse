//
//  MutExAlertData.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public typealias MutExAlertActionHandler = () -> Void

public struct MutExAlertAction {
    let title: String
    let style: UIAlertAction.Style
    let handler: MutExAlertActionHandler
    
    // MARK: Object LifeCycle Methods
    
    public init(title: String, style: UIAlertAction.Style, handler: @escaping MutExAlertActionHandler) {
        self.title = title
        self.style = style
        self.handler = handler
    }
}

// MARK:-

public struct MutExAlertData {
    
    var title: String
    var message: String
    private(set) var actionsArray: [MutExAlertAction]
    
    // MARK: Object LifeCycle Methods
    
    public init(title: String, message: String) {
        self.title = title
        self.message = message
        actionsArray = [MutExAlertAction]()
    }
    
    // MARK: Public Methods
    
    public mutating func addButton(withTitle title: String, handler: @escaping MutExAlertActionHandler) {
        createButton(title: title, style: .default, handler: handler)
    }
    
    public mutating func addButtons(withTitles titles: [String], handlers: [MutExAlertActionHandler]) {
        let minIterator = min(titles.count, handlers.count)
        
        for i in 0 ..< minIterator {
            addButton(withTitle:titles[i], handler:handlers[i])
        }
    }
    
    public mutating func addCancelButton(withTitle title: String, handler: @escaping MutExAlertActionHandler) {
        createButton(title: title, style: .cancel, handler: handler)
    }
    
    public mutating func addCancelButton(handler: @escaping MutExAlertActionHandler) {
        createButton(title: "Dismiss", style: .cancel, handler: handler)
    }
    
    public mutating func addDestructiveButton(withTitle title: String, handler: @escaping MutExAlertActionHandler) {
        createButton(title: title, style: .destructive, handler: handler)
    }
    
    // MARK: Helper Methods
    
    private mutating func createButton(title: String, style: UIAlertAction.Style, handler: @escaping MutExAlertActionHandler) {
        
        let action: MutExAlertAction = MutExAlertAction.init(title: title, style: style, handler: handler)
        actionsArray.append(action)
    }
}
