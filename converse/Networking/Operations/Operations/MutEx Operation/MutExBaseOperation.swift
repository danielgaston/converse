//
//  MutExBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

protocol MutExObserver : AnyObject {
    func operationDidFinish(_ mutexOp: MutExBaseOperation?)
}

// MARK:-

class MutExOperationClosureObserver: MutExObserver {

    private var finishClosure: ((MutExBaseOperation) -> Void)?
    
    init(finishClosure closure: @escaping (MutExBaseOperation) -> Void ) {
        finishClosure = closure
    }
    
    // MARK: MutExObserver Protocol Methods
    
    func operationDidFinish(_ mutexOp: MutExBaseOperation?) {
        if finishClosure != nil && mutexOp != nil {
            DispatchQueue.main.async {
                self.finishClosure!(mutexOp!)
            }
        }
    }
}

// MARK:-

protocol MutExOperation: AnyObject {
    
    var observers: [MutExObserver] {get}
    var isMutuallyExclusive: Bool {get}
    var isSingleInstanceExclusive: Bool {get set}
    
    func addObserver(observer: MutExObserver)
    func removeObserver(observer: MutExObserver)
}

// MARK:-

open class MutExBaseOperation: GenericBaseOperation<ExchangeOperationResult>, MutExOperation {

    // MARK: TypeAlias
    public typealias DefaultResultType = ExchangeOperationResult
    
    // MARK: Variables & Constants
    
    var observers: [MutExObserver] {
        get {
            var tmp = [MutExObserver].init()
            tmp.append(contentsOf:_observers)
            return tmp
        }
    }
    private var _observers: [MutExObserver]
    
    /*!
     * @brief Consecutive MutEx Operations are added with serial dependency.
     * @discussion 'isMutuallyExclusive' is Not editable, belongs to each MutExBaseOperation subclass (YES by default). Assures consecutive operations are serial queued. 'isSingleInstanceExclusive' is editable and assures only one operation instance is either queued or running.
     * @warning if 'isMutuallyExclusive' is YES (default), a second operation will not be executed until a first one finishes. If 'isSingleInstanceExclusive' is YES, a second operation will be discarded if a first one is queued or running. Only one instance can be present at a time.
     */
    
    private (set) var isMutuallyExclusive: Bool
    var isSingleInstanceExclusive: Bool
    
    
    // MARK: Object Life Cycle Methods
    
    override public init() {
        _observers = [MutExObserver]()
        isMutuallyExclusive = true
        isSingleInstanceExclusive = false
        
        super.init()
        
        name = "Mutual Exclusivity Operation"
    }
    
    // MARK: Overriden Methods
    
    override open func finish() {
        
        _observers.forEach { [weak self] (observer) in
            DispatchQueue.main.async {
                self?.removeObserver(observer: observer)
                observer.operationDidFinish(self)
            }
        }
        
        super.finish()
    }
    
    // MARK: MutExOperation Delegate Methods
    
    func addObserver(observer: MutExObserver) {
        _observers.append(observer)
    }
    
    func removeObserver(observer: MutExObserver) {        
        if let index = _observers.firstIndex(where: {$0 === observer}) {
            _observers.remove(at: index)
        }
    }
}
