//
//  AlertManager.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

open class AlertManager {

    let internalQueue: ConverseOperationQueue
    
    // MARK: Object Life Cycle Methods
    
    public init() {
        internalQueue = ConverseOperationQueue()
        internalQueue.qualityOfService = .userInitiated
        internalQueue.name = "Internal Generic Purpose Queue"
        internalQueue.isSuspended = false
    }
    
    deinit {
        internalQueue.cancelAllOperations()
    }
    
    // MARK: Public Methods
    
    public func alert(withData data: MutExAlertData) {
        let alertOp = MutExAlertOperation.init(alertWithData: data)
        internalQueue.addOperation(alertOp)
    }
    
    public func alerts(withData data: [MutExAlertData]) {
        for alertData in data {
            alert(withData: alertData)
        }
    }
    
    public func alertSingleInstance(withData data: MutExAlertData) {
        let alertOp = MutExAlertOperation.init(alertWithData: data, singleInstanceExclusive: true)
        internalQueue.addOperation(alertOp)
    }
    
    
    public func sheet(withData data: MutExAlertData) {
        let sheetOp = MutExAlertOperation.init(sheetWithData: data)
        internalQueue.addOperation(sheetOp)
    }
    
    public func sheets(withData data: [MutExAlertData]) {
        for alertData in data {
            sheet(withData: alertData)
        }
    }
    
    public func sheetSingleInstance(withData data: MutExAlertData) {
        let sheetOp = MutExAlertOperation.init(sheetWithData: data, singleInstanceExclusive: true)
        internalQueue.addOperation(sheetOp)
    }
}
