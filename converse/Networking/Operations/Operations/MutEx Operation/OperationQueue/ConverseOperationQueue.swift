//
//  ConverseOperationQueue.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

class ConverseOperationQueue: OperationQueue {

    // MARK: Overriden Methods
    
    override func addOperation(_ operation: Operation) {
        
        if let op = operation as? MutExBaseOperation {
            
            let mutexOp: MutExBaseOperation = op
            let mutexClasses = NSMutableArray.init().adding(String.init(describing: type(of:mutexOp)))
            
            let mutexController = ExclusivityController.shared
            
            if mutexController.validate(operation: mutexOp, categories: mutexClasses) {
                
                mutexController.add(operation: mutexOp, categories: mutexClasses)
                
                let mutexObserver = MutExOperationClosureObserver.init { (mutexOperation) in
                    mutexController.remove(operation: mutexOp, categories: mutexClasses)
                }
                mutexOp.addObserver(observer: mutexObserver)
                
                super.addOperation(mutexOp)
            }
        } else {
            super.addOperation(operation)
        }
    }
    
    override func addOperations(_ ops: [Operation], waitUntilFinished wait: Bool) {
        /*
         The base implementation of this method does not call `addOperation()`,
         so we'll call it ourselves.
         */
        for operation in ops {
            addOperation(operation)
        }
        
        if wait {
            for operation in ops {
                operation.waitUntilFinished()
            }
        }
    }
}
