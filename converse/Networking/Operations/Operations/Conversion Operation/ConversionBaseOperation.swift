//
//  ConversionBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/* Swift: Double Generic-Type definition.
    - Fulfils superclass one
    - Provides subclasses to override it
 */

open class ConversionBaseOperation<ResultType: OperationResult>: GenericBaseOperation<ResultType> {

    // MARK: Variables & Constants
    
    weak var operationDelegate: ConversionBaseOperationDelegate?
    
    // MARK: Overriden Methods
    
    override open func finish() {
        super.finish()
        
        guard isValid() else {
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            return
        }
            
        if startTime != nil && endTime != nil {
            let interval = endTime!.timeIntervalSince(startTime!)
            operationDelegate?.conversionBaseOperation(self, reportsDuration: interval.rounded(toPlaces: 3))
        }        
    }
    
    // MARK: Public Methods
    
    /*!
     * @brief Checks the opearation has not been discarded and executes the completion closure
     */
    public func reportResult () {
        DispatchQueue.main.async {
            if !self.isDiscarded && self.completion() != nil {
                self.completion()!(self.result())
            }
        }
    }
}
