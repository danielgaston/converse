//
//  ConversionImageOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public typealias ConversionImageOperationCompletion = (ConversionImageOperation.DefaultResultType) -> Void

open class ConversionImageOperation: ConversionBaseOperation<ImageOperationResult> {

    // MARK: TypeAlias
    
    public typealias DefaultResultType = ImageOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    public init(operationDelegate del: ConversionBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        operationDelegate = del
        set (completion: comp)
        name = "Image Conversion Operation"
    }
    
    //MARK: Overriden Methods
    
    override open func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        CLog(type: .operation, flag: .info, level: .level1, message: "Data -> UIImage conversion started.")
        
        result().responseObj = UIImage.init(data: result().data!)
        
        CLog(type: .operation, flag: .info, level: .level1, message: "Data -> UIImage conversion ended.")
        
        reportResult()
        finish()
    }
}
