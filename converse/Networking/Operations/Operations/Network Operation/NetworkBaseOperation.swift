//
//  NetworkBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/**
 -------------------------------------------------------------------------------------------------------------------------------------------
 |                    |    OK                                          | OK-RESULT |            FORCED FAILURE                 | FF-RESULT |
 -------------------------------------------------------------------------------------------------------------------------------------------
 |Data Task           |                                                |           |                                           |           |
 |                    |                                                |           |                                           |           |
 |    Default         |    1.NetworkDataOp - didReceiveResponse        |           |1.NetworkDataOp - didCompleteWithError     |           |
 |                    |    2.NetworkDataOp - willCacheResponse         |           |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
 |                    |    3.NetworkDataOp - didCompleteWithError      |           |                                           |           |
 |                    |    4.NetworkBaseOp - didCompleteWithError      |     ✔     |                                           |           |
 |                    |                                                |           |                                           |           |
 |    Background      |    Bg DataTasks are Unavailable in iOS         |     -     |                                           |     -     |
 -------------------------------------------------------------------------------------------------------------------------------------------
 |Download Task       |                                                |           |                                           |           |
 |                    |                                                |           |                                           |           |
 |    Default         |    1.NetworkDownOp - didFinishDownloadingToURL |           |1.NetworkBaseOp - didCompleteWithError     |     ✔     |
 |                    |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |                                           |           |
 |                    |                                                |           |                                           |           |
 |    Background      |    1.NetworkDownOp - didFinishDownloadingToURL |           |1.NetworkDownOp - didFinishDownloadingToURL|           |
 |                    |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
 -------------------------------------------------------------------------------------------------------------------------------------------
 |Upload Task         |                                                |           |                                           |           |
 |                    |                                                |           |                                           |           |
 |    Default         |    1.NetworkDataOp - didReceiveResponse        |           |1.NetworkDataOp - didReceiveResponse       |           |
 |    (File/Data)     |    2.NetworkDataOp - willCacheResponse         |           |2.NetworkDataOp - didCompleteWithError     |           |
 |                    |    3.NetworkDataOp - didCompleteWithError      |           |3.NetworkBaseOp - didCompleteWithError     |           |
 |                    |    4.NetworkBaseOp - didCompleteWithError      |     ✔     |4.NetworkDataOp - willCacheResponse        |     ✔     |
 |                    |                                                |           |                                           |           |
 |    Background      |    1.NetworkDataOp - didCompleteWithError      |           |1.NetworkDataOp - didCompleteWithError     |           |
 |    (File only)     |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
 -------------------------------------------------------------------------------------------------------------------------------------------
 
 BG Processig: https://developer.apple.com/documentation/foundation/url_loading_system/downloading_files_in_the_background?language=objc
 */

public enum NetworkOperationMode {
    case modeDefault
    case modeBackground
}

public protocol CommonNetworkProcessing {
    func prepareStart()
    func processStart()
    func processExtraInfo()
    func processResponseValidity(response: URLResponse?) -> Bool
}

public typealias NetworkOperationProgress = ((Progress) -> Void)
public typealias NetworkOperationCompletion = (NetworkBaseOperation.DefaultResultType) -> Void

open class NetworkBaseOperation: GenericBaseOperation<NetworkOperationResult>, URLSessionTaskDelegate, CommonNetworkProcessing, OperationReportable {
    
    // MARK: TypeAlias
    public typealias DefaultResultType = NetworkOperationResult
    public typealias RequestType = NetworkRequest
    public typealias URLResponseType = URLResponse
    public typealias URLSessionType = URLSessionTask
    
    // MARK: Variables & Constants
    
    /*!
     * @brief Delegate used to report network speed, and adapt the queue accordingly
     * @warning OperationNetworkManager variable must be retained in a ViewController, otherwise operationDelegate is deallocated before the request finishes
     */
    public weak var operationDelegate : NetworkBaseOperationDelegate?
    
    public var task: URLSessionType?
    public var responseURL: URLResponseType?
    public var taskRequest: RequestType
    public var operationMode: NetworkOperationMode
    public var progress: NetworkOperationProgress?
    
    private var proposedCredential: URLCredential?
    private var sharedCredentials: [String:URLCredential]?
    
    private var _restartCounter = 1;
    
    //MARK: Operation LifeCycle Methods
    
    public convenience init(request: NetworkRequest, operationDelegate: NetworkBaseOperationDelegate?) {
        // Safety check 3 (A convenience initializer must delegate to another initializer before assigning a value to any property (including properties defined by the same class). If it doesn’t, the new value the convenience initializer assigns will be overwritten by its own class’s designated initializer.)
        self.init(request:request, operationDelegate:operationDelegate, completion:nil)
    }
    
    public convenience init(request: NetworkRequest, operationDelegate: NetworkBaseOperationDelegate?, completion: ((DefaultResultType) -> Void)?) {
        // Safety check 3 (A convenience initializer must delegate to another initializer before assigning a value to any property (including properties defined by the same class). If it doesn’t, the new value the convenience initializer assigns will be overwritten by its own class’s designated initializer.)
        self.init(request:request, operationDelegate:operationDelegate, progress:nil, completion:completion)
    }
    
    public init(request: NetworkRequest, operationDelegate opDel: NetworkBaseOperationDelegate?, progress prog: NetworkOperationProgress?, completion comp: ((DefaultResultType) -> Void)?) {
        // 1st Phase (each stored property is assigned an initial value by the class that introduced it)
        
        // Safety check 1 (A designated initializer must ensure that all of the properties introduced by its class are initialized BEFORE it delegates up to a SUPERCLASS initializer.)
        taskRequest = request
        operationMode = .modeDefault
        operationDelegate = opDel
        progress = prog
        
        // Safety check 2 (A designated initializer MUST delegate up to a SUPERCLASS initializer BEFORE assigning a value to an INHERITED property. If it doesn’t, the new value the designated initializer assigns will be overwritten by the superclass as part of its own initialization.)
        super.init()
        
        // 2nd Phase (opportunity to customize its stored properties further before the new instance is considered ready for use)
        
        // Safety check 4 (An initializer cannot call any instance methods, read the values of any instance properties, or refer to self as a value until AFTER the first phase of initialization is complete.)
        name = "Network Base Operation"
        qualityOfService = .userInitiated
        queuePriority = .normal
        result().data = Data.init()
        set(completion: comp)
    }
    
    
    // MARK: Overriden Methods
    
    override open func isValid() -> Bool {
        let validValue = super.isValid()
        
        if !validValue {
            task?.cancel()
        }
        return validValue
    }
    
    override open func cancel() {
        super.cancel()
        
        task?.cancel()
    }
    
    override open func start() {
        super.start()
        
        guard isValid() else {
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            finish()
            return
        }
        
        guard !result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation injected data, no more processing needed.")
            finish()
            return
        }
        
        prepareStart()
        processStart()
    }
    
    func restart() {
        _restartCounter -= 1
        
        CLog(type: .operation, flag: .info, level: .level1, message: "Operation will restart.")
        
        repeatRequest()
    }
    
    // MARK: Public Methods
    
    /*!
     * @brief Creates a new networkTask and performs a networkRequest again.
     * @discussion This method helps with recreating the necessary environment within the Operation to safely perform the networkTask, using the same request that the Operation has been initialized with. This method should be called internally, controlling all the URLSessionTask-related delegate methods, usually by creating the corresponding categories overriding the default delegate methods logic.
     * @warning This shouldn’t be called externally as this is used internally by subclasses/categories if necessary (i.e. 401 related relogin without 'WWW-Authenticate: Basic' headers, that would enter through 'urlSession(_:task:didReceive:completionHandler:)' otherwise).
     */
    public func repeatRequest() {
        // dicard current task
        discardCurrentTask()
        // reset request result
        prepareStart()
        // perform request
        processStart()
    }
    
    /*!
     * @brief Checks the operation has not been discarded and executes the completion block
     */
    public func reportResult() {
        DispatchQueue.main.async {
            if !self.isDiscarded && self.completion() != nil {
                self.completion()!(self.result())
            }
        }
    }
    
    // MARK: CommonNetworkProcessing Delegate Methods

    public func prepareStart() {
        result().data = Data.init()
        result().error = nil
    }
    
    public func processStart() {
        self.finish()
    }
    
    public func processExtraInfo() {
        var extraInfoNetworkResult: NetworkOperationResultExtraInfo
        
        let extraInfo = result().extraInfo?[NetworkOperationResultExtraInfo.key()]
        if extraInfo != nil {
            // Has extraInfo coming from other network operation
            extraInfoNetworkResult = result().extraInfo![NetworkOperationResultExtraInfo.key()] as! NetworkOperationResultExtraInfo
        } else {
            // Creates new extraInfo
            extraInfoNetworkResult = NetworkOperationResultExtraInfo()
        }
        
        extraInfoNetworkResult.task = task
        extraInfoNetworkResult.responseURL = responseURL
        
        result().extraInfo?.updateValue(extraInfoNetworkResult, forKey: NetworkOperationResultExtraInfo.key())
    }
    
    public func processResponseValidity(response: URLResponse?) -> Bool {
        
        responseURL = response
        
        guard let _task = task else {
            result().error = NetworkBaseOperationError.emptyURLSessionTaskError
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionTask does not exist.")
            return false
        }
        
        guard let _response = response, (_response is HTTPURLResponse) else {
            result().error = NetworkBaseOperationError.emptyURLResponseError
            CLog(type: .operation, flag: .error, level: .level1, message: "URLResponse is either nil or not an HTTPURLResponse.")
            return false
        }
        
        let responseType = taskRequest.responseType
        let cnvResponse = responseType.init(withTask: _task, response: _response, data: result().data, error: result().error)
        
        var error: NSError?
        let isValid = cnvResponse.serializer.validate((_response as! HTTPURLResponse), data: result().data, error:&error)
        result().error = result().error ?? error
        
        return isValid
    }
    
    // MARK: Helper Methods
    
    func discardCurrentTask() {
        operationDelegate?.networkBaseOperation(self, didDiscardTask: task)
        task?.cancel()
    }
    
    func showAuthenticationAlertController(withCompletion completion: @escaping ((URLSession.AuthChallengeDisposition, URLCredential?) -> Void) ) {
        
        let alertController : UIAlertController = UIAlertController.init(title: "Authentication", message: "Please enter credentials", preferredStyle: .alert)
        
        if sharedCredentials?.count != 0 {
            // TODO: ⚠️ Show List of matching sharedCredentials
        }
        
        let cancel: UIAlertAction = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
            completion(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        }
        
        let submit: UIAlertAction = UIAlertAction.init(title: "Submit", style: .default) { (action) in
            
            let userTextField: UITextField = (alertController.textFields?[0])!
            let passTextField: UITextField = (alertController.textFields?[1])!
            
            let newCredential = URLCredential.init(user: userTextField.text ?? "", password: passTextField.text ?? "", persistence: .none)
            
            completion(URLSession.AuthChallengeDisposition.useCredential, newCredential)
        }
        
        alertController.addAction(cancel)
        alertController.addAction(submit)
        
        alertController.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = "Username"
            textField.text = self.proposedCredential?.user
        })
        
        alertController.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        })
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        if keyWindow?.rootViewController != nil {
           keyWindow!.rootViewController?.present(alertController, animated: true, completion: nil)
        } else {
            completion(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        }
    }
    
    // MARK: URLSessionTaskDelegate
    
    public func urlSession(_ session: URLSession,
                                task: URLSessionTask,
                                didCompleteWithError error: Error?) {
        _ = self.processResponseValidity(response: self.task?.response)
        
        /// .constrained determines iOS is in LOW DATA mode.
        /// .expensive or .cellullar determines iOS is in EXPENSIVE DATA mode
        /// restart() should be complemented with accessing a low quality resource. At the moment this is not happening so that we access the same resource twice
        if #available(iOS 13.0, *) {
            if let error = error as? URLError, error.networkUnavailableReason == .constrained, _restartCounter != 0 {
                self.restart()
                return
            }
        } else {
            // Fallback on earlier versions
        }
        
        endTime = Date.init()
        result().error = error ?? self.result().error
        
        if isValid() {
            if result().hasError() {
                CLog(type: .operation, flag: .error, level: .level1, message: "Failed to receive response: \(String(describing: result().error?.localizedDescription)).")

                cancel()
            }
            
            processExtraInfo()
            
            if (completion() != nil) {
                reportResult()
            }
            
            if (startTime != nil && endTime != nil && result().hasData()) {
                let interval = endTime!.timeIntervalSince(startTime!)
                // number of KiloBytes container in the mutabla data object
                let bytes: Int = result().data?.count ?? 0
                let KB = Double(bytes) / Double(1024)
                let KBPerSec = KB / interval
                
                operationDelegate?.networkBaseOperation(self, reportsKBperSecond: KBPerSec)
            }
            
            /// Data appended at this point already
            finish()
        }
    }
    
    /*!
     * @brief https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/URLLoadingSystem/Articles/AuthenticationChallenges.html
     * @discussion Important: The URL loading system classes do not call their delegates to handle request challenges unless the server response contains a WWW-Authenticate header. Other authentication types, such as proxy authentication and TLS trust validation do not require this header.
     
     NSURLSessionAuthChallengeDisposition:
     -------------------------------------
     - NSURLSessionAuthChallengeUseCredential: Use the specified credential, which may be nil.
     - NSURLSessionAuthChallengePerformDefaultHandling: Use the default handling for the challenge as though this delegate method were not implemented. The provided credential parameter is ignored.
     - NSURLSessionAuthChallengeCancelAuthenticationChallenge: Cancel the entire request. The provided credential parameter is ignored.
     - NSURLSessionAuthChallengeRejectProtectionSpace: Reject this challenge, and call the authentication delegate method again with the next authentication protection space. The provided credential parameter is ignored.
     */
    
    public func urlSession(_ session: URLSession,
                                task: URLSessionTask,
                didReceive challenge: URLAuthenticationChallenge,
                    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let authMethod = challenge.protectionSpace.authenticationMethod
        
        if authMethod == NSURLAuthenticationMethodHTTPBasic {
            // Same as [[NSURLCredentialStorage sharedCredentialStorage] defaultCredentialForProtectionSpace:challenge.protectionSpace]
            proposedCredential = challenge.proposedCredential
            // All Credentials (including not default) that match the protection space
            sharedCredentials = URLCredentialStorage.shared.credentials(for: challenge.protectionSpace)
            
            if challenge.previousFailureCount == 0 && proposedCredential != nil {
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, proposedCredential)
            } else {
                // performSelector:withObject:afterDelay: is unavailable in Swift. Workaround:
                DispatchQueue.main.async {
                    self.showAuthenticationAlertController(withCompletion: completionHandler)
                }
            }
        } else {
            completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil)
        }
    }
    
    /*!
     * @brief https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/URLLoadingSystem/Articles/RequestChanges.html
     * @warning This method is called only for tasks in default and ephemeral sessions. Tasks in background sessions automatically follow redirects.
     */
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
    
        let newRequest: URLRequest = request

        completionHandler (newRequest)
    }
}
