//
//  NetworkOperationError.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/// `NetworkBaseOperationError` represents an error that occurs while working with any NetworkBaseOperation.
public enum NetworkBaseOperationError: Error {
    /// Error while validating `URLResponse`.
    case emptyURLSessionTaskError
    /// Error while validating `URLResponse`.
    case emptyURLResponseError
}

/// `NetworkDataOperationError` represents an error that occurs while working with any NetworkDataOperation.
public enum NetworkDataOperationError: Error {
    /// Error when creating `URLSessionDataTask`.
    case emptyURLSession
    /// Error when creating `URLSessionDataTask`.
    case createURLSessionDataTaskError(Error)
}

/// `NetworkUploadOperationError` represents an error that occurs while working with any NetworkUploadOperationError.
public enum NetworkUploadOperationError: Error {
    /// Error when creating `URLSessionUploadTask`.
    case emptyURLSession
    /// Error when creating `URLSessionUploadTask`.
    case createURLSessionUploadTaskError(Error)
    /// Error when creating `URLSessionUploadTask`.
    case invalidUploadNetworkRequestError
}

/// `NetworkDownloadOperationError` represents an error that occurs while working with any NetworkDownloadOperationError.
public enum NetworkDownloadOperationError: Error {
    /// Error when creating `URLSessionDownloadTask`.
    case emptyURLSession
    /// Error when creating `URLSessionDownloadTask`.
    case createURLSessionDownloadTaskError(Error)
    /// Error when creating destination folder URL
    case emptyDestinationFolderURLError
    /// Error when creating destination file URL
    case emptyDestinationFileURLError
    /// Error when looking for a downloaded file at a specified URL location.
    case emptyFileAtLocationError
    /// Error when moving a downloaded file into a new URL location
    case moveFileToLocationError
}




