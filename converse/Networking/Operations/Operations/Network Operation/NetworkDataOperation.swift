//
//  NetworkDataOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

open class NetworkDataOperation: NetworkBaseOperation, URLSessionDataDelegate {
    
    // MARK: TypeAlias
    typealias RequestType = NetworkDataRequest
    typealias URLResponseType = HTTPURLResponse
    typealias URLSessionType = URLSessionDataTask

    // MARK: Variables & Constants

    var observers = [NSKeyValueObservation?]()
    
    // MARK: Object LifeCycle Methods
    
    public init(request: NetworkDataRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
       
        super.init(request: request, operationDelegate: operationDelegate, progress: progress, completion: completion)
        name = "Network Data Operation"
        qualityOfService = .userInitiated
        queuePriority = .normal
    }
    
    deinit {
        deinitObservers()
    }
    
    // MARK: Initialization Methods
    
    func initObservers() {
        
        deinitObservers()
        
        observers = [
            task?.progress.observe(\.fractionCompleted, changeHandler: { [weak self] (taskProgress, _) in
                if self?.progress != nil {
                    DispatchQueue.main.async {
                        self?.progress?(taskProgress)
                    }
                }
            })]
    }
    
    func deinitObservers() {
        observers.forEach { (observer) in
            observer?.invalidate()
        }
    }
    
    // MARK: Helper Methods
    
    func sessionForCurrentOperationMode() -> URLSession? {
        var session: URLSession?
        switch operationMode {
        case .modeBackground:
            session = operationDelegate?.networkBaseOperationRequestBackgroundSession(self)
        default:
            session = operationDelegate?.networkBaseOperationRequestDefaultSession(self)
        }
        return session
    }
    
    // MARK: CommonNetworkProcessing Delegate Methods
    
    override public func processStart() {
        CLog(type: .operation, flag: .info, level: .level1, message: "URLSessionDataTask start for: \(taskRequest.absoluteURL?.absoluteString ?? "N/A").")
        
        guard let session = sessionForCurrentOperationMode() else {
            result().error = result().error ?? NetworkDataOperationError.emptyURLSession
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionDataTask has completed with error: \(String(describing: NetworkDataOperationError.emptyURLSession)).")
            if isValid() {
                finish()
            }
            return
        }
        
        do {
            try task = session.runSessionDataTask(request: taskRequest as! NetworkDataRequest)
            if operationMode == .modeBackground {
                operationDelegate?.networkBaseOperation(self, requiresStoreCompletion: completion(), forTask: task)
            }
            
            initObservers()
            
            task?.resume()
            operationDelegate?.networkBaseOperation(self, didStartWithTask: task)
        } catch {
            result().error = result().error ?? NetworkDataOperationError.createURLSessionDataTaskError(error)
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionDataTask has completed with error: \(String(describing: error.localizedDescription)).")
            if isValid() {
                finish()
            }
        }
    }
    
    // MARK: URLSessionTaskDelegate
    
    override public func urlSession(_ session: URLSession,
                                task: URLSessionTask,
                                didCompleteWithError error: Error?) {
        deinitObservers()
        super.urlSession(session, task: task, didCompleteWithError: error)
    }
    
    
    // MARK: URLSessionDataTask Delegate Methods
    // Tells the delegate that the data task received the initial reply (headers) from the server.
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        let valid = processResponseValidity(response: response)
        processExtraInfo()
        
        completionHandler(valid ? .allow : .cancel)
    }
    
    // Tells the delegate that the data task has received some of the expected data.
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        if isValid() {
            result().data?.append(data)
        }
    }
    
    // This delegate method is called only for Data and Upload tasks. The caching policy for download tasks is determined by the specified cache policy exclusively. usePrococolCachePolicy caches HTTPS responses to disk, which may be undesirable for securing user data. this method allows to change this behavior by manually handling caching behavior.
    // https://developer.apple.com/documentation/foundation/urlsessiondatadelegate/1411612-urlsession
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void) {
        
        _ = isValid()
        
        var newCachedResponse = proposedResponse
        let newUserInfo: [String : Any] = ["Cached Date" : NSDate.init()]
        
        if proposedResponse.response.url?.scheme == "https" {
            newCachedResponse = CachedURLResponse.init(response: proposedResponse.response, data: proposedResponse.data, userInfo: newUserInfo, storagePolicy: .allowedInMemoryOnly)
        } else {
            newCachedResponse = CachedURLResponse.init(response: proposedResponse.response, data: proposedResponse.data, userInfo: newUserInfo, storagePolicy: proposedResponse.storagePolicy)
        }
        
        completionHandler(newCachedResponse)
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // Left blank intentionally
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // Left blank intentionally
    }
}
