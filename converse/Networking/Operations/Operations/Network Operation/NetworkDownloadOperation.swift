//
//  NetworkDownloadOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

final class NetworkDownloadOperation: NetworkBaseOperation, URLSessionDownloadDelegate {
    
    // MARK: TypeAlias

    typealias RequestType = NetworkDownloadRequest
    typealias URLSessionType = URLSessionDownloadTask

    
    // MARK: Variables & Constants

    var observers = [NSKeyValueObservation?]()
    var destinationFolder: FileManager.SearchPathDirectory?
    
    enum NetworkDownloadOperationErrors: Error {
        case EmptyFileURL
    }
    
    // MARK: Object LifeCycle Methods
    
    /*!
     * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
     * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    
    public convenience init(request: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?) {
        self.init(request:request, operationDelegate:operationDelegate, completion:nil)
    }
    
    public convenience init(request: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, completion: NetworkOperationCompletion?) {
        self.init(request:request, operationDelegate:operationDelegate, progress:nil, completion:completion)
    }
    
    public convenience init(request: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        self.init(request:request, operationDelegate:operationDelegate, destinationFolder: nil, progress:progress, completion:completion)
    }
    
    public init(request: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, destinationFolder destFolder: FileManager.SearchPathDirectory?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        // 1st Phase
        destinationFolder = destFolder
        
        // Super init
        super.init(request:request, operationDelegate:operationDelegate, progress:progress, completion:completion)
        
        // 2nd Phase
        name = "Network Download Operation"
        qualityOfService = .background
        queuePriority = .low
    }
    
    /*
     * Comply with Background Transfer Limitations
     * With background sessions, the actual transfer is performed by a process that is separate from your app’s process. Because restarting your app’s process is fairly expensive, a few features are unavailable, resulting in the following limitations:
     *
     * ·The session must provide a delegate for event delivery. (For uploads and downloads, the delegates behave the same as for in-process transfers.)
     * ·Only HTTP and HTTPS protocols are supported (no custom protocols).
     * ·Redirects are always followed. As a result, even if you have implemented URLSession:task:willPerformHTTPRedirection:newRequest:completionHandler:, it is not called.
     *
     * ·Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     */
    
    public convenience init(backgroundRequest: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?) {
        self.init(backgroundRequest:backgroundRequest, operationDelegate:operationDelegate, completion:nil)
    }
    
    public convenience init(backgroundRequest: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, completion: NetworkOperationCompletion?) {
        self.init(backgroundRequest:backgroundRequest, operationDelegate:operationDelegate, progress:nil, completion:completion)
    }
    
    public convenience init(backgroundRequest: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        self.init(backgroundRequest:backgroundRequest, operationDelegate:operationDelegate, destinationFolder: nil, progress:progress, completion:completion)
    }
    
    /*!
     * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
     * @discussion Download will be executing in background even if the app is suspended. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    public init(backgroundRequest: NetworkDownloadRequest, operationDelegate: NetworkBaseOperationDelegate?, destinationFolder destFolder: FileManager.SearchPathDirectory?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        destinationFolder = destFolder
        
        super.init(request:backgroundRequest, operationDelegate:operationDelegate, progress:progress, completion:completion)
        
        name = "Network Background Download Operation"
        qualityOfService = .background
        queuePriority = .low
    }
    
    deinit {
        deinitObservers()
    }
    
    // MARK: - Overriden Methods
    
    /// Necessary for download tasks with completion where result.responseObject() is URL. Relying on the NetworkBaseOperation - reportResults() would return nil URL.
    override public func reportResult() {
        DispatchQueue.main.async {
            if !self.isDiscarded && self.completion() != nil {
                self.completion()!(self.result())
            }
        }
    }
    
    // MARK: Initialization Methods
    
    func initObservers() {
        
        deinitObservers()
        
        observers = [
        task?.progress.observe(\.fractionCompleted, changeHandler: { [weak self] (taskProgress, _) in
            if self?.progress != nil {
                DispatchQueue.main.async {
                    self?.progress?(taskProgress)
                }
            }
        })]
    }
    
    func deinitObservers() {
        observers.forEach { (observer) in
            observer?.invalidate()
        }
    }
    
    // MARK: Helper Methods
    
    func sessionForCurrentOperationMode() -> URLSession? {
        var session: URLSession?
        switch operationMode {
        case .modeBackground:
            session = operationDelegate?.networkBaseOperationRequestBackgroundSession(self)
        default:
            session = operationDelegate?.networkBaseOperationRequestDefaultSession(self)
        }
        return session
    }
    
    // MARK: CommonNetworkProcessing Delegate Methods
    
    override public func processStart() {
        CLog(type: .operation, flag: .info, level: .level1, message: "URLSessionDownloadTask start for: \(taskRequest.absoluteURL?.absoluteString ?? "N/A").")
        
        guard let session = sessionForCurrentOperationMode() else {
            result().error = result().error ?? NetworkDownloadOperationError.emptyURLSession
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionDownloadTask has completed with error: \(String(describing: NetworkDownloadOperationError.emptyURLSession)).")
            if isValid() {
                finish()
            }
            return
        }

        do {
            try task = session.runSessionDownloadTask(request: taskRequest as! NetworkDownloadRequest)
            
            if operationMode == .modeBackground {
                operationDelegate?.networkBaseOperation(self, requiresStoreCompletion: completion(), forTask: task)
            }
            
            initObservers()
            
            task?.resume()
            
            operationDelegate?.networkBaseOperation(self, didStartWithTask: task)
        } catch {
            result().error = result().error ?? NetworkDownloadOperationError.createURLSessionDownloadTaskError(error)
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionDownloadTask has completed with error: \(String(describing: error.localizedDescription)).")
            if isValid() {
                finish()
            }
        }
    }
    
    // MARK: URLSessionTaskDelegate
    
    override public func urlSession(_ session: URLSession,
                                task: URLSessionTask,
                                didCompleteWithError error: Error?) {
        deinitObservers()
        super.urlSession(session, task: task, didCompleteWithError: error)
    }
    
    
    // MARK: URLSessionDownloadTask Delegate Methods
    
    /// Handling Download Life Cycle Changes
    /// Pipeline:
    /// 1st. urlSession(URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo: URL)
    /// 2nd. urlSession(URLSession, task: URLSessionTask, didCompleteWithError: Error?)
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        CLog(type: .operation, flag: .info, level: .level1, message: "URLSessionDownloadTask has completed.")
        result().data = location.absoluteString.data(using: .utf8)
        if processResponseValidity(response: downloadTask.response) {
            
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: location.path) {
                // downloaded file does exist at the specified URL location
                
                // 1. Create Destination Folder URL
                var _destinationFolderURL : URL?
                if destinationFolder != nil {
                    _destinationFolderURL = fileManager.urls(for: destinationFolder!, in: .userDomainMask).first
                } else {
                    _destinationFolderURL = URL.init(fileURLWithPath: NSTemporaryDirectory())
                }
                guard let destinationFolderURL = _destinationFolderURL else {
                    result().error = result().error ?? NetworkDownloadOperationError.emptyDestinationFolderURLError
                    finish()
                    return
                }
                
                // 2. Create Destination Folder+File URL
                let _destinationFileURL : URL? = destinationFolderURL.appendingPathComponent(taskRequest.absoluteURL?.pathComponents.last ?? "") //myPhoto.jpg
                guard var destinationFileURL = _destinationFileURL else {
                    result().error = result().error ?? NetworkDownloadOperationError.emptyDestinationFileURLError
                    finish()
                    return
                }
                
                // 3. Move file to Destination Folder+File URL
                if !fileManager.fileExists(atPath: destinationFileURL.path) {
                    // 3.1 File is downloaded for the first time
                    do {
                        try fileManager.moveItem(at: location, to: destinationFileURL)
                        result().responseObj = destinationFileURL
                    } catch {
                        CLog(type: .operation, flag: .error, level: .level1, message: error.localizedDescription)
                        result().error = result().error ?? NetworkDownloadOperationError.moveFileToLocationError
                    }
                } else {
                    // 3.2 File has been previously downloadeded. Requires rename first to avoid name collision, and move it afterwards
                    var pathExtension = taskRequest.absoluteURL?.pathExtension  // jpg
                    if pathExtension != nil {
                        // extension prefixed with a dot
                        pathExtension = ".".appending(pathExtension!) // .jpg
                    }
                    let fileName = taskRequest.absoluteURL?.deletingPathExtension().lastPathComponent      // myPhoto
                    destinationFileURL = destinationFolderURL.appendingPathComponent( (fileName ?? "") + "_" + "\(Date.init().timeIntervalSinceReferenceDate)" + (pathExtension ?? ".XXX"))
                    
                    do {
                        try fileManager.moveItem(at: location, to: destinationFileURL)
                        result().responseObj = destinationFileURL
                    } catch {
                        CLog(type: .operation, flag: .error, level: .level1, message: error.localizedDescription)
                        result().error = result().error ?? NetworkDownloadOperationError.moveFileToLocationError
                    }
                }
            } else {
                // downloaded file does not exist at the specified URL location
                result().error = result().error ?? NetworkDownloadOperationError.emptyFileAtLocationError
            }
        }        
    }
    
    // Receiving Progress Updates
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        // Left blank intentionally
    }

    // Resuming Paused Downloads
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        let percentDone = (Double(fileOffset)/Double(expectedTotalBytes)) * 100
        CLog(type: .operation, flag: .info, level: .level1, message: "URLSessionDownloadTask did resume at: \(String(format: "%.2f %%", percentDone)).")
    }
}
