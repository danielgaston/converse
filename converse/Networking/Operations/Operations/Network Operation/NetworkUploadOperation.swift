//
//  NetworkUploadOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

final class NetworkUploadOperation: NetworkDataOperation {
    
    // MARK: TypeAlias
    typealias RequestType = NetworkUploadRequest
    typealias URLSessionType = URLSessionUploadTask

    // MARK: Variables & Constants
    
    // MARK: Object LifeCycle Methods
    
    /************************************************************************
     * Comply with Background Transfer Limitations
     * With background sessions, the actual transfer is performed by a process that is separate from your app’s process. Because restarting your app’s process is fairly expensive, a few features are unavailable, resulting in the following limitations:
     *
     * ·The session must provide a delegate for event delivery. (For uploads and downloads, the delegates behave the same as for in-process transfers.)
     * ·Only HTTP and HTTPS protocols are supported (no custom protocols).
     * ·Redirects are always followed. As a result, even if you have implemented URLSession:task:willPerformHTTPRedirection:newRequest:completionHandler:, it is not called.
     *
     * ·Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     *************************************************************************/
    
    /*!
     * @brief Upload Operation
     * @discussion Upload will be executing in background even if the app is suspended. There is No caching involved in this download. Use 'inBackground' only if you really need large uploads to continue in the background after the app is suspended/terminated.
     * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     */
    
    public convenience init(request: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?) {
        self.init(request: request, operationDelegate: operationDelegate, completion: nil)
    }
    
    public convenience init(request: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?, completion: NetworkOperationCompletion?) {
        self.init(request: request, operationDelegate: operationDelegate, progress: nil, completion: completion)
    }
    
    public init(request: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        super.init(request: request, operationDelegate: operationDelegate, progress: progress, completion: completion)
        
        name = "Network Upload Operation"
        qualityOfService = .userInitiated
        queuePriority = .normal
        operationMode = .modeDefault
    }
    
    public convenience init(backgroundRequest: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?) {
        self.init(backgroundRequest: backgroundRequest, operationDelegate: operationDelegate, completion: nil)
    }
    
    public convenience init(backgroundRequest: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?, completion: NetworkOperationCompletion?) {
        self.init(backgroundRequest: backgroundRequest, operationDelegate: operationDelegate, progress: nil, completion: completion)
    }

    public init(backgroundRequest: NetworkUploadRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        super.init(request: backgroundRequest, operationDelegate: operationDelegate, progress: progress, completion: completion)
        
        name = "Network Background Upload Operation"
        qualityOfService = .background
        queuePriority = .low
        operationMode = .modeBackground
    }
    
    deinit {
        deinitObservers()
    }
    
    // MARK: Initialization Methods
    
    override func initObservers() {
        
        deinitObservers()
        
        observers = [
        task?.progress.observe(\.fractionCompleted, changeHandler: { [weak self] (taskProgress, _) in
            if self?.progress != nil {
                DispatchQueue.main.async {
                    self?.progress?(taskProgress)
                }
            }
        })]
    }
    
    override func deinitObservers() {
        observers.forEach { (observer) in
            observer?.invalidate()
        }
    }

    // MARK: Helper Methods
    
    override func sessionForCurrentOperationMode() -> URLSession? {
        var session: URLSession?
        switch operationMode {
            case .modeBackground:
                session = operationDelegate?.networkBaseOperationRequestBackgroundSession(self)
            default:
                session = operationDelegate?.networkBaseOperationRequestDefaultSession(self)
        }
        return session
    }
    
    // MARK: CommonNetworkProcessing Delegate Methods
    
    override public func processStart() {
        CLog(type: .operation, flag: .info, level: .level1, message: "URLSessionUploadTask start for: \(String(describing: taskRequest.absoluteURL?.absoluteString)).")
        
        guard let session = sessionForCurrentOperationMode() else {
            result().error = result().error ?? NetworkUploadOperationError.emptyURLSession
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionUploadTask has completed with error: \(String(describing: NetworkUploadOperationError.emptyURLSession)).")
            if isValid() {
                finish()
            }
            return
        }
        
        do {
            guard let uploadRequest = taskRequest as? NetworkUploadRequest else {
                
                result().error = result().error ?? NetworkUploadOperationError.invalidUploadNetworkRequestError
                if isValid() {
                    finish()
                }
                return
            }
            
            switch uploadRequest.mode {
                case .UploadFile:
                    task = try session.runSessionUploadTask(request: uploadRequest, fileURL: uploadRequest.uploadFileURL!)
                case .UploadData:
                    task = try session.runSessionUploadTask(request: uploadRequest, bodyData: uploadRequest.uploadData!)
            }
            
            if operationMode == .modeBackground {
                operationDelegate?.networkBaseOperation(self, requiresStoreCompletion: completion(), forTask: task)
            }
            
            initObservers()
            
            task?.resume()
            
            operationDelegate?.networkBaseOperation(self, didStartWithTask: task)
        } catch {
            result().error = result().error ?? NetworkUploadOperationError.createURLSessionUploadTaskError(error)
            CLog(type: .operation, flag: .error, level: .level1, message: "URLSessionUploadTask has completed with error: \(String(describing: error.localizedDescription)).")
            if isValid() {
                finish()
            }
        }
    }
    
    // MARK: URLSessionTaskDelegate
    
    override public func urlSession(_ session: URLSession,
                                task: URLSessionTask,
                                didCompleteWithError error: Error?) {
        deinitObservers()
        super.urlSession(session, task: task, didCompleteWithError: error)
    }
    
    // MARK: URLSessionUploadTask Delegate Methods
    
    // SessionUploadDelegate does not exist. It is inheriting NetworkDataOperation Delegate Methods as URLSessionUploadTask inherits from URLSessionDataTask
}
