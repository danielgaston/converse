//
//  CacheJSONOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

class CacheJSONOperation: CacheBaseOperation {

    // MARK: Overriden Methods
    
    override init(URLString: String?, locked: Bool, operationDelegate: CacheBaseOperationDelegate?) {
        super.init(URLString: URLString, locked: locked, operationDelegate: operationDelegate)
        
        cache = cacheManager.cache(forType: .CacheTypeJSON).first
        name = "JSON Cache Operation"
    }
    
    convenience init(URLString: String?, operationDelegate: CacheBaseOperationDelegate?) {
        self.init(URLString: URLString, locked: false, operationDelegate: operationDelegate)
    }
}
