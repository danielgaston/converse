//
//  CacheBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit
import SPTPersistentCache

open class CacheBaseOperation: GenericBaseOperation<ExchangeOperationResult> {
    
    // MARK: Variables & Constants
    
    weak var operationDelegate: CacheBaseOperationDelegate?
    var URLString: String?
    var locked: Bool
    var cacheManager: CacheManager
    var cache: Cache?
    
    //MARK: Operation LifeCycle Methods
    
    public init(URLString URLStr: String?, locked lkd: Bool, operationDelegate del: CacheBaseOperationDelegate?) {
        URLString = URLStr
        locked = lkd
        operationDelegate = del
        cacheManager = CacheManager.init()
        
        super.init()
    }
    
    public convenience init(URLString: String?, operationDelegate: CacheBaseOperationDelegate?) {
        self.init(URLString: URLString, locked: false, operationDelegate: operationDelegate)
    }
    
    //MARK: Overriden Methods
    
    override open func start() {
        super.start()
    
        guard isValid() else {
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            finish()
            return
        }
        
        guard URLString != nil else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation DID NOT injected URL, no more processing needed.")
            finish()
            return
        }
        
        if hasDataToProcess() {
            name = "Write " + (name ?? "N/A")
            CLog(type: .operation, flag: .info, level: .level1, message: "Starting Cache Write Strategy.")
            
            checkAndStoreData()
        } else {
            name = "Read " + (name ?? "N/A")
            CLog(type: .operation, flag: .info, level: .level1, message: "Starting Cache Read Strategy.")
            
            readData()
        }
    }
    
    override open func finish() {
        super.finish()
        
        guard isValid() else {
            return
        }
        
        if startTime != nil && endTime != nil {
            let interval: TimeInterval = endTime!.timeIntervalSince(startTime!)
            operationDelegate?.cacheBaseOperation(self, reportsDuration: interval.rounded(toPlaces: 3))
        }
    }
    
    //MARK: Public Methods
    
    func readData () {
        CLog(type: .operation, flag: .info, level: .level1, message: "Cache Read -Start- for: \(String(describing: URLString)).")

        _ = cache?.loadData(forURLString: URLString!, withCallback: { [weak self] (response) in
            
            if !(self?.result().hasError() ?? false) {
                self?.result().error = response.error
            }
            
            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Read -End- for: \(String(describing: self?.URLString)).")
            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Read Result: \(String(describing: response.description)).")
            
            var newData = Data.init()
            newData.append(response.record.data)
            self?.result().data = newData
            
            self?.finish()
            
        }, onQueue: DispatchQueue.global(qos: .default))
        
    }

    func storeData() {
        
        // stores new value or overwrittes the existing one
        CLog(type: .operation, flag: .info, level: .level1, message: "Cache Write -Start- for: \(String(describing: URLString)).")
        
        let ttl = processTTLStorage()
        let data = processDataStorage()
        
        guard data != nil else {
            readData()
            return
        }
        
        _ = cache?.storeData(data: data!, forURLString: URLString!, ttl: ttl, locked: locked, withCallback: { [weak self] (response) in
            
            if !(self?.result().hasError() ?? false) {
                self?.result().error = response.error
            }

            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Write -End- for: \(String(describing: self?.URLString)).")
            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Write -Result-: \(String(describing: response.description)).")
            
            /*!
             * @brief Reads recently Stored data to make it available it in the operation exchange response
             */
            self?.readData()
            
        }, onQueue: DispatchQueue.global(qos: .default))
    }
    
    private func checkAndStoreData() {
    
        // reads for an existing value. It does not overwrite any value, just stores new one if it does not exist
        CLog(type: .operation, flag: .info, level: .level1, message: "Cache Touch -Start- for: \(String(describing: URLString)).")
        cache?.touchData(forURLString: URLString!, withCallback: { [weak self] (response) in
            
            if !(self?.result().hasError() ?? false) {
                self?.result().error = response.error
            }
            
            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Touch -End- for: \(String(describing: self?.URLString)).")
            CLog(type: .operation, flag: .info, level: .level1, message: "Cache Touch -Result-: \(String(describing: response.description)).")
            
            switch response.result {
            case .notFound:
                self?.storeData()
            case .operationError,
                 .operationSucceeded:
                CLog(type: .operation, flag: .info, level: .level1, message: "Cache Existing Data for: \(String(describing: self?.URLString)).")
                self?.finish()
            @unknown default:
                fatalError("DEBUG: Cache Response Result not handled")
            }
        }, onQueue: DispatchQueue.global(qos: .default))
    }

    
    // MARK: Helper Methods
    
    private func hasDataToProcess() -> Bool {
        let hasData = result().hasData()
        let hasResult = result().hasDataObject()

        return hasData || hasResult
    }
    
    private func processTTLStorage () -> Int {
        var ttl: Int = 0
        
        guard let urlString = URLString else {
            return ttl
        }
        
        guard let cookieURL = URL.init(string: urlString) else {
            return ttl
        }
        
        // TODO: ⚠️ TTL treatment
        if let extraInfoNetworkResult: NetworkOperationResultExtraInfo = (result().extraInfo?[NetworkOperationResultExtraInfo.key()] as? NetworkOperationResultExtraInfo) {
            
            if extraInfoNetworkResult.responseURL != nil {
                
                guard let cookies = HTTPCookieStorage.shared.cookies(for: cookieURL) else {
                    return ttl;
                }
                if cookies.count > 0 {
                    guard let cookie = cookies.first else {
                        return ttl;
                    }
                    guard let expirationDate = cookie.expiresDate else {
                        return ttl;
                    }
                    
                    ttl = expirationDate.timeIntervalSinceNow > 0 ? Int(expirationDate.timeIntervalSinceNow) : 0
                }
                guard let response: HTTPURLResponse = (extraInfoNetworkResult.responseURL as? HTTPURLResponse) else {
                    return ttl;
                }
                _ = response.allHeaderFields["Cache-Control"]
            }
        }
        
        return ttl
    }
    
    private func processDataStorage() -> Data? {
        return result().data
    }
}
