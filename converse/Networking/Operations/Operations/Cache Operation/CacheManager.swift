//
//  CacheManager.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit
import SPTPersistentCache

class CacheManager {

    private let kJSONCacheId = "/com.danielgaston.converse.json.cache"
    private let kImageCacheId = "/com.danielgaston.converse.image.cache"
    private let kVideoCacheId = "/com.danielgaston.converse.video.cache"
    private let kFileCacheId = "/com.danielgaston.converse.file.cache"
    
    enum CacheType {
        case CacheTypeALL
        case CacheTypeJSON
        case CacheTypeImage
        case CacheTypeVideo
        case CacheTypeFile
    }
    
    // Swift: Use lazy to prevent the closure for being created more than once. Note ()
    private lazy var jsonCache : Cache = {
        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask, true).first
        let options = CacheOptions.init()
        options.cachePath = cachePath!
        options.cacheIdentifier = kJSONCacheId
        options.defaultExpirationPeriod = 60 * 60 * 24 * 1 // 1 days
        options.garbageCollectionInterval = UInt(1.5) * SPTPersistentCacheDefaultGCIntervalSec
        options.sizeConstraintBytes = 1024 * 1024 * 5 // 5 MiB
        options.debugOutput = { (message) -> Void in
//            print(message)
        }
        
        let cache = Cache.init(options: options)
        return cache
    }()
    
    private lazy var imageCache : Cache = {
        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask, true).first
        let options = CacheOptions.init()
        options.cachePath = cachePath!
        options.cacheIdentifier = kImageCacheId
        options.defaultExpirationPeriod = 60 * 60 * 24 * 30 // 30 days
        options.garbageCollectionInterval = UInt(1.5) * SPTPersistentCacheDefaultGCIntervalSec
        options.sizeConstraintBytes = 1024 * 1024 * 10 // 10 MiB
        options.debugOutput = { (message) -> Void in
//            print(message)
        }
        
        let cache = Cache.init(options: options)
        return cache
    }()
    
    private lazy var videoCache : Cache = {
        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask, true).first
        let options = CacheOptions.init()
        options.cachePath = cachePath!
        options.cacheIdentifier = kVideoCacheId
        options.defaultExpirationPeriod = 60 * 60 * 24 * 30 // 30 days
        options.garbageCollectionInterval = UInt(1.5) * SPTPersistentCacheDefaultGCIntervalSec
        options.sizeConstraintBytes = 1024 * 1024 * 10 // 10 MiB
        options.debugOutput = { (message) -> Void in
//            print(message)
        }
        
        let cache = Cache.init(options: options)
        return cache
    }()
    private lazy var fileCache : Cache = {
        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask, true).first
        let options = CacheOptions.init()
        options.cachePath = cachePath!
        options.cacheIdentifier = kFileCacheId
        options.defaultExpirationPeriod = 60 * 60 * 24 * 30 // 30 days
        options.garbageCollectionInterval = UInt(1.5) * SPTPersistentCacheDefaultGCIntervalSec
        options.sizeConstraintBytes = 1024 * 1024 * 10 // 10 MiB
        options.debugOutput = { (message) -> Void in
//            print(message)
        }
        
        let cache = Cache.init(options: options)
        return cache
    }()
    
    func cache(forType type: CacheType) -> [Cache] {
        
        var caches = [Cache]()
        switch (type) {
            case .CacheTypeALL:
                caches.append(jsonCache)
                caches.append(imageCache)
                caches.append(videoCache)
                caches.append(fileCache)
            
            case .CacheTypeJSON:
                caches.append(jsonCache)
            
            case .CacheTypeImage:
                caches.append(imageCache)
            
            case .CacheTypeVideo:
                caches.append(videoCache)
            
            case .CacheTypeFile:
                caches.append(fileCache)
        }
        return caches
    }
    
    func scheduleGarbageCollection(forType type: CacheType) {
        let caches:[Cache] = self.cache(forType: type)
        
        for cache in caches {
            cache.scheduleGarbageCollector()
        }
    }
    
    func unscheduleGarbageCollection(forType type: CacheType) {
        let caches:[Cache] = self.cache(forType: type)
        
        for cache in caches {
            cache.unscheduleGarbageCollector()
        }
    }
}

// MARK : Cache Adapter Classes

class CacheOptions : SPTPersistentCacheOptions {

}

typealias cacheResponseCallback = (_ response: SPTPersistentCacheResponse) -> Void
class Cache : SPTPersistentCache {
    
    func loadData(forURL URL: URL, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        return self.loadData(forKey: URL.absoluteString.MD5(), withCallback: callback, on: queue)
    }
    
    func loadData(forURLString URLString: String, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        return self.loadData(forKey: URLString.MD5(), withCallback: callback, on: queue)
    }

    func storeData(data: Data, forURL URL: URL, locked: Bool, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        
        return self.store(data, forKey: URL.absoluteString.MD5(), locked: locked, withCallback: callback, on: queue)
    }

    func storeData(data: Data, forURLString URLString: String, locked: Bool, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        return self.store(data, forKey: URLString.MD5(), locked: locked, withCallback: callback, on: queue)
    }

    func storeData(data: Data, forURL URL: URL, ttl: Int, locked: Bool, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        return self.store(data, forKey: URL.absoluteString.MD5(), ttl: UInt(ttl), locked: locked, withCallback: callback, on: queue)
    }

    func storeData(data: Data, forURLString URLString: String, ttl: Int, locked: Bool, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) -> Bool {
        return self.store(data, forKey: URLString.MD5(), ttl: UInt(ttl), locked: locked, withCallback: callback, on: queue)
    }

    func touchData(forURL URL: URL, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) {
        self.touchData(forKey: URL.absoluteString.MD5(), callback: callback, on: queue)
    }

    func touchData(forURLString URLString: String, withCallback callback: cacheResponseCallback?, onQueue queue: DispatchQueue?) {
        self.touchData(forKey: URLString.MD5(), callback: callback, on: queue)
    }
}
