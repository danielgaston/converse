//
//  ParseJSONOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public typealias ParseJSONOperationCompletion = ((ParseJSONOperation.DefaultResultType) -> Void)

open class ParseJSONOperation: ParseDataOperation<JSONOperationResult> {

    // MARK: TypeAlias
    
    public typealias DefaultResultType = JSONOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    public init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override open func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            result().responseObj = try JSONSerialization.jsonObject(with: result().data!, options: .mutableContainers)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}
