//
//  ParseDataOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

open class ParseDataOperation<ResultType: OperationResult>: ParseBaseOperation<ResultType> {
    
    // MARK: TypeAlias
        
    // MARK: Public Methods
    /*!
     * @brief Checks the opearation has not been discarded and executes the completion closure
     */
    public func reportResult() {
        DispatchQueue.main.async {
            if !self.isDiscarded && self.completion() != nil {
                self.completion()!(self.result())
            }
        }
    }
}
