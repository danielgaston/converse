//
//  ParseBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

open class ParseBaseOperation<ResultType: OperationResult>: GenericBaseOperation<ResultType> {

    // MARK: TypeAlias
    
    // MARK: Variables & Constants
    public var operationDelegate: ParseBaseOperationDelegate?
    
    // MARK: Overriden Methods
    
    override open func finish() {
        super.finish()
        
        guard isValid() else {
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .parser, flag: .error, level: .level1, message: "Failed to parse response object: \(String(describing: result().error?.localizedDescription)).")
            return
        }
        
        if startTime != nil && endTime != nil {
            let interval = endTime!.timeIntervalSince(startTime!)
            operationDelegate?.parseBaseOperation(self, reportsDuration: interval.rounded(toPlaces: 3))
        }
    }
}
