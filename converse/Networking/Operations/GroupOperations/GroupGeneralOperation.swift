//
//  GroupGeneralOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

class GroupGeneralOperation: GroupBaseOperation {

    private var internalQueue: ConverseOperationQueue?
    
    
    // MARK:- Public Methods
    
    /*!
     * @brief Runs concurrent operations on an internal queue (AdHoc)
     */
    public func runAdHocWithOperations(operations:[BaseOperation]) {
        internalQueue = ConverseOperationQueue.init()
        internalQueue?.qualityOfService = .userInitiated
        internalQueue?.isSuspended = true
        internalQueue?.name = "Internal Generic Purpose Queue"
        
        for operation in operations {
            internalQueue?.addOperation(operation)
        }
        
        internalQueue?.isSuspended = false
    }
    
    /*!
     * @brief Runs concurrent operations on a app manager queue (Shared)
     */
    public func runWithOperations(operations: [BaseOperation]) {
        for operation in operations {
            storeSubOperation(operation)
            queueController.add(operation: operation, inQueue: .compute)
        }
    }
 
    // MARK: Overriden Methods
    
    override public func cancel() {
        // affects to internal queue (AdHoc) coming from 'runWithOperations'
        internalQueue?.cancelAllOperations()
        
        // affects to computational queue (Shared) coming from 'runWithManager:operations'
        super.cancel()
    }
}
