//
//  GroupUploadOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

extension GroupBaseOperation: GroupUploadOperation {

    public func runWith(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        // No need to make use of cache ops
        let networkRequest = UploadNetworkRequest.init(toURLString: toURLString, fromFileURL: fileURL, parameters: nil, body: nil)
        let networkOp = NetworkUploadOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: completion)
        processRequest(networkRequest)
        
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
    
    public func runWith(fileData: Data, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        // No need to make use of cache ops
        let networkRequest = UploadNetworkRequest.init(toURLString: toURLString, fromData: fileData, parameters: nil, body: nil)
        let networkOp = NetworkUploadOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: completion)
        processRequest(networkRequest)
        
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
    
    /*!
     * @brief Default URLSession Upload
     * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. Upload Data must exist in some device path. There is No caching involved in this upload.
     * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     */
    public func runInBackgroundWith(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        // No need to make use of cache ops
        let networkRequest = UploadNetworkRequest.init(toURLString: toURLString, fromFileURL: fileURL, parameters: nil, body: nil)
        let networkOp = NetworkUploadOperation.init(backgroundRequest:networkRequest, operationDelegate:operationController, progress:progress, completion:completion)
        processRequest(networkRequest)
        
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
}
