//
//  GroupImageOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

extension GroupBaseOperation: GroupImageOperation {

    public func runWith(dataURLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) {
    
        if (useCache) {
            // Cache <- Network
            let readCacheOp = CacheImageOperation.init(URLString: dataURLString, locked: locked, operationDelegate: operationController)
  
            let networkRequest = ImageDataNetworkRequest.init(withURLString: dataURLString)
            let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: nil)
        
            let wrappedReadCacheOp = WrappedBaseOperation.init(readCacheOp)
            let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
            
            let CNAdapterOp = AdapterBaseOperation.init(fromOperation:wrappedReadCacheOp, toOperation:wrappedNetworkOp)
        
            // Network <- Cache
        
            let writeCacheOp = CacheImageOperation.init(URLString: dataURLString, locked: locked, operationDelegate: operationController)
            
            let wrappedWriteCacheOp = WrappedBaseOperation.init(writeCacheOp)

            let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedWriteCacheOp)
        
            // Cache <- Conversion
        
            let conversionOp = ConversionImageOperation.init(operationDelegate: operationController, completion: completion)
            
            let wrappedConversionOp = WrappedBaseOperation.init(conversionOp)
            
            let CCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedWriteCacheOp, toOperation: wrappedConversionOp)
        
            // Dependencies
        
            CNAdapterOp.addDependency(readCacheOp)
            networkOp.addDependency(CNAdapterOp)
            
            NCAdapterOp.addDependency(networkOp)
            writeCacheOp.addDependency(NCAdapterOp)
            
            CCAdapterOp.addDependency(writeCacheOp)
            conversionOp.addDependency(CCAdapterOp)
            
            storeSubOperation(readCacheOp)
            storeSubOperation(CNAdapterOp)
            storeSubOperation(networkOp)
            storeSubOperation(NCAdapterOp)
            storeSubOperation(writeCacheOp)
            storeSubOperation(CCAdapterOp)
            storeSubOperation(conversionOp)
            
            queueController.add(operation: readCacheOp, inQueue: .compute)
            queueController.add(operation: CNAdapterOp, inQueue: .compute)
            queueController.add(operation: networkOp, inQueue: .data)
            queueController.add(operation: NCAdapterOp, inQueue: .compute)
            queueController.add(operation: writeCacheOp, inQueue: .compute)
            queueController.add(operation: CCAdapterOp, inQueue: .compute)
            queueController.add(operation: conversionOp, inQueue: .compute)
        } else {
            // Network <- Conversion

            let networkRequest = ImageDataNetworkRequest.init(withURLString: dataURLString)
            let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: nil)
            
            let conversionOp = ConversionImageOperation.init(operationDelegate: operationController, completion: completion)
            
            let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
            let wrappedConversionOp = WrappedBaseOperation.init(conversionOp)
            
            let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedConversionOp)
            
            NCAdapterOp.addDependency(networkOp)
            conversionOp.addDependency(NCAdapterOp)
            
            storeSubOperation(networkOp)
            storeSubOperation(NCAdapterOp)
            storeSubOperation(conversionOp)
            
            queueController.add(operation: networkOp, inQueue: .data)
            queueController.add(operation: NCAdapterOp, inQueue: .compute)
            queueController.add(operation: conversionOp, inQueue: .compute)
        }
    }
    
    public func runWith(downloadURLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) {
    
        if (useCache) {
            // Cache <- Network
            let readCacheOp = CacheImageOperation.init(URLString: downloadURLString, locked: locked, operationDelegate: operationController)
            
            let networkRequest = ImageDownloadNetworkRequest.init(withURLString: downloadURLString)
            let networkOp = NetworkDownloadOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: nil)
            
            let wrappedReadCacheOp = WrappedBaseOperation.init(readCacheOp)
            let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
            
            let CNAdapterOp = AdapterBaseOperation.init(fromOperation:wrappedReadCacheOp, toOperation:wrappedNetworkOp)
        
            // Network <- Cache
        
            let writeCacheOp = CacheImageOperation.init(URLString: downloadURLString, locked: locked, operationDelegate: operationController)
            
            let wrappedWriteCacheOp = WrappedBaseOperation.init(writeCacheOp)

            let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedWriteCacheOp)
        
            // Cache <- Conversion
        
            let conversionOp = ConversionImageOperation.init(operationDelegate: operationController, completion: completion)
            
            let wrappedConversionOp = WrappedBaseOperation.init(conversionOp)
            
            let CCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedWriteCacheOp, toOperation: wrappedConversionOp)
        
            // Dependencies
        
            CNAdapterOp.addDependency(readCacheOp)
            networkOp.addDependency(CNAdapterOp)
            
            NCAdapterOp.addDependency(networkOp)
            writeCacheOp.addDependency(NCAdapterOp)
            
            CCAdapterOp.addDependency(writeCacheOp)
            conversionOp.addDependency(CCAdapterOp)
            
            storeSubOperation(readCacheOp)
            storeSubOperation(CNAdapterOp)
            storeSubOperation(networkOp)
            storeSubOperation(NCAdapterOp)
            storeSubOperation(writeCacheOp)
            storeSubOperation(CCAdapterOp)
            storeSubOperation(conversionOp)
            
            queueController.add(operation: readCacheOp, inQueue: .compute)
            queueController.add(operation: CNAdapterOp, inQueue: .compute)
            queueController.add(operation: networkOp, inQueue: .data)
            queueController.add(operation: NCAdapterOp, inQueue: .compute)
            queueController.add(operation: writeCacheOp, inQueue: .compute)
            queueController.add(operation: CCAdapterOp, inQueue: .compute)
            queueController.add(operation: conversionOp, inQueue: .compute)
        } else {
        
            // Network <- Conversion
            let networkRequest = ImageDownloadNetworkRequest.init(withURLString: downloadURLString)
            let networkOp = NetworkDownloadOperation.init(request: networkRequest, operationDelegate: operationController, progress: progress, completion: nil)
            
            let conversionOp = ConversionImageOperation.init(operationDelegate: operationController, completion: completion)
            
            let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
            let wrappedConversionOp = WrappedBaseOperation.init(conversionOp)
            
            let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedConversionOp)
            
            NCAdapterOp.addDependency(networkOp)
            conversionOp.addDependency(NCAdapterOp)
            
            storeSubOperation(networkOp)
            storeSubOperation(NCAdapterOp)
            storeSubOperation(conversionOp)
            
            queueController.add(operation: networkOp, inQueue: .data)
            queueController.add(operation: NCAdapterOp, inQueue: .compute)
            queueController.add(operation: conversionOp, inQueue: .compute)
        }
    }
}
