//
//  GroupDataOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

extension GroupBaseOperation: GroupDataOperation {

    // MARK: Public Methods
    
    public func runWith(networkOp: NetworkDataOperation) {
        // Network
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
    
    public func runWith<P: BaseResultOperation, OR: Any>(networkOp: NetworkDataOperation, parseOp:P) where OR == P.ResultType {
        
        guard parseOp is ParseBaseOperation<OR> else {
            fatalError("Inyected Parse operation does not inherit from ParseBaseOperation")
        }
        
        // Network <- Parse
        let wrappedParseOp = WrappedBaseOperation.init(parseOp)
        let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
        let NPAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedParseOp)
        
        NPAdapterOp.addDependency(networkOp)
        parseOp.addDependency(NPAdapterOp)
        
        storeSubOperation(networkOp)
        storeSubOperation(NPAdapterOp)
        storeSubOperation(parseOp)
        
        queueController.add(operation: networkOp, inQueue: .data)
        queueController.add(operation: NPAdapterOp, inQueue: .compute)
        queueController.add(operation: parseOp, inQueue: .compute)
    }
    
    public func runWith(readCacheOp: CacheBaseOperation,
                networkOp: NetworkDataOperation,
                writeCacheOp: CacheBaseOperation) {
        
        // Cache (read) <- Network
        let wrappedReadCacheOp = WrappedBaseOperation.init(readCacheOp)
        let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
        let CNAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedReadCacheOp, toOperation: wrappedNetworkOp)
        
        // Network <- Cache (write)
        let wrappedWriteCacheOp = WrappedBaseOperation.init(writeCacheOp)
        let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedWriteCacheOp)
        
        CNAdapterOp.addDependency(readCacheOp)
        networkOp.addDependency(CNAdapterOp)
        NCAdapterOp.addDependency(networkOp)
        writeCacheOp.addDependency(NCAdapterOp)
        
        storeSubOperation(readCacheOp)
        storeSubOperation(CNAdapterOp)
        storeSubOperation(networkOp)
        storeSubOperation(NCAdapterOp)
        storeSubOperation(writeCacheOp)
        
        queueController.add(operation: readCacheOp, inQueue: .compute)
        queueController.add(operation: CNAdapterOp, inQueue: .compute)
        queueController.add(operation: networkOp, inQueue: .data)
        queueController.add(operation: NCAdapterOp, inQueue: .compute)
        queueController.add(operation: writeCacheOp, inQueue: .compute)
    }
    
    public func runWith<P: BaseResultOperation, OR: Any>(readCacheOp: CacheBaseOperation,
                networkOp: NetworkDataOperation,
                writeCacheOp: CacheBaseOperation,
                parseOp: P) where OR == P.ResultType {
        
        guard parseOp is ParseBaseOperation<OR> else {
            fatalError("Inyected Parse operation does not inherit from ParseBaseOperation")
        }
        
        // Cache (read) <- Network
        let wrappedReadCacheOp = WrappedBaseOperation.init(readCacheOp)
        let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
        let CNAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedReadCacheOp, toOperation: wrappedNetworkOp)
        
        // Network <- Cache (write)
        let wrappedWriteCacheOp = WrappedBaseOperation.init(writeCacheOp)
        let NCAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedWriteCacheOp)
        
        // Cache (write) <- Parse
        let wrappedParseOp = WrappedBaseOperation.init(parseOp)
        let CPAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedWriteCacheOp, toOperation: wrappedParseOp)
        
        CNAdapterOp.addDependency(readCacheOp)
        networkOp.addDependency(CNAdapterOp)
        NCAdapterOp.addDependency(networkOp)
        writeCacheOp.addDependency(NCAdapterOp)
        CPAdapterOp.addDependency(writeCacheOp)
        parseOp.addDependency(CPAdapterOp)
        
        storeSubOperation(readCacheOp)
        storeSubOperation(CNAdapterOp)
        storeSubOperation(networkOp)
        storeSubOperation(NCAdapterOp)
        storeSubOperation(writeCacheOp)
        storeSubOperation(CPAdapterOp)
        storeSubOperation(parseOp)
        
        queueController.add(operation: readCacheOp, inQueue: .compute)
        queueController.add(operation: CNAdapterOp, inQueue: .compute)
        queueController.add(operation: networkOp, inQueue: .data)
        queueController.add(operation: NCAdapterOp, inQueue: .compute)
        queueController.add(operation: writeCacheOp, inQueue: .compute)
        queueController.add(operation: CPAdapterOp, inQueue: .compute)
        queueController.add(operation: parseOp, inQueue: .compute)
    }
}
