//
//  GroupDownloadOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

extension GroupBaseOperation: GroupDownloadOperation {

    /*!
     * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
     * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    
    public func runWith(URLString: String, destinationFolder: FileManager.SearchPathDirectory, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        
        // No need to make use of cache ops
        let networkRequest = DownloadNetworkRequest.init(withURLString: URLString)
        let networkOp = NetworkDownloadOperation.init(request: networkRequest, operationDelegate: operationController, destinationFolder: destinationFolder, progress: progress, completion: completion)
        
        processRequest(networkRequest)
        
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
    
    
    /*!
     * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
     * @discussion Download will be executing in background even if the app is suspended. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */

    public func runInBackgroundWith(URLString: String, destinationFolder: FileManager.SearchPathDirectory, progress: NetworkOperationProgress?,
                             completion: NetworkOperationCompletion?) {
        
        // No need to make use of cache ops
        let networkRequest = DownloadNetworkRequest.init(withURLString: URLString)
        let networkOp = NetworkDownloadOperation.init(backgroundRequest: networkRequest, operationDelegate: operationController, destinationFolder: destinationFolder, progress: progress, completion: completion)

        processRequest(networkRequest)
        
        storeSubOperation(networkOp)
        queueController.add(operation: networkOp, inQueue: .data)
    }
}
