//
//  GroupBaseOperationProtocols.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

protocol GroupDataOperation {
    
    func runWith(networkOp: NetworkDataOperation)
    func runWith<P: BaseResultOperation, OR: Any>(networkOp: NetworkDataOperation, parseOp:P) where OR == P.ResultType
    func runWith(readCacheOp: CacheBaseOperation,
                        networkOp: NetworkDataOperation,
                        writeCacheOp: CacheBaseOperation)
    func runWith<P: BaseResultOperation, OR: Any>(readCacheOp: CacheBaseOperation,
                                                         networkOp: NetworkDataOperation,
                                                         writeCacheOp: CacheBaseOperation,
                                                         parseOp: P) where OR == P.ResultType
}

// MARK: -

protocol GroupDownloadOperation {
    
    func runWith(URLString: String, destinationFolder: FileManager.SearchPathDirectory, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?)
    
    func runInBackgroundWith(URLString: String, destinationFolder: FileManager.SearchPathDirectory, progress: NetworkOperationProgress?,
                                    completion: NetworkOperationCompletion?)
}

// MARK: -

protocol GroupUploadOperation {
    
    func runWith(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?)
    
    func runWith(fileData: Data, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?)
    
    func runInBackgroundWith(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?)
}

// MARK: -

protocol GroupImageOperation {
    
    func runWith(dataURLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion)
    func runWith(downloadURLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion)
}

// MARK: -
