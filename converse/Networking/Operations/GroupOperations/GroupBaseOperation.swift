//
//  GroupBaseOperation.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public protocol GroupBaseOperationPr: AnyObject {
    func groupUUID() -> UUID
    func cancel()
    func cancelAndDiscard()
    func storeSubOperation(_ operation: BaseOperation?)
    func processRequest(_ request: NetworkRequest?)
    func storeRequest(_ request: NetworkRequest?)
    func containsRequest(_ request: NetworkRequest?) -> Bool
}

// MARK:-

open class GroupBaseOperation: GroupBaseOperationPr {
    
    // MARK:- Private Wrappers. NSHashTable requires working with objects instead protocols
    
    private class BaseOperationWrapper {
        weak var operation: BaseOperation?
        init(_ op: BaseOperation?) {
            operation = op
        }
    }
    
    private class NetworkRequestWrapper {
        weak var request: NetworkRequest?
        init(_ req: NetworkRequest?) {
            request = req
        }
    }
    
    // MARK:-
    
    public var queueController: QueueControllerPr
    public var operationController: OperationControllerPr
    
    let baseUUID: UUID
    private let weakOpTable: NSHashTable<BaseOperationWrapper>
    private let weakOpRequestTable: NSHashTable<NetworkRequestWrapper>
    
    // MARK:- Object Life cycle Methods

    public init(queueController _queueController: QueueControllerPr, customOperationController _customOperationController: OperationControllerPr) {
        baseUUID = UUID.init()
        weakOpTable = NSHashTable<BaseOperationWrapper>.init()
        weakOpRequestTable = NSHashTable<NetworkRequestWrapper>.init()
        queueController = _queueController
        operationController = _customOperationController
        
        queueController.storeGroupOperation(self)
    }
    
    // MARK:- GroupBaseOperations Protocol Methods
    
    public func groupUUID() -> UUID {
        return baseUUID
    }
    
    public func cancel() {
        weakOpTable.allObjects.reversed().forEach { (baseOperationWrapper) in
            baseOperationWrapper.operation?.cancel()
        }
    }
    
    public func cancelAndDiscard() {
        weakOpTable.allObjects.reversed().forEach { (baseOperationWrapper) in
            baseOperationWrapper.operation?.isDiscarded = false
            baseOperationWrapper.operation?.cancel()
        }
    }
    
    public func storeSubOperation(_ operation: BaseOperation?) {
        weakOpTable.add(BaseOperationWrapper.init(operation))
    }
    
    public func processRequest(_ request: NetworkRequest?) {
        if request != nil {
            queueController.cancelGroupOperationIfMatchesRequest(request!)
            storeRequest(request)
        }
    }
    
    public func storeRequest(_ request: NetworkRequest?) {
        if request != nil {
            weakOpRequestTable.add(NetworkRequestWrapper.init(request))
        }
    }
    
    public func containsRequest(_ request: NetworkRequest?) -> Bool {
        var contained = false
        
        guard request != nil else {
            return false
        }
        
        weakOpRequestTable.allObjects.reversed().forEach { (networkRequestWrapper) in
            
            if let storedRequest = networkRequestWrapper.request {
                if storedRequest.isEqualTo(request!) {
                    contained = true
                }
            }
        }
        return contained
    }
}
