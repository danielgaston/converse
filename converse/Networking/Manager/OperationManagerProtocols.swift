//
//  OperationManagerProtocols.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

public protocol OperationManagerDelegate: AnyObject {
    // scalation ready
}

// MARK:-

public protocol QueueControllerPr: AnyObject {
    func add(operation: BaseOperation, inQueue type: QueueType)
    func storeGroupOperation(_ groupOp: GroupBaseOperationPr)
    func cancelAllOperations(inQueue type: QueueType)
    func cancelGroupOperationIfMatchesRequest(_ request: NetworkRequest)
    func currentConcurrentOpsInQueue(_ type: QueueType) -> Int
}

// MARK:-

public protocol OperationControllerPr: NetworkBaseOperationDelegate, ParseBaseOperationDelegate, CacheBaseOperationDelegate, ConversionBaseOperationDelegate, (URLSessionDelegate & URLSessionTaskDelegate & URLSessionDataDelegate & URLSessionDownloadDelegate) {
    
    var defaultSession: (URLSession & NetworkOperationClientPr)? { get set }
    var backgroundSession: (URLSession & NetworkOperationClientPr)? { get set }
    
    func handleEventsForBackgroundURLSession(identifier: String, completion: @escaping () -> Void)
    
    func resetDefaultSession(completion: @escaping () -> Void)
    func resetBackgroundSession(completion: @escaping () -> Void)
}

// MARK:-

public protocol OperationManagerPr: AnyObject {
    var queueController: QueueControllerPr { get }
    var operationController: OperationControllerPr { get }
}

// MARK:-

public protocol URLOperationManager {
    
    // MARK: - RESET
    
    func reset(completion: @escaping () -> Void)
    
    // MARK: - DATA
    
    func data(_ URLString: String, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func data(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func data(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func data(_ URLString: String, useCache: Bool , locked: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - UPLOAD
    
    func upload(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func upload(fileURL: URL, toURLString: String, inBackground: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func upload(fileData: Data, toURLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - DOWNLOAD
    
    func downloadJSON(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr
    
    func downloadJSON(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr
    
    func downloadJSON(_ URLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr
    
    func downloadDataImage(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr
    
    func downloadDataImage(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr
    
    func downloadDataImage(_ URLString: String, useCache: Bool, locked: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr
    
    func downloadFile(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func downloadFile(_ URLString: String, inBackground: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func downloadFile(_ URLString: String, inBackground: Bool, toDocumentsFolder: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - Background Task
    
    func handleEventsForBackgroundURLSession(identifier: String, completion: @escaping () -> Void)
}

public protocol HTTPOperationManager {
    
    // MARK: - GET
    
    func GET(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func GET(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func GET(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    
    // MARK: - HEAD
    
    func HEAD(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func HEAD(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func HEAD(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - POST
    
    func POST(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func POST(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func POST(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    
    // MARK: - PUT
    
    func PUT(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func PUT(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func PUT(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - PATCH
    
    func PATCH(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func PATCH(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func PATCH(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    // MARK: - DELETE
    
    func DELETE(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func DELETE(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
    
    func DELETE(_ URLString: String, parameters: RequestParameters?, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr
}

public protocol ConcurrentOperationManager {
    
    func runConcurrentOperations(operations: [BaseOperation]) -> GroupBaseOperationPr
    
    func cancelAllRequests()
    
    func currentConcurrentOpsInQueues() -> Int
}
