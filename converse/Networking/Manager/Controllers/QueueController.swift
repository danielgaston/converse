//
//  QueueManager.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public enum QueueType {
    case data
    case background
    case compute
    case all
}

extension Notification.Name {
    static let networkActivityDidStart = Notification.Name("networkActivityDidStart")
    static let networkActivityDidFinish = Notification.Name("networkActivityDidFinish")
}

public class QueueController: QueueControllerPr {
    
    /**
     @warning: reducing MAX concurrent operations (i.e. 2), make NETWORK Operations (i.e.Download) no to be finished, properly cancelled actually. And this BLOCKS the whole queue as the operations dont get released!! Leave this values to SystemMax for now. The problem needs to be addressed on operations, either APINetworkBase or APIBase to fix how to FINISH an operation which is not READY nor EXECUTING. Seems that cancelling an op
     */
    let DataQueueMaxConcurrentOperationDefaultMode: Int = OperationQueue.defaultMaxConcurrentOperationCount
    let BackgroundQueueMaxConcurrentOperationDefaultMode: Int = OperationQueue.defaultMaxConcurrentOperationCount
    
    var dataQueue: OperationQueue
    var backgroundQueue: OperationQueue
    var computeQueue: OperationQueue
    
    var weakOpTable: NSHashTable<GroupBaseOperation>
    
    var hashTableThreadSafeQueue: DispatchQueue

    var observers: [NSKeyValueObservation?]
    
    // MARK: Object Life Cycle Methods
    
    init() {
        weakOpTable = NSHashTable.weakObjects()
        hashTableThreadSafeQueue = DispatchQueue(label: "com.danielgaston.converse.queue_controller_serial_queue")
        observers = [NSKeyValueObservation?]()
        
        // dataQueue
        dataQueue = OperationQueue()
        dataQueue.name = "Data Queue"
        // https://developer.apple.com/documentation/foundation/qualityofservice
        dataQueue.qualityOfService = .userInitiated
        dataQueue.maxConcurrentOperationCount = DataQueueMaxConcurrentOperationDefaultMode
        
        // backgroundQueue
        backgroundQueue = OperationQueue()
        backgroundQueue.name = "Background Queue"
        // https://developer.apple.com/documentation/foundation/qualityofservice
        backgroundQueue.qualityOfService = .utility
        backgroundQueue.maxConcurrentOperationCount = BackgroundQueueMaxConcurrentOperationDefaultMode

        // computeQueue
        computeQueue = OperationQueue()
        computeQueue.name = "Compute Queue"
        // https://developer.apple.com/documentation/foundation/qualityofservice
        computeQueue.qualityOfService = .default
        computeQueue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
        
        // we can call instance methods from this point 'cause all variables are initialized already
        initializeObservers()
    }
    
    deinit {
        deinitializeObservers()
    }
    
    // MARK: Initialization Methods
    
    func initializeObservers() {
        for queue in queues(forType: .all) {
            let observation1: NSKeyValueObservation? = queue.observe(\.operationCount, options: [.new, .old], changeHandler: { [weak self] (model, change) in
                self?.logChange(forQueue: model)
            })
            let observation2: NSKeyValueObservation? = queue.observe(\.maxConcurrentOperationCount, options: [.new, .old], changeHandler: { [weak self] (model, change) in
                self?.logChange(forQueue: model)
            })
            let observation3: NSKeyValueObservation? = queue.observe(\.isSuspended, options: [.new, .old], changeHandler: { [weak self] (model, change) in
                self?.logChange(forQueue: model)
            })
            observers.append(contentsOf:([observation1, observation2, observation3]))
        }
    }
    
    func deinitializeObservers() {
        observers.forEach { (observer) in
            observer?.invalidate()
        }
    }
    
    // MARK: - QueueControllerPr Methods
    
    
    public func add(operation: BaseOperation, inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.addOperation(operation)
        }
    }
    
    public func storeGroupOperation(_ groupOp: GroupBaseOperationPr) {
        // Serial Queue processing requires to use "sync"
        hashTableThreadSafeQueue.sync {
            weakOpTable.add(groupOp as? GroupBaseOperation)
        }
    }
    
    /*!
     * @brief Cancels all queued and executing operations.
     */
    public func cancelAllOperations(inQueue type: QueueType) {
        hashTableThreadSafeQueue.sync {
            for queue in queues(forType: type) {
                queue.cancelAllOperations()
            }
        }
    }
    
    /*!
     * @brief Cancels all suboperations of a group operation, if any network suboperation shares the SAME request (URLRequest), and makes sure the cancelled ones does not execute its completion block.
     * @discussion check NSOperation (Discardable) for furher details
     */
    
    public func cancelGroupOperationIfMatchesRequest(_ request: NetworkRequest) {
        hashTableThreadSafeQueue.sync {
            for groupOp in weakOpTable.allObjects.reversed() {
                if groupOp.containsRequest(request) {
                    groupOp.cancelAndDiscard()
                    CLog(type: .queue, flag: .info, level: .level0, message: "Duplicated Running Request Discarded \(String(describing: request.absoluteURL?.absoluteString)).")
                }
            }
        }
    }
    
    public func currentConcurrentOpsInQueue(_ type: QueueType) -> Int {
        assert(type != .all, "Please do not use .ALL type for a getter")
        
        let queue = queues(forType: type).first
        return queue?.operationCount ?? 0
    }
    
    // MARK: Queue Methods
    
    public func maxConcurrentOps(inQueue type: QueueType) -> Int {
        assert(type != .all, "Please do not use .ALL type for a getter")
        
        let queue = queues(forType: type).first
        return queue?.maxConcurrentOperationCount ?? 0
    }
    
    public func setMaxConcurrentOps(maxOperations maxOps: Int, inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.maxConcurrentOperationCount = maxOps
        }
    }
    
    public func qualityOfService(inQueue type: QueueType) -> QualityOfService? {
        assert(type != .all, "Please do not use .ALL type for a getter")
        
        let queue = queues(forType: type).first
        return queue?.qualityOfService
    }
    
    public func setQualityOfService(qos: QualityOfService, inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.qualityOfService = qos
        }
    }
    
    /*!
     * @brief Prevents the queue from starting any queued operations
     * @warning already executing operations continue to execute. You may continue to add operations to a queue that is suspended but those operations are not scheduled for execution until the queue is resumed
     */
    public func pauseAllOperations(inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.isSuspended = true
        }
    }
    
    /*!
     * @brief Prevents the queue from starting any queued operations
     * @warning already executing operations continue to execute. You may continue to add operations to a queue that is suspended but those operations are not scheduled for execution until the queue is resumed
     */
    public func resumeAllOperations(inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.isSuspended = false
        }
    }
    
    /*!
     * @brief Blocks the queue thread until all pending and executing operations are finished
     * @warning During this time, add operations is not permitted.
     */
    public func waitUntilAllDataOpsAreFinished(inQueue type: QueueType) {
        for queue in queues(forType: type) {
            queue.waitUntilAllOperationsAreFinished()
        }
    }
    
    // MARK: Helper Methods

    private func postNetworkNotification() {
        
        if (dataQueue.operationCount > 0 || backgroundQueue.operationCount > 0) {
            NotificationCenter.default.post(name: .networkActivityDidStart, object: nil)
        } else {
            NotificationCenter.default.post(name: .networkActivityDidFinish, object: nil)
        }
    }
    
    private func queues(forType type: QueueType) -> [OperationQueue] {
        var queues = [OperationQueue]()
        
        switch type {
            case .data:
                queues.append(dataQueue)
            case .background:
                queues.append(backgroundQueue)
            case .compute:
                queues.append(computeQueue)
            case .all:
                queues.append(dataQueue)
                queues.append(backgroundQueue)
                queues.append(computeQueue)
        }
        
        return queues
    }
    
    private func logChange(forQueue queue: OperationQueue) {
        var logString = ""
        logString.append("\n")
        logString.append("\n<<<<---- \(queue.name ?? "N/A") ----->>>>")
        logString.append("\n\(queue.name ?? "N/A") Max Concurrent Ops: \(verboseMaxCurrentOps(queue.maxConcurrentOperationCount))")
        logString.append("\n\(queue.name ?? "N/A") Cur Concurrent Ops: \(queue.operationCount)")
        logString.append("\n\(queue.name ?? "N/A") Quality of Service: \(verboseQualityOfService(queue.qualityOfService))")
        logString.append("\n\(queue.name ?? "N/A") Suspended: \(queue.isSuspended)")
        logString.append("\n\(queue.name ?? "N/A") Operations: \(queue.operations)")
        logString.append("\n\n")
        
        CLog(type: .queue, flag: .info, level: .level0, message: logString)
        
        DispatchQueue.main.async {
            self.postNetworkNotification()
        }
    }
    
    private func verboseMaxCurrentOps(_ maxOps: Int) -> String {
        switch maxOps {
        case OperationQueue.defaultMaxConcurrentOperationCount:
            return "System Max"
        default:
            return String(maxOps)
        }
    }
    
    private func verboseQualityOfService(_ qos: QualityOfService) -> String {
        switch qos {
        case .userInteractive:
            return "User Interactive"
        case .userInitiated:
            return "User Initiated"
        case .utility:
            return "Utility"
        case .background:
            return "Background"
        case .default:
            return "Default"
        default:
            return ""
        }
    }
}
