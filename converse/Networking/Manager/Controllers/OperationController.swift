//
//  OperationController.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public class OperationController: NSObject, OperationControllerPr {

    public var defaultSession: (URLSession & NetworkOperationClientPr)?
    public var backgroundSession: (URLSession & NetworkOperationClientPr)?
    
    private class CompletionWrapper {
        var closure: NetworkOperationCompletion?
        // MARK: Object LifeCycle Methods
        init(_ completionClosure: NetworkOperationCompletion?) {
            closure = completionClosure
        }
    }
    
    private var weakTaskToStrongOpMapTable: NSMapTable<URLSessionTask, NetworkBaseOperation>
    // Completion handlers for background URLSession tasks. In Swift we need to wrap completions in classes (OperationManager.ClosureWrapper) to satisfy NSMapTable constraints (needs to store classes instances only)
    private var weakTaskToWeakCompletionsMapTable: NSMapTable<URLSessionTask, CompletionWrapper>
    
    private let mapTableThreadSafeQueue = DispatchQueue(label: "com.danielgaston.converse.operation_controller_serial_queue")
    
    
    override public init() {
        
        weakTaskToStrongOpMapTable = NSMapTable.weakToStrongObjects()
        weakTaskToWeakCompletionsMapTable = NSMapTable.weakToWeakObjects()
        
        super.init()
    }
    
    // MARK: - Helper Methods (Thread Safe - Operations Table)

    private func store(operation op: NetworkBaseOperation, forTask task: URLSessionTask) {
        mapTableThreadSafeQueue.sync {
            weakTaskToStrongOpMapTable.setObject(op, forKey: task)
        }
    }
    
    private func operation(forTask task: URLSessionTask) -> NetworkBaseOperation? {
        mapTableThreadSafeQueue.sync {
            return weakTaskToStrongOpMapTable.object(forKey: task)
        }
    }
    
    private func removeOperation(forTask task: URLSessionTask) {
        mapTableThreadSafeQueue.sync {
            weakTaskToStrongOpMapTable.removeObject(forKey: task)
        }
    }
    
    private func removeAllOperation() {
        mapTableThreadSafeQueue.sync {
            weakTaskToStrongOpMapTable.removeAllObjects()
        }
    }

    // MARK: - Helper Methods (Thread Safe - Completions Table)
    
    private func store(completion: CompletionWrapper, forTask task: URLSessionTask) {
        mapTableThreadSafeQueue.sync {
            weakTaskToWeakCompletionsMapTable.setObject(completion, forKey: task)
        }
    }
    
    private func completion(forTask task: URLSessionTask) -> CompletionWrapper? {
        mapTableThreadSafeQueue.sync {
            weakTaskToWeakCompletionsMapTable.object(forKey: task)
        }
    }
    
    private func removeCompletion(forTask task: URLSessionTask) {
        mapTableThreadSafeQueue.sync {
            weakTaskToWeakCompletionsMapTable.removeObject(forKey: task)
        }
    }
    
    private func removeAllCompletion() {
        mapTableThreadSafeQueue.sync {
            weakTaskToWeakCompletionsMapTable.removeAllObjects()
        }
    }
    
    // MARK: - RESET
    
    /*!
    * @brief Empties all cookies, caches and credential stores, removes disk files, flushes in-progress downloads to disk, and ensures that future requests occur on a new socket.
    */
    public func resetDefaultSession(completion: @escaping () -> Void) {
        defaultSession?.reset(completionHandler: completion)
    }
    
    /*!
    * @brief Empties all cookies, caches and credential stores, removes disk files, flushes in-progress downloads to disk, and ensures that future requests occur on a new socket.
    */
    public func resetBackgroundSession(completion: @escaping () -> Void) {
        backgroundSession?.reset(completionHandler: completion)
    }

    
    // MARK: - Network Background Task
    
    public func handleEventsForBackgroundURLSession(identifier: String, completion: @escaping () -> Void) {
        
        backgroundSession?.getTasksWithCompletionHandler{ [weak self] (tasks, uploads, downloads) in
            
            let allSessionTasks: [URLSessionTask] = [tasks, uploads, downloads].compactMap({ $0 }) as! [URLSessionTask]
            for sessionTask in allSessionTasks {
                
                let op = self?.operation(forTask: sessionTask)
                let sessionCompletionWrapper = self?.completion(forTask: sessionTask)
                
                guard let sessionCompletionHandler = sessionCompletionWrapper?.closure else {
                    return
                }
                
                self?.removeCompletion(forTask: sessionTask)
                
                guard let result = op?.result() else {
                    return
                }
                
                DispatchQueue.main.async {
                    sessionCompletionHandler(result)
                    completion()
                }
            }
        }
    }
    
    // MARK: - NetworkBaseOperationDelegate Methods
    
    public func networkBaseOperationRequestDefaultSession(_ op: NetworkBaseOperation) -> URLSession {
        if defaultSession == nil {
            defaultSession = NetworkOperationClient.defaultClientWithDelegate(self)
        }
        return defaultSession!
    }
    
    /*!
     * @warning There must be only one background session instance in the App, independently from where it was instanced! As long as we use single NetworkManager via dependency injection there wont be any problem, however, in case the app requires two or more NetworkManager (undesired), background downloads/uploads wont work as it would necessarily require to create a new instance of backgroundSession showing the next error: A background URLSession with identifier ConverseBackgroundOperationSession already exists!
     */
    public func networkBaseOperationRequestBackgroundSession(_ op: NetworkBaseOperation) -> URLSession {
        if backgroundSession == nil {
            backgroundSession = NetworkOperationClient.backgroundClientWithDelegate(self)
        }
        return backgroundSession!
    }
    
    public func networkBaseOperation(_ op: NetworkBaseOperation, didStartWithTask task: URLSessionTask?) {
        guard let task = task else {
            return
        }
        self.store(operation: op, forTask: task)
    }
    
    public func networkBaseOperation(_ op: NetworkBaseOperation, didDiscardTask task: URLSessionTask?) {
        guard let task = task else {
            return
        }
         self.removeOperation(forTask: task)
    }
    
    public func networkBaseOperation(_ op: NetworkBaseOperation, requiresStoreCompletion completion: NetworkOperationCompletion?, forTask task: URLSessionTask?) {
        guard let task = task else {
            return
        }
        store(completion: OperationController.CompletionWrapper.init(completion), forTask: task)
    }
    
    public func networkBaseOperation(_ op: Operation, reportsKBperSecond KBperSec: Double) {
        CLog(type: .operation, flag: .info, level: .level2, message: (op.name ?? "N/A") + " -- TRANSFER SPEED (KB/s): \(KBperSec.rounded(toPlaces: 2))")
    }
    
    
    // MARK: - ParseBaseOperationDelegate Methods
    
    public func parseBaseOperation(_ op: BaseOperation, reportsDuration duration: TimeInterval) {
        CLog(type: .operation, flag: .info, level: .level2, message: (op.name ?? "N/A") + " -- PARSE DURATION (s): \(duration)")
    }
    
    // MARK: - CacheBaseOperationDelegate Methods
    
    public func cacheBaseOperation(_ op: CacheBaseOperation, reportsDuration duration: TimeInterval) {
        CLog(type: .operation, flag: .info, level: .level2, message: (op.name ?? "N/A") + " -- CACHE DURATION (s): \(duration)")
    }
    
    // MARK: - ConversionBaseOperationDelegate Methods
    
    public func conversionBaseOperation(_ op: BaseOperation, reportsDuration duration: TimeInterval) {
        CLog(type: .operation, flag: .info, level: .level2, message: (op.name ?? "N/A") + " -- CONVERSION DURATION (s): \(duration)")
    }
    
    // MARK: - URLSessionDelegate Methods
    
    /*!
     * @brief The last message a session receives.  A session will only become invalid because of a systemic error or when it has been explicitly invalidated, in which case the error parameter will be nil.
     */
    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        CLog(type: .operation, flag: .error, level: .level1, message: "URLSession: " + (session.sessionDescription ?? "Unnamed Session") + " has been INVALIDATED.")
    }
    
    /*!
     * @brief If an application has received an -application:handleEventsForBackgroundURLSession:completionHandler: message, the session delegate will receive this message to indicate that all messages previously enqueued for this session have been delivered.  At this time it is safe to invoke the previously stored completion handler, or to begin any internal updates that will result in invoking the completion handler.
     * @discussion called after finishing a bg task in bg (while app was not in foreground).
     */
    public func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        CLog(type: .operation, flag: .error, level: .level1, message: "URLSession: \(String(describing:session.sessionDescription)) did FINISH EVENTS.")
    }
    
    /*!
     * @brief If implemented, when a connection level authentication challenge has occurred, this delegate will be given the opportunity to provide authentication credentials to the underlying connection. Some types of authentication will apply to more than one request on a given connection to a server (SSL Server Trust challenges).  If this delegate message is not implemented, the behavior will be to use the default handling, which may involve user interaction.
     */
    // TODO Proper handling with UI (alert) involved. Check difference with similar NSURLSessionTaskDelegate method
    //    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
    //        CLog(type: .operation, flag: .error, level: .level1, message: "URLSession: \(String(describing:session.sessionDescription)) did RECEIVE CREDENTIAL CHALLENGE.")
    //    }
    
    // MARK: - URLSessionTaskDelegate Methods
    
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didCompleteWithError error: Error?) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        // In general the use of weak-to-strong map tables is not recommended. The strong values for weak keys which get zeroed out continue to be maintained until the map table RESIZES itself (adding or removing elements). The reason of strongify values is that some network ops were dealloc before reaching '- (void)URLSession:task:didCompleteWithError:' in this class, so that the client never was receiving the operation result. Thats why remove the task (key) at this point, to release the operation (value) that otherwise would never be released because an NSURLSession holds all created NSURLSessionTasks (finished/unfinished) until it gets invalidated (- (void)invalidateAndCancel or - (void)finishTasksAndInvalidate).
        self.removeOperation(forTask: task)
        op?.urlSession?(session, task: task, didCompleteWithError: error)
    }
    
    /*!
     * @brief This method is called only for tasks in default and ephemeral sessions. Tasks in background sessions automatically follow redirects.
     */
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           willPerformHTTPRedirection response: HTTPURLResponse,
                           newRequest request: URLRequest,
                           completionHandler: @escaping (URLRequest?) -> Void) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, task: task, willPerformHTTPRedirection: response, newRequest: request, completionHandler: completionHandler)
    }
    
    /*!
     * @brief Sent periodically to notify the delegate of upload progress. This information is also available as properties of the task.
     */
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didSendBodyData bytesSent: Int64,
                           totalBytesSent: Int64,
                           totalBytesExpectedToSend: Int64) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, task: task, didSendBodyData: bytesSent, totalBytesSent: totalBytesSent, totalBytesExpectedToSend: totalBytesExpectedToSend)
    }
    
    /*!
     * @brief Sent if a task requires a new, unopened body stream.  This may be necessary when authentication has failed for any request that involves a body stream.
     */
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           needNewBodyStream completionHandler: @escaping (InputStream?) -> Void) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, task: task, needNewBodyStream: completionHandler)
    }
    
    /*!
     * @brief The task has received a request specific authentication challenge. If this delegate is not implemented, the session specific authentication challenge will *NOT* be called and the behavior will be the same as using the default handling disposition.
     */
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didReceive challenge: URLAuthenticationChallenge,
                           completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, task: task, didReceive: challenge, completionHandler: completionHandler)
    }
    
    
    /*!
     * @brief This method is called when a background session task with a delayed start time (as set with the earliestBeginDate property) is ready to start. This delegate method should only be implemented if the request might become stale while waiting for the network load and needs to be replaced by a new request. For loading to continue, the delegate must call the completion handler, passing in a disposition that indicates how the task should proceed. Passing the NSURLSessionDelayedRequestCancel disposition is equivalent to calling cancel on the task directly.
     */
    /*
     func urlSession(_ session: URLSession,
     task: URLSessionTask,
     willBeginDelayedRequest request: URLRequest,
     completionHandler: @escaping (URLSession.DelayedRequestDisposition, URLRequest?) -> Void) {
     
     let op = self.operation(forTask: task)
     
     if let _ = op?.responds(to: #selector(URLSessionTaskDelegate.urlSession(_:task:willBeginDelayedRequest:completionHandler:))) {
     op?.urlSession?(session, task: task, willBeginDelayedRequest: request, completionHandler: completionHandler)
     }
     }
     */
    
    /*!
     * @brief Sent when a task cannot start the network loading process because the current network connectivity is not available or sufficient for the task's request. This delegate will be called at most one time per task, and is only called if the waitsForConnectivity property in the NSURLSessionConfiguration has been set to YES. This delegate callback will never be called for background sessions, because the waitForConnectivity property is ignored by those sessions.
     */
    public func urlSession(_ session: URLSession,
                           taskIsWaitingForConnectivity task: URLSessionTask) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, taskIsWaitingForConnectivity: task)
    }
    
    /*!
     * @brief Sent when complete statistics information has been collected for the task.
     */
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didFinishCollecting metrics: URLSessionTaskMetrics) {
        
        let op: URLSessionTaskDelegate? = self.operation(forTask: task)
        op?.urlSession?(session, task: task, didFinishCollecting: metrics)
    }
    
    // MARK: - URLSessionDataDelegate Methods
    
    /*!
     * @brief The task has received a response and no further messages will be received until the completion block is called. The disposition allows you to cancel a request or to turn a data task into a download task. This delegate message is optional - if you do not implement it, you can get the response as a property of the task. This method will not be called for background upload tasks (which cannot be converted to download tasks).
     */
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didReceive response: URLResponse,
                           completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        let op: URLSessionDataDelegate? = self.operation(forTask: dataTask) as? URLSessionDataDelegate
        op?.urlSession?(session, dataTask: dataTask, didReceive: response, completionHandler: completionHandler)
    }
    
    /*!
     * @brief Notification that a data task has become a download task. No future messages will be sent to the data task.
     */
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didBecome downloadTask: URLSessionDownloadTask) {
        
        let op: URLSessionDataDelegate? = self.operation(forTask: dataTask) as? URLSessionDataDelegate
        op?.urlSession?(session, dataTask: dataTask, didBecome: downloadTask)
    }
    
    /*!
     * @brief Notification that a data task has become a bidirectional stream task.  No future messages will be sent to the data task.  The newly created streamTask will carry the original request and response as properties. For requests that were pipelined, the stream object will only allow reading, and the object will immediately issue a -URLSession:writeClosedForStream:.  Pipelining can be disabled for all requests in a session, or by the NSURLRequest HTTPShouldUsePipelining property. The underlying connection is no longer considered part of the HTTP connection cache and won't count against the total number of connections per host.
     */
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didBecome streamTask: URLSessionStreamTask) {
        
        let op: URLSessionDataDelegate? = self.operation(forTask: dataTask) as? URLSessionDataDelegate
        op?.urlSession?(session, dataTask: dataTask, didBecome: streamTask)
    }
    
    /*!
     * @brief Sent when data is available for the delegate to consume.  It is assumed that the delegate will retain and not copy the data.  As the data may be discontiguous, you should use [NSData enumerateByteRangesUsingBlock:] to access it.
     */
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didReceive data: Data) {
        
        let op: URLSessionDataDelegate? = self.operation(forTask: dataTask) as? URLSessionDataDelegate
        op?.urlSession?(session, dataTask: dataTask, didReceive: data)
    }
    
    /*!
     * @brief Invoke the completion routine with a valid NSCachedURLResponse to allow the resulting data to be cached, or pass nil to prevent caching. Note that there is no guarantee that caching will be attempted for a given resource, and you should not rely on this message to receive the resource data.
     */
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           willCacheResponse proposedResponse: CachedURLResponse,
                           completionHandler: @escaping (CachedURLResponse?) -> Void) {
        
        let op: URLSessionDataDelegate? = self.operation(forTask: dataTask) as? URLSessionDataDelegate
        op?.urlSession?(session, dataTask: dataTask, willCacheResponse: proposedResponse, completionHandler: completionHandler)
    }
    
    // MARK: - URLSessionDownloadDelegate Methods
    
    /*!
     * @brief Sent when a download task that has completed a download.  The delegate should copy or move the file at the given location to a new location as it will be removed when the delegate message returns. URLSession:task:didCompleteWithError: will still be called.
     */
    public func urlSession(_ session: URLSession,
                           downloadTask: URLSessionDownloadTask,
                           didFinishDownloadingTo location: URL) {
        
        let op: URLSessionDownloadDelegate? = self.operation(forTask: downloadTask) as? URLSessionDownloadDelegate
        op?.urlSession(session, downloadTask: downloadTask, didFinishDownloadingTo: location)
    }
    
    /*!
     * @brief Sent when a download has been resumed. If a download failed with an error, the -userInfo dictionary of the error will contain an NSURLSessionDownloadTaskResumeData key, whose value is the resume data.
     */
    public func urlSession(_ session: URLSession,
                           downloadTask: URLSessionDownloadTask,
                           didResumeAtOffset fileOffset: Int64,
                           expectedTotalBytes: Int64) {
        
        let op: URLSessionDownloadDelegate? = self.operation(forTask: downloadTask) as? URLSessionDownloadDelegate
        op?.urlSession?(session, downloadTask: downloadTask, didResumeAtOffset: fileOffset, expectedTotalBytes: expectedTotalBytes)
    }
    
    /*!
     * @brief Sent periodically to notify the delegate of download progress.
     */
    public func urlSession(_ session: URLSession,
                           downloadTask: URLSessionDownloadTask,
                           didWriteData bytesWritten: Int64,
                           totalBytesWritten: Int64,
                           totalBytesExpectedToWrite: Int64) {
        
        let op: URLSessionDownloadDelegate? = self.operation(forTask: downloadTask) as? URLSessionDownloadDelegate
        op?.urlSession?(session, downloadTask: downloadTask, didWriteData: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpectedToWrite: totalBytesExpectedToWrite)
    }
}
