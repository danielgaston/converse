//
//  OperationManager.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/// Swift: URLSessionTaskDelegate, URLSessionDataDelegate, URLSessionDownloadDelegate inherits from URLSessionDelegate. But URLSessionDelegate inherits from NSObjectProtocol. That means that apart from implementing all the methods from previous delegates, we also have to implement all the methods from NSObjectProtocol (e.g. equality, hash). If we inherit from NSObject, all that NSObjectProtocol methods are already implemented for us.

open class OperationManager: OperationManagerPr {
    
    weak var delegate: OperationManagerDelegate?
    
    public private(set) var queueController: QueueControllerPr
    public private(set) var operationController: OperationControllerPr
    
    // MARK: - Object Life Cycle Methods
    
    public init() {
        queueController = QueueController()
        operationController = OperationController()
    }
    
    /*!
     * @brief Initializes Manager with configuration for the default NSURLSession. This configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration.
     * @discussion Default sessions behave much like the shared session (unless you customize them further), but let you obtain data incrementally using a delegate. Ephemeral sessions are similar to default sessions, but they don’t write caches, cookies, or credentials to disk. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
     * @warning The configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration.
     */
    public class func initWith(defaultConfig: URLSessionConfiguration, delegate: OperationManagerDelegate?) -> OperationManager {
        return initWith(defaultConfig: defaultConfig, backgroundConfig: nil, delegate: delegate)
    }
    
    /*!
     * @brief Initializes Manager with configuration for the background NSURLSession. This configuration must be a 'background' NSURLSessionConfiguration.
     * @discussion Background sessions let you perform uploads and downloads of content in the background while your app isn’t running. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
     * @warning The configuration must be a 'background' NSURLSessionConfiguration.
     */
    public class func initWith(backgroundConfig: URLSessionConfiguration, delegate: OperationManagerDelegate?) -> OperationManager {
        return initWith(defaultConfig: nil, backgroundConfig: backgroundConfig, delegate: delegate)
    }
    /*!
     * @brief Initializes Manager with configurations for the default and background NSURLSessions. Default configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration. Background configuration must be a 'background' NSURLSessionConfiguration.
     * @discussion Default sessions behave much like the shared session (unless you customize them further), but let you obtain data incrementally using a delegate. Ephemeral sessions are similar to default sessions, but they don’t write caches, cookies, or credentials to disk. Background sessions let you perform uploads and downloads of content in the background while your app isn’t running. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
     * @warning Default configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration. Background configuration must be a 'background' NSURLSessionConfiguration.
     */
    public class func initWith(defaultConfig: URLSessionConfiguration?, backgroundConfig: URLSessionConfiguration?, delegate: OperationManagerDelegate?) -> OperationManager {
        let operationManager = OperationManager.init()
        operationManager.operationController.defaultSession = defaultConfig != nil ? NetworkOperationClient.clientWithConfiguration(config: defaultConfig!, delegate: operationManager.operationController) : nil
        operationManager.operationController.backgroundSession = backgroundConfig != nil ? NetworkOperationClient.clientWithConfiguration(config: backgroundConfig!, delegate: operationManager.operationController) : nil
        
        operationManager.delegate = delegate
        
        return operationManager
    }
    
}

// MARK: -

extension OperationManager: URLOperationManager {

    // MARK: - RESET
    
    /*!
    * @brief Empties all cookies, caches and credential stores, removes disk files, flushes in-progress downloads to disk, and ensures that future requests occur on a new socket.
    */
    open func reset(completion: @escaping () -> Void) {
        self.operationController.resetDefaultSession(completion: completion)
        self.operationController.resetBackgroundSession(completion: completion)
    }
    
    // MARK: - DATA
    
    /*!
     * @brief Default RAW Data Fetch. NSData output in "result.data"
     */
    @discardableResult
    open func data(_ URLString: String, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        return data(URLString, progress: nil, completion: completion)
    }
    
    /*!
     * @brief Default RAW Data Fetch. NSData output in "result.data"
     */
    @discardableResult
    open func data(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        return data(URLString, useCache: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default RAW Data Fetch. NSData output in "result.data"
     */
    @discardableResult
    open func data(_ URLString: String, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        return data(URLString, useCache: useCache, locked: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default RAW Data Fetch. NSData output in "result.data"
     */
    @discardableResult
    open func data(_ URLString: String, useCache: Bool = false, locked: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: locked, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: locked, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }
    
    // MARK: - UPLOAD

    /*!
     * @brief Default URLSession Upload
     * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. Upload File must exist in some device path. There is No caching involved in this upload.
     */
    @discardableResult
    open func upload(fileURL: URL, toURLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return upload(fileURL: fileURL, toURLString: toURLString, inBackground: false, progress: progress, completion: completion)
    }
    
    
    /*!
     * @brief Default/Background URLSession Upload
     * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. If 'inBackground' Upload will be executing even if the app is suspended. Upload File must exist in some device path. There is No caching involved in this upload.
     * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     */
    @discardableResult
    open func upload(fileURL: URL, toURLString: String, inBackground: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if inBackground {
            group.runInBackgroundWith(fileURL: fileURL, toURLString: toURLString, progress: progress, completion: completion)
        } else {
            group.runWith(fileURL: fileURL, toURLString: toURLString, progress: progress, completion: completion)
        }
        
        return group
    }
    
    /*!
     * @brief Default URLSession Upload
     * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. Upload Data must exist in some device path. There is No caching involved in this upload.
     * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
     */
    @discardableResult
    open func upload(fileData: Data, toURLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(fileData: fileData, toURLString: toURLString, progress: progress, completion: completion)
     
        return group
    }
    
    
    // MARK: - DOWNLOAD
    
    /*!
     * @brief Default JSON Download without Cache
     */
    @discardableResult
    open func downloadJSON(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr {
        return self.downloadJSON(URLString, useCache: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default JSON Download with/without Cache
     */
    @discardableResult
    open func downloadJSON(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr {
        return self.downloadJSON(URLString, useCache: useCache, locked: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default JSON Download with/without Cache and permanence
     */
    @discardableResult
    open func downloadJSON(_ URLString: String, useCache: Bool = false, locked: Bool = false, progress: NetworkOperationProgress?, completion: @escaping ParseJSONOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = JSONDataNetworkRequest.init(withURLString: URLString)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: nil)
        let parseOp = ParseJSONOperation.init(operationDelegate: self.operationController, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: locked, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: locked, operationDelegate: self.operationController)

            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp, parseOp: parseOp)
        } else {
            group.runWith(networkOp: networkOp, parseOp: parseOp)
        }
        
        return group
    }
    
    
    /*!
     * @brief Default HTTP Image Download with Cache
     */
    @discardableResult
    open func downloadDataImage(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        return downloadDataImage(URLString, useCache: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default HTTP Image Download with/without Cache and permanence
     */
    @discardableResult
    open func downloadDataImage(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        return downloadDataImage(URLString, useCache: useCache, locked: false, progress: progress, completion: completion)
    }
    
    /*!
    * @brief Default HTTP Image Download
    */
    @discardableResult
    open func downloadDataImage(_ URLString: String, useCache: Bool = false, locked: Bool = false, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(dataURLString: URLString, useCache: useCache, locked: locked, progress: progress, completion: completion)
        
        return group
    }
    
    /*!
     * @brief Default URL Image Download with Cache
     */
    @discardableResult
    open func downloadFileImage(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        return downloadFileImage(URLString, useCache: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default URL Image Download with/without Cache and permanence
     */
    @discardableResult
    open func downloadFileImage(_ URLString: String, useCache: Bool, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        return downloadFileImage(URLString, useCache: useCache, locked: false, progress: progress, completion: completion)
    }
    
    /*!
    * @brief Default URL Image Download
    */
    @discardableResult
    open func downloadFileImage(_ URLString: String, useCache: Bool = false, locked: Bool = false, progress: NetworkOperationProgress?, completion: @escaping ConversionImageOperationCompletion) -> GroupBaseOperationPr {
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(downloadURLString: URLString, useCache: useCache, locked: locked, progress: progress, completion: completion)
        
        return group
    }
    
    
    /*!
     * @brief Default URLSession Download and Stored in Temp Folder
     * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    @discardableResult
    open func downloadFile(_ URLString: String, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return downloadFile(URLString, inBackground: false, progress: progress, completion: completion)
    }
    
    /*!
     * @brief Default/Background URLSession Download and Stored in Temp Folder.
     * @discussion If not 'inBackground' Download only will execute while the app is running, if it goes to background, the download will be paused. If 'inBackground' Download will be executing even if the app is suspended. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    @discardableResult
    open func downloadFile(_ URLString: String, inBackground: Bool, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return downloadFile(URLString, inBackground: inBackground, toDocumentsFolder: false,  progress: progress, completion: completion)
    }
    
    /*!
     * @brief Downloaded and Stored in Temp or Documents Folder
     * @discussion If not 'inBackground' Download only will execute while the app is running, if it goes to background, the download will be paused. If 'inBackground' Download will be executing even if the app is suspended. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
     * @warning File location URL is returned as NSData in NetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
     */
    @discardableResult
    open func downloadFile(_ URLString: String, inBackground: Bool = false, toDocumentsFolder: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if inBackground {
            group.runInBackgroundWith(URLString: URLString, destinationFolder: toDocumentsFolder ? FileManager.SearchPathDirectory.documentDirectory : FileManager.SearchPathDirectory.cachesDirectory, progress: progress, completion: completion)
        } else {
            group.runWith(URLString: URLString, destinationFolder: toDocumentsFolder ? FileManager.SearchPathDirectory.documentDirectory : FileManager.SearchPathDirectory.cachesDirectory, progress: progress, completion: completion)
        }
        
        return group
    }
    
    // MARK: - BACKGROUND TASK Helper
    
    open func handleEventsForBackgroundURLSession(identifier: String, completion: @escaping () -> Void) {
        operationController.handleEventsForBackgroundURLSession(identifier: identifier, completion:completion)
    }
}

// MARK: -

extension OperationManager: ConcurrentOperationManager {
    
    // MARK: - CONCURRENT
    
    /*!
     * @brief Runs multiple defined operations concurrently
     */
    @discardableResult
    open func runConcurrentOperations(operations: [BaseOperation]) -> GroupBaseOperationPr {
        
        let group = GroupGeneralOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWithOperations(operations: operations)
        
        return group
    }
    
    public func cancelAllRequests() {
        queueController.cancelAllOperations(inQueue: .all)
    }
    
    public func currentConcurrentOpsInQueues() -> Int {
        let dataOps = queueController.currentConcurrentOpsInQueue(.data)
        let computeOps = queueController.currentConcurrentOpsInQueue(.compute)
        let bgOps = queueController.currentConcurrentOpsInQueue(.background)
        
        return dataOps + computeOps + bgOps
    }
}

// MARK: -

extension OperationManager: HTTPOperationManager {

    // MARK: - GET
    
    @discardableResult
    open func GET(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return GET(URLString, parameters: parameters, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func GET(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return GET(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func GET(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
    
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .get, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }
  
    
    // MARK: - HEAD
    
    @discardableResult
    open func HEAD(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return HEAD(URLString, parameters: parameters, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func HEAD(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return HEAD(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func HEAD(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .head, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }

    // MARK: - POST
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return POST(URLString, parameters: parameters, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return POST(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .post, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return POST(URLString, parameters: parameters, body: body, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return POST(URLString, parameters: parameters, body: body, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func POST(_ URLString: String, parameters: RequestParameters?, body: @escaping (RequestMultipartFormData) -> Void, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .post, parameters: parameters, body: body)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }

    // MARK: - PUT
    
    @discardableResult
    open func PUT(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return PUT(URLString, parameters: parameters, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func PUT(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return PUT(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func PUT(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .put, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }
    
    // MARK: - PATCH
    
    @discardableResult
    open func PATCH(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {

        return PATCH(URLString, parameters: parameters, progress: nil, completion: completion)
    }

    @discardableResult
    open func PATCH(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {

        return PATCH(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }

    @discardableResult
    open func PATCH(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {

        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .patch, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)

        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)

        if useCache {
        let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
        let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)

        group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
        group.runWith(networkOp: networkOp)
        }

        return group
    }
    
    // MARK: - DELETE
    
    @discardableResult
    open func DELETE(_ URLString: String, parameters: RequestParameters?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return DELETE(URLString, parameters: parameters, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func DELETE(_ URLString: String, parameters: RequestParameters?, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        return DELETE(URLString, parameters: parameters, useCache: false, progress: nil, completion: completion)
    }
    
    @discardableResult
    open func DELETE(_ URLString: String, parameters: RequestParameters?, useCache: Bool = false, progress: NetworkOperationProgress?, completion: @escaping NetworkOperationCompletion) -> GroupBaseOperationPr {
        
        let networkRequest = DataNetworkRequest.init(withURLString: URLString, method: .delete, parameters: parameters)
        let networkOp = NetworkDataOperation.init(request: networkRequest, operationDelegate: self.operationController, progress: progress, completion: completion)
        
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        
        if useCache {
            let readCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            let writeCacheOp = CacheJSONOperation.init(URLString: URLString, locked: false, operationDelegate: self.operationController)
            
            group.runWith(readCacheOp: readCacheOp, networkOp: networkOp, writeCacheOp: writeCacheOp)
        } else {
            group.runWith(networkOp: networkOp)
        }
        
        return group
    }
}
