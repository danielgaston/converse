//
//  NetworkOperationClient.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public protocol NetworkOperationClientPr: AnyObject {
    
    func runSessionDataTask(request: NetworkDataRequest) throws -> URLSessionDataTask
    func runSessionDownloadTask(request: NetworkDownloadRequest) throws -> URLSessionDownloadTask
    func runSessionUploadTask(request: NetworkUploadRequest, fileURL: URL) throws -> URLSessionUploadTask
    func runSessionUploadTask(request: NetworkUploadRequest, bodyData: Data) throws -> URLSessionUploadTask
}

// MARK: -

extension URLSession: NetworkOperationClientPr {

    // MARK: DefaultOperationClient Delegate Methods

    public func runSessionDataTask(request: NetworkDataRequest) throws -> URLSessionDataTask {
        let task = try operationDataTask(request: request)
        return task
    }
    
    public func runSessionDownloadTask(request: NetworkDownloadRequest) throws -> URLSessionDownloadTask {
        let task = try operationDownloadTask(request: request)
        return task
    }
    
    public func runSessionUploadTask(request: NetworkUploadRequest, fileURL: URL) throws -> URLSessionUploadTask {
        let task = try operationUploadTask(request: request, fileURL: fileURL)
        return task
    }
    
    public func runSessionUploadTask(request: NetworkUploadRequest, bodyData: Data) throws -> URLSessionUploadTask {
        let task = try operationUploadTask(request: request, bodyData: bodyData)
        return task
    }
}

// MARK:-

class NetworkOperationClient {

    // MARK: - Class Methods
    
    class func clientWithConfiguration(config: URLSessionConfiguration, delegate: URLSessionDelegate) -> URLSession & NetworkOperationClientPr {
        let session = URLSession.init(configuration: config, delegate: delegate, delegateQueue: nil)
        session.sessionDescription = "CONFIGURED URLSession"
        return session
    }
    
    class func defaultClientWithDelegate(_ delegate: URLSessionDelegate) -> URLSession & NetworkOperationClientPr {
        /*!
         * @brief Default Session:
         * @discussion:
         timeoutIntervalForRequest: 60s
         timeoutIntervalForResource: 7days
         networkServiceType: NSURLNetworkServiceTypeDefault
         allowsCellularAccess: YES
         HTTPCookieAcceptPolicy: NSHTTPCookieAcceptPolicyOnlyFromMainDocumentDomain
         HTTPShouldSetCookies: YES
         HTTPCookieStorage: [NSHTTPCookieStorage sharedHTTPCookieStorage]
         URLCredentialStorage: [URLCredentialStorage sharedCredentialStorage]
         URLCache: [NSURLCache sharedURLCache]
         requestCachePolicy: NSURLRequestUseProtocolCachePolicy
         configuration.HTTPMaximumConnectionsPerHost: 4
         HTTPShouldUsePipelining: NO
         connectionProxyDictionary: NULL
         */
    
        let configuration = URLSessionConfiguration.default
        let session = URLSession.init(configuration: configuration, delegate: delegate, delegateQueue: nil)
        session.sessionDescription = "DEFAULT URLSession"
        return session
    }
    
    class func ephemeralClientWithDelegate(_ delegate: URLSessionDelegate) -> URLSession & NetworkOperationClientPr {
       
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession.init(configuration: configuration, delegate: delegate, delegateQueue: nil)
        session.sessionDescription = "EPHEMERAL URLSession"
        return session
    }

    static let BACKGROUND_OPERATION_SESSION_ID = "BackgroundSession"

    class func backgroundClientWithDelegate(_ delegate: URLSessionDelegate) -> URLSession & NetworkOperationClientPr {
        
        let bundleId: String? = Bundle.main.bundleIdentifier

        let configuration = URLSessionConfiguration.background(withIdentifier: bundleId ?? NetworkOperationClient.BACKGROUND_OPERATION_SESSION_ID)
        configuration.sessionSendsLaunchEvents = true
        // Indicates whether the app should be resumed or launched in the background when transfers finish.
        // When finished the system calls the app delegate’s application:handleEventsForBackgroundURLSession:completionHandler:
        configuration.isDiscretionary = true
        // Determines whether background tasks can be scheduled at the discretion of the system for optimal performance.
        let session = URLSession.init(configuration: configuration, delegate: delegate, delegateQueue: nil)
        session.sessionDescription = "BACKGROUND URLSession"
        return session
    }
}
