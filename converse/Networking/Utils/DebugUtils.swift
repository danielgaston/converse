//
//  DebugUtils.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public enum LogMode {
    case disabled
    case enabled
}

// MARK: -

public struct LogType: OptionSet {
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let queue        = LogType(rawValue: 1 << 0)
    public static let cache        = LogType(rawValue: 1 << 1)
    public static let operation    = LogType(rawValue: 1 << 2)
    public static let parser       = LogType(rawValue: 1 << 3)
    public static let network      = LogType(rawValue: 1 << 4)
    
    public static let all: LogType = [.queue, .cache, .operation, .parser, .network]
}

// MARK: -

public struct LogFlag: OptionSet {
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let info     = LogFlag(rawValue: 1 << 0)
    public static let warn     = LogFlag(rawValue: 1 << 1)
    public static let error    = LogFlag(rawValue: 1 << 2)
    
    public static let all: LogFlag = [.info, .warn, .error]
}

// MARK: -

public enum LogLevel: Int {
    case level0 = 0
    case level1 = 1
    case level2 = 2
    case level3 = 3
}

// MARK: - Console Debug Flags

// global variables available to be modified by any app (default values defined)
public var logModeOption: LogMode = .disabled
public var logTypeOption: LogType = LogType.all
public var logFlagOption: LogFlag = LogFlag.all
public var logDeepLevel: LogLevel = .level3


// MARK: - Console Debug Methods

public func CLog(type: LogType, flag: LogFlag, level: LogLevel, message: String) {
    
    if logModeOption == .enabled {
        if logTypeOption.contains(type) && logFlagOption.contains(flag) && logDeepLevel.rawValue >= level.rawValue {
            let logString = LogHelper.logTypeString(type: type) + " -- " + LogHelper.logFlagString(flag: flag) + " ··· " + LogHelper.logLevelString(level: level) + message
            print(logString)
        }
    }
}

// MARK: - Console Debug Settings Methods

public func CLogStart() {
    logModeOption = .enabled
    logTypeOption = LogType.all
    logFlagOption = LogFlag.all
    logDeepLevel  = .level3
}

public func CLogStop() {
    logModeOption = .disabled
}

// MARK: - Console Debug String Helper

class LogHelper {
    
    private init() {}
    
    class func logTypeString(type: LogType) -> String {
        switch type {
            case .queue: return "Queue"
            case .cache: return "Cache"
            case .operation: return "Operations"
            case .parser: return "Parser"
            case .network: return "Network"
            default: return "Unknown"
        }
    }
    
    class func logFlagString(flag: LogFlag) -> String {
        switch flag {
            case .info: return "ℹ️ Info"
            case .warn: return "⚠️ Warn"
            case .error: return "❌ Error"
            default: return "❔Unknown"
        }
    }
    
    class func logLevelString(level: LogLevel) -> String {
        switch level {
            case .level0: return ""
            case .level1: return "----- "
            case .level2: return "----- ----- "
            case .level3: return "----- ----- ----- "
        }
    }
}

