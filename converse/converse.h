//
//  converse.h
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

#import <UIKit/UIKit.h>

// Swift: We can't use bridging headers in Swift Frameworks. (File's Target-Membership must be set to public)
#import "RequestSerialization.h"
#import "ResponseSerialization.h"

//! Project version number for converse.
FOUNDATION_EXPORT double converseVersionNumber;

//! Project version string for converse.
FOUNDATION_EXPORT const unsigned char converseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <converse/PublicHeader.h>


