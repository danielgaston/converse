//
//  DoubleExtensions.swift
//  converse
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

public extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
