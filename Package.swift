// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "converse",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "converse",
            targets: ["converse"]),
    ],
    dependencies: [
        // not valid since the library does not have any Packake.swift, so that the library is not prepared for SwiftPackageManager
//        .package(url: "https://github.com/spotify/SPTPersistentCache", from: "1.1.1")
        
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "converse",
            /*dependencies: [SPTPersistentCache],*/ // not valid since the library does not have any Packake.swift, so that the library is not prepared for SwiftPackageManager
            path: "./converse",
            exclude: []),
        .testTarget(
            name: "converseTests",
            dependencies: ["converse"]),
    ]
)
