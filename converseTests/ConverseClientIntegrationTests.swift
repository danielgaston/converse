//
//  ConverseClientIntegrationTests.swift
//  converseTests
//
//  Created by Daniel Iglesias on 09/12/2019.
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

/*!
 * @warning Make sure in simulator 'Hardware->Keyboard->Connect Hardware keyboard' is disabled
 */
import XCTest
@testable import converse

class ConverseClientIntegrationTests: XCTestCase {
        
    override class func setUp() {
        super.setUp()
        
        CLogStart()
    }
    
    override func setUp() {
        super.setUp()
        
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("************************** START ******************************")
        print("---------------------------------------------------------------")
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("---------------------------------------------------------------")
        print("*************************** END *******************************")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
    }
    
    //MARK:- NETWORK CLIENT
    
    func test_client_dataTask() {

        let expectation = self.expectation(description: #function)
        
        let networkRequest = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        
        // <1MB
        let dataTask = try? URLSession.shared.operationDataTask(request: networkRequest) { (data, response, error) in
            
            XCTAssertNil(error, "error should be nil");
            XCTAssertNotNil(data, "data should not be nil");
                        
            expectation.fulfill()
        }
        
        dataTask?.resume()
        
        self.waitForExpectations(timeout: 30) {(error) in
            if error != nil {
                print(error!.localizedDescription)
            }
        }
    }
    
    func test_client_downloadTask() {

        let expectation = self.expectation(description: #function)
        
        let networkRequest = DownloadNetworkRequest.init(withURLString: "http://www.pdf995.com/samples/pdf.pdf")
        
        // <1MB
        let dataTask = try? URLSession.shared.operationDownloadTask(request: networkRequest) { (data, response, error) in
            
            XCTAssertNil(error, "error should be nil");
            XCTAssertNotNil(data, "data should not be nil");
                        
            expectation.fulfill()
        }
        
        dataTask?.resume()
        
        self.waitForExpectations(timeout: 30) {(error) in
            if error != nil {
                print(error!.localizedDescription)
            }
        }
    }
    
    func test_client_uploadTask_fileURL() {

        let expectation = self.expectation(description: #function)
        
        let toURLString = "http://ptsv2.com/t/converse/post"
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        let networkRequest = UploadNetworkRequest.init(toURLString: toURLString, fromFileURL: fileURL, parameters: nil, body: nil)
        
        // <1MB
        let dataTask = try? URLSession.shared.operationUploadTaskFromFileURL(request: networkRequest) { (data, response, error) in
            
            XCTAssertNil(error, "error should be nil");
            XCTAssertNotNil(data, "data should not be nil");
                        
            expectation.fulfill()
        }
        
        dataTask?.resume()
        
        self.waitForExpectations(timeout: 30) {(error) in
            if error != nil {
                print(error!.localizedDescription)
            }
        }
    }
    
    func test_client_uploadTask_fileData() {

        let expectation = self.expectation(description: #function)
        
        let toURLString = "http://ptsv2.com/t/converse/post"
        
        guard let url = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        guard let data = try? Data.init(contentsOf: url) else {
            XCTFail("There is no Upload Data")
            expectation.fulfill()
            return
        }
        
        let networkRequest = UploadNetworkRequest.init(toURLString: toURLString, fromData: data, parameters: nil, body: nil)
        
        // <1MB
        let dataTask = try? URLSession.shared.operationUploadTaskFromData(request: networkRequest) { (data, response, error) in
            
            XCTAssertNil(error, "error should be nil");
            XCTAssertNotNil(data, "data should not be nil");
                        
            expectation.fulfill()
        }
        
        dataTask?.resume()
        
        self.waitForExpectations(timeout: 30) {(error) in
            if error != nil {
                print(error!.localizedDescription)
            }
        }
    }
    
    
    
    
    
    
    
}
