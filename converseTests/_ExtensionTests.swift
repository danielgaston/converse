//
//  _ExtensionTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _ExtensionTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Date Formatter
    
    func test_dateFormatter_iso8601Full() {
        
        let dateFormatter = DateFormatter.iso8601Full
        XCTAssertNotNil(dateFormatter, "DateFormatter should exist")
        
        let dateString = "1997-07-16T19:20:30.222+01:00"
        let date = dateFormatter.date(from: dateString)
        XCTAssertNotNil(date, "date should exist")
        
        let formattedDateString = dateFormatter.string(from: date!)
        XCTAssertNotNil(formattedDateString, "String date should exist")
    }

    func test_dateFormatter_yyyyMMdd() {
        
        let dateFormatter = DateFormatter.yyyyMMdd
        XCTAssertNotNil(dateFormatter, "DateFormatter should exist")
                
        let dateString = "1997-07-16"
        let date = dateFormatter.date(from: dateString)
        XCTAssertNotNil(date, "date should exist")
        
        let formattedDateString = dateFormatter.string(from: date!)
        XCTAssertNotNil(formattedDateString, "String date should exist")
    }
    
    // MARK: String
    
    func test_string_MD5() {
        
        let string = "danielgaston"
        let stringMD5 = string.MD5()
        XCTAssertNotNil(stringMD5, "stringMD5 should exist")
        
        XCTAssertTrue(stringMD5 == "c150a10759c2b473d7b72eeb35b4c7a5f50b8a0a26a0c5d6e3a5523599d8ebec", "MD5 does not have the expected value")
    }
}
