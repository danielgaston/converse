//
//  SKOperationManager.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

extension OperationManagerPr {

    // MARK: CUSTOM REQUESTS
  
    @discardableResult
    func getLocales(completion: @escaping ((SKLocalesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initLocales()
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKLocalesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func getCurrencies(completion: @escaping ((SKCurrenciesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initCurrencies()
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKCurrenciesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func getMarkets(locale: String, completion: @escaping ((SKMarketsParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initMarkets(locale: locale)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKMarketsParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func getPlaces(country: String, currency: String, locale: String, query: String, completion: @escaping ((SKPlacesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initPlaces(country: country, currency: currency, locale: locale, query: query)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKPlacesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func getPlace(country: String, currency: String, locale: String, placeId: String, completion: @escaping ((SKPlacesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initPlace(country: country, currency: currency, locale: locale, placeId: placeId)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKPlacesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func getPlace(placeId: String, completion: @escaping ((SKPlacesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initPlace(id: placeId)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKPlacesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func browseQuotes(originId: String, destinationId: String, departDate: String, returnDate: String, completion: @escaping ((SKBrowseQuotesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initBrowseQuotes(originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKBrowseQuotesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func browseRoutes(originId: String, destinationId: String, departDate: String, returnDate: String, completion: @escaping ((SKBrowseRoutesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initBrowseRoutes(originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKBrowseRoutesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func browseDates(originId: String, destinationId: String, departDate: String, returnDate: String, completion: @escaping ((SKBrowseDatesParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initBrowseDates(originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKBrowseDatesParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func browseGrid(originId: String, destinationId: String, departDate: String, returnDate: String, completion: @escaping ((SKBrowseGridParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initBrowseGrid(originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKBrowseGridParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func createLiveSession(params: RequestParameters, progress: NetworkOperationProgress?, completion: @escaping ((SKLiveCreateSessionParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKRequest.initCreateLiveSession(params: params)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKLiveCreateSessionParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func pollLiveSession(location: String, params: RequestParameters, progress: NetworkOperationProgress?, completion: @escaping ((SKLivePollSessionParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initPollLiveSession(location: location, params: params)
        
        let group = SKGroupPollLiveSessionOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(request: networkRequest, progress: progress, pollingInterval: 2, parseCompletion: completion)
        
        return group
    }
    
    @discardableResult
    func createLiveBooking(uri: String, body: String, progress: NetworkOperationProgress?, completion: @escaping ((SKLiveCreateBookingParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKRequest.initCreateLiveBooking(uri: uri, body: body)
        let networkOp = SKNetworkDataOperation.init(request: networkRequest, operationDelegate: operationController, progress: nil, completion: nil)
        let parseOp = SKLiveCreateBookingParseDataOperation.init(operationDelegate: operationController, completion: completion)
        let group = GroupBaseOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(networkOp: networkOp, parseOp: parseOp)
        
        return group
    }
    
    @discardableResult
    func pollLiveBooking(sessionKey: String, outboundLegId: String, inboundLegId: String?, params: RequestParameters, progress: NetworkOperationProgress?, completion: @escaping ((SKLivePollBookingParseDataOperation.DefaultResultType) -> Void)) -> GroupBaseOperationPr {
        
        let networkRequest = SKJSONRequest.initPollLiveBooking(sessionKey: sessionKey, outboundLegId: outboundLegId, inboundLegId: inboundLegId, params: params)
        
        let group = SKGroupPollLiveBookingOperation.init(queueController: queueController, customOperationController: operationController)
        group.runWith(request: networkRequest, progress: progress, pollingInterval: 5, parseCompletion: completion)
        
        return group
    }
}
