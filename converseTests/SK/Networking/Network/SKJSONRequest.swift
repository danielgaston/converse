//
//  SKJSONRequest.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

class SKJSONRequest: JSONDataNetworkRequest {

    private static var baseURLString: String { return SKConstants.baseURLString }
    private static var version: String { return SKConstants.version }
    private static var apiKey: String { return SKConstants.apiKey }
    private static var country: String { return SKConstants.country }
    private static var locale: String { return SKConstants.locale }
    private static var currency: String { return SKConstants.currency }
    private static var cabinClass: String { return SKConstants.cabinClass }
    private static var locationSchema: String { return SKConstants.locationSchema }
    
    static func initLocales() -> SKJSONRequest {
        // reference/v1.0/locales
        let relativeURLString = "apiservices/reference/\(version)/locales"
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        let params = SKApiKeyRequestParameters.init()
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initCurrencies() -> SKJSONRequest {
        // /reference/v1.0/currencies
        let relativeURLString = "apiservices/reference/\(version)/currencies"
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        let params = SKApiKeyRequestParameters.init()
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initMarkets(locale: String) -> SKJSONRequest {
        // /reference/v1.0/countries/{locale}
        let apiPath = "apiservices/reference/\(version)/countries"
        let relativeURLString = apiPath + "/" + locale
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        let params = SKMarketsRequestParameters.init(locale: locale)
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initPlaces(country: String, currency: String, locale: String, query: String) -> SKJSONRequest {
        // /autosuggest/v1.0/{market}/{currency}/{locale}
        let apiPath = "apiservices/autosuggest/\(version)"
        let relativeURLString = apiPath + "/" + country + "/" + currency + "/" + locale
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        let params = SKPlacesRequestParameters.init(query: query)
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initPlaces(query: String) -> SKJSONRequest {
        return SKJSONRequest.initPlaces(country: country, currency: currency, locale: locale, query: query)
    }
    
    static func initPlace(country: String, currency: String, locale: String, placeId: String) -> SKJSONRequest {
        // /autosuggest/v1.0/{market}/{currency}/{locale}
        let apiPath = "apiservices/autosuggest/\(version)"
        let relativeURLString = apiPath + "/" + country + "/" + currency + "/" + locale
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        let params = SKPlaceRequestParameters.init(id: placeId)
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initPlace(id: String) -> SKJSONRequest {
        return SKJSONRequest.initPlace(country: country, currency: currency, locale: locale, placeId: id)
    }
    
    
    private static func initBrowse(apiPath: String, originId: String, destinationId: String, departDate: String, returnDate: String) -> SKJSONRequest {
        let relativeURLString = apiPath + "/" + originId + "/" + destinationId + "/" + departDate + "/" + returnDate
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        
        let params = SKApiKeyRequestParameters.init()
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initBrowseQuotes(originId: String, destinationId: String, departDate: String, returnDate: String) -> SKJSONRequest {
        // browsequotes/v1.0/{country}/{currency}/{locale}/
        let apiPath = "apiservices/browsequotes/\(version)" + "/" + country + "/" + currency + "/" + locale
        return SKJSONRequest.initBrowse(apiPath: apiPath, originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
    }
    
    static func initBrowseRoutes(originId: String, destinationId: String, departDate: String, returnDate: String) -> SKJSONRequest {
        // browseroutes/v1.0/{country}/{currency}/{locale}/
        let apiPath = "apiservices/browseroutes/\(version)" + "/" + country + "/" + currency + "/" + locale
        return SKJSONRequest.initBrowse(apiPath: apiPath, originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
    }
    
    static func initBrowseDates(originId: String, destinationId: String, departDate: String, returnDate: String) -> SKJSONRequest {
        // browsedates/v1.0/{country}/{currency}/{locale}/
        let apiPath = "apiservices/browsedates/\(version)" + "/" + country + "/" + currency + "/" + locale
        return SKJSONRequest.initBrowse(apiPath: apiPath, originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
    }
    
    static func initBrowseGrid(originId: String, destinationId: String, departDate: String, returnDate: String) -> SKJSONRequest {
        // browsegrid/v1.0/{country}/{currency}/{locale}/
        let apiPath = "apiservices/browsegrid/\(version)" + "/" + country + "/" + currency + "/" + locale
        return SKJSONRequest.initBrowse(apiPath: apiPath, originId: originId, destinationId: destinationId, departDate: departDate, returnDate: returnDate)
    }
    
    static func initPollLiveSession(location: String, params: RequestParameters) -> SKJSONRequest{
        
        // pricing/v1.0/{SessionKey}
        
        let request = SKJSONRequest.init(withURLString: location, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request
    }
    
    static func initPollLiveBooking(sessionKey: String, outboundLegId: String, inboundLegId: String?, params: RequestParameters) -> SKJSONRequest{
        
        // apiservices/pricing/v1.0/{SessionKey}/booking/{OutboundLegId};{InboundLegId}?apikey=prtl6749387986743898559646983194
        
        let apiPath = "apiservices/pricing/\(version)"
        let relativeURLString = apiPath + "/" + sessionKey + "/booking/" + outboundLegId + ";" + (inboundLegId ?? "")
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        
        let request = SKJSONRequest.init(withURLString: URLString!, method: .get, parameters: params, body: nil)
        request.responseType = SKJSONResponse.self
        
        return request        
    }
}
