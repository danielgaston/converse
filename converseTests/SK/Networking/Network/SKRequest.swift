//
//  SKRequest.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

class SKRequest: DataNetworkRequest {
    
    private static var baseURLString: String { return SKConstants.baseURLString }
    private static var version: String { return SKConstants.version }
    private static var apiKey: String { return SKConstants.apiKey }
    private static var country: String { return SKConstants.country }
    private static var locale: String { return SKConstants.locale }
    private static var currency: String { return SKConstants.currency }
    private static var cabinClass: String { return SKConstants.cabinClass }
    private static var locationSchema: String { return SKConstants.locationSchema }
    
    static func initCreateLiveSession(params: RequestParameters) -> SKRequest{
        
        // http://partners.api.skyscanner.net/apiservices/pricing/v1.0
        let apiPath = "apiservices/pricing/\(version)"
        let relativeURLString = apiPath
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        
        let request = SKRequest.init(withURLString: URLString!, method: .post, parameters: params, body: nil)
        request.responseType = SKResponse.self
        
        return request
    }
    
    /** The url is provided in the response of the live prices:
     "BookingDetailsLink": {
     "Uri": "/apiservices/pricing/v1.0/abb2a69708624a7ca82762ed73493598_ecilpojl_DCE634A426CBDA30CE7EA3E9068CD053/booking",
     "Body": "OutboundLegId=11235-1705301925--32480-0-13554-1705302055&InboundLegId=13554-1706020700--32480-0-11235-1706020820",
     "Method": "PUT" } }
     */
    static func initCreateLiveBooking(uri: String, body: String) -> SKRequest{
        
        let relativeURLString = uri + "?apikey=" + apiKey
        let URLString = URL.init(string: relativeURLString, relativeTo: URL.init(string: baseURLString))?.absoluteString
        
        let request = SKRequest.init(withURLString: URLString!, method: .put, parameters: nil, body: nil)
        request.responseType = SKResponse.self
        
        let putData = body.data(using: .utf8, allowLossyConversion: true)
        request.headers["Content-Type"] = "application/x-www-form-urlencoded"
        request.httpBody = putData

        do {
            _ = try request.urlRequest()
        } catch {
            fatalError(error.localizedDescription)
        }
        
        return request
    }
}
