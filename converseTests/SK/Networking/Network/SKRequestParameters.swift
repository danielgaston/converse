//
//  SKRequestParameters.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

struct SKApiKeyRequestParameters: CodableParameter, RequestParameters {
    
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    
    init() {
        params = NetworkRequestParameters.init()
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        _ = encoder.container(keyedBy: CodingKeys.self)
    }
    
    public init (from decoder: Decoder) throws {
        _ = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        
        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
    }
    
    public mutating func addKeys() {
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)
    }
}

// MARK: -

struct SKMarketsRequestParameters: CodableParameter, RequestParameters {
    
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    var locale: String
    
    init(locale loc: String) {
        params = NetworkRequestParameters.init()
        locale = loc
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(locale, forKey: .locale)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        locale = try container.decode(String.self, forKey: .locale)
        
        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
        case locale = "locale"
    }
    
    public mutating func addKeys() {
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)
        add(key: CodingKeys.locale.rawValue, value: locale)
    }
}

// MARK: -

struct SKPlacesRequestParameters: CodableParameter, RequestParameters {
    
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    var query: String
    
    init(query qry: String) {
        params = NetworkRequestParameters.init()
        query = qry
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(query, forKey: .query)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        query = try container.decode(String.self, forKey: .query)
        
        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
        case query = "query"
    }
    
    public mutating func addKeys() {
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)
        add(key: CodingKeys.query.rawValue, value: query)
    }
}

// MARK: -

struct SKPlaceRequestParameters: CodableParameter, RequestParameters {
    
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    var id: String
    
    init(id identifier: String) {
        params = NetworkRequestParameters.init()
        id = identifier
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
    }
    
    public init (from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        id = try container.decode(String.self, forKey: .id)
        
        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
        case id = "id"
    }
    
    public mutating func addKeys() {
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)
        add(key: CodingKeys.id.rawValue, value: id)
    }
}

// MARK: -

struct SKLiveCreateSessionRequestParameters: /*CodableParameter,*/ RequestParameters {
    
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    var country: String?
    var currency: String?
    var locale: String?
    var locationSchema: String?
    var originPlace: String?
    var destinationPlace: String?
    var outboundDate: Date?
    var inboundDate: Date?
    var cabinClass: String?
    var adults: Int?
    var children: Int?
    var infants: Int?
    var includeCarriers: String?
    var excludeCarriers: String?
    
    init() {
        params = NetworkRequestParameters.init()
        
        country = SKConstants.country
        currency = SKConstants.currency
        locale = SKConstants.locale
        cabinClass = SKConstants.cabinClass
        locationSchema = SKConstants.locationSchema
        adults = SKConstants.adults
        children = SKConstants.children
        infants = SKConstants.infants
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(country, forKey: .country)
        try container.encodeIfPresent(currency, forKey: .currency)
        try container.encodeIfPresent(locale, forKey: .locale)
        try container.encodeIfPresent(locationSchema, forKey: .locationSchema)
        try container.encodeIfPresent(originPlace, forKey: .originPlace)
        try container.encodeIfPresent(destinationPlace, forKey: .destinationPlace)
        try container.encodeIfPresent(outboundDate, forKey: .outboundDate)
        try container.encodeIfPresent(inboundDate, forKey: .inboundDate)
        try container.encodeIfPresent(cabinClass, forKey: .cabinClass)
        try container.encodeIfPresent(adults, forKey: .adults)
        try container.encodeIfPresent(children, forKey: .children)
        try container.encodeIfPresent(infants, forKey: .infants)
        try container.encodeIfPresent(includeCarriers, forKey: .includeCarriers)
        try container.encodeIfPresent(excludeCarriers, forKey: .excludeCarriers)
    }
    
    public init (from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        country = try container.decodeIfPresent(String.self, forKey: .country)
        currency = try container.decodeIfPresent(String.self, forKey: .currency)
        locale = try container.decodeIfPresent(String.self, forKey: .locale)
        locationSchema = try container.decodeIfPresent(String.self, forKey: .locationSchema)
        originPlace = try container.decodeIfPresent(String.self, forKey: .originPlace)
        destinationPlace = try container.decodeIfPresent(String.self, forKey: .destinationPlace)
        cabinClass = try container.decodeIfPresent(String.self, forKey: .cabinClass)
        adults = try container.decodeIfPresent(Int.self, forKey: .adults)
        children = try container.decodeIfPresent(Int.self, forKey: .children)
        infants = try container.decodeIfPresent(Int.self, forKey: .infants)
        includeCarriers = try container.decodeIfPresent(String.self, forKey: .includeCarriers)
        excludeCarriers = try container.decodeIfPresent(String.self, forKey: .excludeCarriers)
        
        let dateFormatter = DateFormatter.sk_iso8601_yyyy_MM_dd
        let outboundDateString = try container.decode(String.self, forKey: .outboundDate)
        let inboundDateString = try container.decodeIfPresent(String.self, forKey: .inboundDate)
        let outDate = dateFormatter.date(from: outboundDateString)
        outboundDate = outDate!
        
        if inboundDateString != nil, let inDate = dateFormatter.date(from: inboundDateString!) {
            inboundDate = inDate
        }

        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
        case country = "country"
        case currency = "currency"
        case locale = "locale"
        case locationSchema = "locationSchema"
        case originPlace = "originPlace"
        case destinationPlace = "destinationPlace"
        case outboundDate = "outboundDate"
        case inboundDate = "inboundDate"
        case cabinClass = "cabinClass"
        case adults = "adults"
        case children = "children"
        case infants = "infants"
        case includeCarriers = "includeCarriers"
        case excludeCarriers = "excludeCarriers"
    }
    
    public mutating func addKeys() {
        
        let dateFormatter = DateFormatter.sk_iso8601_yyyy_MM_dd
        
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)

        if country != nil {
            add(key: CodingKeys.country.rawValue, value: country!)
        }
        if currency != nil {
            add(key: CodingKeys.currency.rawValue, value: currency!)
        }
        if locale != nil {
            add(key: CodingKeys.locale.rawValue, value: locale!)
        }
        if locationSchema != nil {
            add(key: CodingKeys.locationSchema.rawValue, value: locationSchema!)
        }
        if originPlace != nil {
            add(key: CodingKeys.originPlace.rawValue, value: originPlace!)
        }
        if destinationPlace != nil {
            add(key: CodingKeys.destinationPlace.rawValue, value: destinationPlace!)
        }
        if outboundDate != nil {
            add(key: CodingKeys.outboundDate.rawValue, value: dateFormatter.string(from: outboundDate!))
        }
        if inboundDate != nil {
            add(key: CodingKeys.inboundDate.rawValue, value: dateFormatter.string(from: inboundDate!))
        }
        if cabinClass != nil {
            add(key: CodingKeys.cabinClass.rawValue, value: cabinClass!)
        }
        if adults != nil {
            add(key: CodingKeys.adults.rawValue, value: String(adults!))
        }
        if children != nil {
            add(key: CodingKeys.children.rawValue, value: String(children!))
        }
        if infants != nil {
            add(key: CodingKeys.infants.rawValue, value: String(infants!))
        }
        if includeCarriers != nil {
            add(key: CodingKeys.includeCarriers.rawValue, value: includeCarriers!)
        }
        if excludeCarriers != nil {
            add(key: CodingKeys.excludeCarriers.rawValue, value: excludeCarriers!)
        }
    }
}

// MARK: -

struct SKLivePollSessionRequestParameters: CodableParameter, RequestParameters {
    var params: NetworkRequestParameters
    let apiKey: String = SKConstants.apiKey
    var pageIndex: String?
    var pageSize: String?
    var sortType: String?
    var sortOrder: String?
    var duration: String?
    var includeCarriers: String?
    var excludeCarriers: String?
    var originAirports: String?
    var destinationAirports: String?
    var stops: String?
    var outboundDepartTime: String?
    var outboundDepartStartTime: String?
    var outboundDepartEndTime: String?
    var outboundArriveStartTime: String?
    var outboundArriveEndTime: String?
    var inboundDepartTime: String?
    var inboundDepartStartTime: String?
    var inboundDepartEndTime: String?
    var inboundArriveStartTime: String?
    var inboundArriveEndTime: String?
    
    init() {
        params = NetworkRequestParameters.init()
        
        addKeys()
    }
    
    // MARK: RequestParameters Protocol Methods
    
    func toDict() -> [String : String] {
        return params.toDict()
    }
    
    mutating func add(key: String, value: String) {
        params.add(key: key, value: value)
    }
    
    mutating func remove(key: String) {
        params.remove(key: key)
    }
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    public func encode (to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(pageIndex, forKey: .pageIndex)
        try container.encodeIfPresent(pageSize, forKey: .pageSize)
        try container.encodeIfPresent(sortType, forKey: .sortType)
        try container.encodeIfPresent(sortOrder, forKey: .sortOrder)
        try container.encodeIfPresent(duration, forKey: .duration)
        try container.encodeIfPresent(includeCarriers, forKey: .includeCarriers)
        try container.encodeIfPresent(excludeCarriers, forKey: .excludeCarriers)
        try container.encodeIfPresent(originAirports, forKey: .originAirports)
        try container.encodeIfPresent(destinationAirports, forKey: .destinationAirports)
        try container.encodeIfPresent(stops, forKey: .stops)
        try container.encodeIfPresent(outboundDepartTime, forKey: .outboundDepartTime)
        try container.encodeIfPresent(outboundDepartStartTime, forKey: .outboundDepartStartTime)
        try container.encodeIfPresent(outboundDepartEndTime, forKey: .outboundDepartEndTime)
        try container.encodeIfPresent(outboundArriveStartTime, forKey: .outboundArriveStartTime)
        try container.encodeIfPresent(outboundArriveEndTime, forKey: .outboundArriveEndTime)
        try container.encodeIfPresent(inboundDepartTime, forKey: .inboundDepartTime)
        try container.encodeIfPresent(inboundDepartStartTime, forKey: .inboundDepartStartTime)
        try container.encodeIfPresent(inboundDepartEndTime, forKey: .inboundDepartEndTime)
        try container.encodeIfPresent(inboundArriveStartTime, forKey: .inboundArriveStartTime)
        try container.encodeIfPresent(inboundArriveEndTime, forKey: .inboundArriveEndTime)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = NetworkRequestParameters.init()
        pageIndex = try container.decodeIfPresent(String.self, forKey: .pageIndex)
        pageSize = try container.decodeIfPresent(String.self, forKey: .pageSize)
        sortType = try container.decodeIfPresent(String.self, forKey: .sortType)
        sortOrder = try container.decodeIfPresent(String.self, forKey: .sortOrder)
        duration = try container.decodeIfPresent(String.self, forKey: .duration)
        includeCarriers = try container.decodeIfPresent(String.self, forKey: .includeCarriers)
        excludeCarriers = try container.decodeIfPresent(String.self, forKey: .excludeCarriers)
        originAirports = try container.decodeIfPresent(String.self, forKey: .originAirports)
        destinationAirports = try container.decodeIfPresent(String.self, forKey: .destinationAirports)
        stops = try container.decodeIfPresent(String.self, forKey: .stops)
        outboundDepartTime = try container.decodeIfPresent(String.self, forKey: .outboundDepartTime)
        outboundDepartStartTime = try container.decodeIfPresent(String.self, forKey: .outboundDepartStartTime)
        outboundDepartEndTime = try container.decodeIfPresent(String.self, forKey: .outboundDepartEndTime)
        outboundArriveStartTime = try container.decodeIfPresent(String.self, forKey: .outboundArriveStartTime)
        outboundArriveEndTime = try container.decodeIfPresent(String.self, forKey: .outboundArriveEndTime)
        inboundDepartTime = try container.decodeIfPresent(String.self, forKey: .inboundDepartTime)
        inboundDepartStartTime = try container.decodeIfPresent(String.self, forKey: .inboundDepartStartTime)
        inboundDepartEndTime = try container.decodeIfPresent(String.self, forKey: .inboundDepartEndTime)
        inboundArriveStartTime = try container.decodeIfPresent(String.self, forKey: .inboundArriveStartTime)
        inboundArriveEndTime = try container.decodeIfPresent(String.self, forKey: .inboundArriveEndTime)
        
        addKeys()
    }
    
    enum CodingKeys: String, CodingKey {
        case params = "params"
        case apiKey = "apiKey"
        case pageIndex = "PageIndex"
        case pageSize = "PageSize"
        case sortType = "SortType"
        case sortOrder = "SortOrder"
        case duration = "Duration"
        case includeCarriers = "IncludeCarriers"
        case excludeCarriers = "ExcludeCarriers"
        case originAirports = "OriginAirports"
        case destinationAirports = "DestinationAirports"
        case stops = "Stops"
        case outboundDepartTime = "OutboundDepartTime"
        case outboundDepartStartTime = "OutboundDepartStartTime"
        case outboundDepartEndTime = "OutboundDepartEndTime"
        case outboundArriveStartTime = "OutboundArriveStartTime"
        case outboundArriveEndTime = "OutboundArriveEndTime"
        case inboundDepartTime = "InboundDepartTime"
        case inboundDepartStartTime = "InboundDepartStartTime"
        case inboundDepartEndTime = "InboundDepartEndTime"
        case inboundArriveStartTime = "InboundArriveStartTime"
        case inboundArriveEndTime = "InboundArriveEndTime"
    }
    
    public mutating func addKeys() {
                
        add(key: CodingKeys.apiKey.rawValue, value: apiKey)
        
        if pageIndex != nil {
            add(key: CodingKeys.pageIndex.rawValue, value: pageIndex!)
        }
        if pageSize != nil {
            add(key: CodingKeys.pageSize.rawValue, value: pageSize!)
        }
        if sortType != nil {
            add(key: CodingKeys.sortType.rawValue, value: sortType!)
        }
        if sortOrder != nil {
            add(key: CodingKeys.sortOrder.rawValue, value: sortOrder!)
        }
        if duration != nil {
            add(key: CodingKeys.duration.rawValue, value: duration!)
        }
        if includeCarriers != nil {
            add(key: CodingKeys.includeCarriers.rawValue, value: includeCarriers!)
        }
        if excludeCarriers != nil {
            add(key: CodingKeys.excludeCarriers.rawValue, value: excludeCarriers!)
        }
        if originAirports != nil {
            add(key: CodingKeys.originAirports.rawValue, value: originAirports!)
        }
        if destinationAirports != nil {
            add(key: CodingKeys.destinationAirports.rawValue, value: destinationAirports!)
        }
        if stops != nil {
            add(key: CodingKeys.stops.rawValue, value: stops!)
        }
        if outboundDepartTime != nil {
            add(key: CodingKeys.outboundDepartTime.rawValue, value: outboundDepartTime!)
        }
        if outboundDepartStartTime != nil {
            add(key: CodingKeys.outboundDepartStartTime.rawValue, value: outboundDepartStartTime!)
        }
        if outboundDepartEndTime != nil {
            add(key: CodingKeys.outboundDepartEndTime.rawValue, value: outboundDepartEndTime!)
        }
        if outboundArriveStartTime != nil {
            add(key: CodingKeys.outboundArriveStartTime.rawValue, value: outboundArriveStartTime!)
        }
        if outboundArriveEndTime != nil {
            add(key: CodingKeys.outboundArriveEndTime.rawValue, value: outboundArriveEndTime!)
        }
        if inboundDepartTime != nil {
            add(key: CodingKeys.inboundDepartTime.rawValue, value: inboundDepartTime!)
        }
        if inboundDepartStartTime != nil {
            add(key: CodingKeys.inboundDepartStartTime.rawValue, value: inboundDepartStartTime!)
        }
        if inboundDepartEndTime != nil {
            add(key: CodingKeys.inboundDepartEndTime.rawValue, value: inboundDepartEndTime!)
        }
        if inboundArriveStartTime != nil {
            add(key: CodingKeys.inboundArriveStartTime.rawValue, value: inboundArriveStartTime!)
        }
        if inboundArriveEndTime != nil {
            add(key: CodingKeys.inboundArriveEndTime.rawValue, value: inboundArriveEndTime!)
        }
    }
}


