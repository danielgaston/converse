//
//  SKGroupPollLiveSessionOperation.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

class SKGroupPollLiveSessionOperation: GroupBaseOperation {

    // MARK: Variables & Constants
    
    var timer: Timer?
    var request: NetworkDataRequest!
    var progress: NetworkOperationProgress?
    var pollingInterval: TimeInterval!
    var result: SKLivePollSessionParseDataOperation.DefaultResultType!
    weak var parseOp: SKLivePollSessionParseDataOperation?  // weak in order not to create a memory leak
    var parseCompletion: ((SKLivePollSessionParseDataOperation.DefaultResultType) -> Void)!
    
    var observers = [NSKeyValueObservation?]()
    
    // MARK: Object LifeCycle Methods
    
    deinit {
        deinitObservers()
    }
    
    
    // MARK: Public Methods
    
    func runWith(request: NetworkDataRequest, progress: NetworkOperationProgress?, pollingInterval: TimeInterval, parseCompletion: @escaping ((SKLivePollSessionParseDataOperation.DefaultResultType) -> Void)) {
        
        self.request = request
        self.progress = progress
        self.parseCompletion = parseCompletion
        self.pollingInterval = pollingInterval
        
        fireTimerWithDelay(0)
    }
    
    // MARK: Overriden Methods
    
    override func cancel() {
        deinitObservers()
        invalidateTimer()
        
        super.cancel()
    }
    
    // MARK: Observation Methods

    func initObservers(_ parseOperation: Operation) {
        observers = [
            parseOperation.observe(\.isFinished, options:[.new, .old], changeHandler: { [weak self] (model, change) in
                if change.newValue != nil {

                    self?.deinitObservers();
                    
                    var completePoll = true
                    var abortPoll = false
                    
                    if (self?.result.hasError() ?? true) || !(self?.result.hasData() ?? true) {
                        abortPoll = true
                    } else {
                        completePoll = self?.result.responseObj?.isCompleted() ?? true
                    }
                    
                    if abortPoll {
                        self?.invalidateTimer()
                    } else if completePoll {
                        self?.invalidateTimer()
//                        processCaching()
                    } else {
                        self?.fireTimerWithDelay((self?.pollingInterval ?? 2))
                    }
                    
                    if let completion = self?.parseCompletion, let pollResult = self?.result {
                        DispatchQueue.main.async {
                            completion(pollResult);
                        }
                    }
                }
            })]
    }
    
    func deinitObservers() {
        observers.forEach { (observer) in
            observer?.invalidate()
        }
    }
    
    
    // MARK: Helper Methods
    
    func fireTimerWithDelay(_ delay: TimeInterval) {
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(self.processPolling), userInfo: nil, repeats: false)
        }
    }
    
    func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func processPolling() {
        let networkOp = SKNetworkDataOperation.init(request: request, operationDelegate: operationController, progress: progress, completion: nil)

        // holds strong reference
        let parseOp = SKLivePollSessionParseDataOperation.init(operationDelegate: operationController, completion: { (result) in
            self.result = result
        })
        
        // assigns strong reference to weak reference
        self.parseOp = parseOp
        
        // In `processPolling()`, strong `parseOp` reference will exist and hold strong retainment. When `processPolling()` method finishes strong `parseOp` is released, weak `parseOp` should be nil then, but it is not since the reference it holds, still exists as it has been `storeSubOperation` and also added to the corresponding queue to be executed. Additionally, since we weakily hold the operation, we do NOT create any memory leak, since as soon it finishes in the queue, it will be released.
        initObservers(parseOp)
        
        // Network <- Parse
        let wrappedParseOp = WrappedBaseOperation.init(parseOp)
        let wrappedNetworkOp = WrappedBaseOperation.init(networkOp)
        let NPAdapterOp = AdapterBaseOperation.init(fromOperation: wrappedNetworkOp, toOperation: wrappedParseOp)
        
        NPAdapterOp.addDependency(networkOp)
        parseOp.addDependency(NPAdapterOp)
        
        storeSubOperation(networkOp)
        storeSubOperation(NPAdapterOp)
        storeSubOperation(parseOp)
        
        queueController.add(operation: networkOp, inQueue: .data)
        queueController.add(operation: NPAdapterOp, inQueue: .compute)
        queueController.add(operation: parseOp, inQueue: .compute)
    }
    
    func processCaching() {
        if request.method == .get {
            let cacheWriteOp = CacheJSONOperation.init(URLString: request.absoluteURL?.absoluteString, operationDelegate: operationController)
            cacheWriteOp.set(result: result.adaptedResponseObj())
            queueController.add(operation: cacheWriteOp, inQueue: .compute)
        }
    }
}
