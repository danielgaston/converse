//
//  SKParseDataOperation.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

// MARK:-

class SKLocalesOperationResult : GenericOperationResult<SKLocalesDataModel> {}

// MARK:-

class SKLocalesParseDataOperation: ParseDataOperation<SKLocalesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKLocalesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK Locales Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKLocalesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKCurrenciesOperationResult : GenericOperationResult<SKCurrenciesDataModel> {}

// MARK:-

class SKCurrenciesParseDataOperation: ParseDataOperation<SKCurrenciesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKCurrenciesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK Currencies Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKCurrenciesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKMarketsOperationResult : GenericOperationResult<SKMarketsDataModel> {}

// MARK:-

class SKMarketsParseDataOperation: ParseDataOperation<SKMarketsOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKMarketsOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK Markets Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKMarketsDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKPlacesOperationResult : GenericOperationResult<SKPlacesDataModel> {}

// MARK:-

class SKPlacesParseDataOperation: ParseDataOperation<SKPlacesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKPlacesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK Places Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKPlacesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKBrowseQuotesOperationResult : GenericOperationResult<SKBrowseQuotesDataModel> {}

// MARK:-

class SKBrowseQuotesParseDataOperation: ParseDataOperation<SKBrowseQuotesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKBrowseQuotesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK BrowseQuotes Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKBrowseQuotesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKBrowseRoutesOperationResult : GenericOperationResult<SKBrowseRoutesDataModel> {}

// MARK:-

class SKBrowseRoutesParseDataOperation: ParseDataOperation<SKBrowseRoutesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKBrowseRoutesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK BrowseRoutes Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKBrowseRoutesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKBrowseDatesOperationResult : GenericOperationResult<SKBrowseDatesDataModel> {}

// MARK:-

class SKBrowseDatesParseDataOperation: ParseDataOperation<SKBrowseDatesOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKBrowseDatesOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK BrowseDates Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKBrowseDatesDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKBrowseGridOperationResult : GenericOperationResult<SKBrowseGridDataModel> {}

// MARK:-

class SKBrowseGridParseDataOperation: ParseDataOperation<SKBrowseGridOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKBrowseGridOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK BrowseGrid Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKBrowseGridDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKLiveCreateSessionOperationResult : GenericOperationResult<SKLiveCreateSessionDataModel> {}

// MARK:-

class SKLiveCreateSessionParseDataOperation: ParseDataOperation<SKLiveCreateSessionOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKLiveCreateSessionOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK LiveCreateSession Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard let extraInfoNetworkResult: NetworkOperationResultExtraInfo = result().extraInfo?[NetworkOperationResultExtraInfo.key()] as? NetworkOperationResultExtraInfo else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Response does not contain any NetworkOperationResult ExtraInfo")
            reportResult()
            finish()
            return
        }
        
        guard let responseURL: HTTPURLResponse = extraInfoNetworkResult.responseURL as? HTTPURLResponse else {
            CLog(type: .operation, flag: .info, level: .level1, message: "URL response does not contain the URL for polling the results in the newly created session")
            reportResult()
            finish()
            return
        }
        
        guard let location: String = responseURL.allHeaderFields[SKLiveCreateSessionDataModel.locationKey] as? String else {
            CLog(type: .operation, flag: .info, level: .level1, message: "'Location' header is not present in server response")
            reportResult()
            finish()
            return
        }
        
        let responseObj = SKLiveCreateSessionDataModel.init(location: location)
        result().responseObj = responseObj
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKLivePollSessionOperationResult : GenericOperationResult<SKLivePollSessionDataModel> {}

// MARK:-

class SKLivePollSessionParseDataOperation: ParseDataOperation<SKLivePollSessionOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKLivePollSessionOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK LivePollSession Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKLivePollSessionDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKLiveCreateBookingOperationResult : GenericOperationResult<SKLiveCreateBookingDataModel> {}

// MARK:-

class SKLiveCreateBookingParseDataOperation: ParseDataOperation<SKLiveCreateBookingOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKLiveCreateBookingOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK LiveBookingSession Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard let extraInfoNetworkResult: NetworkOperationResultExtraInfo = result().extraInfo?[NetworkOperationResultExtraInfo.key()] as? NetworkOperationResultExtraInfo else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Response does not contain any NetworkOperationResult ExtraInfo")
            reportResult()
            finish()
            return
        }
        
        guard let responseURL: HTTPURLResponse = extraInfoNetworkResult.responseURL as? HTTPURLResponse else {
            CLog(type: .operation, flag: .info, level: .level1, message: "URL response does not contain the URL for polling the results in the newly created session")
            reportResult()
            finish()
            return
        }
        
        guard let location: String = responseURL.allHeaderFields[SKLiveCreateSessionDataModel.locationKey] as? String else {
            CLog(type: .operation, flag: .info, level: .level1, message: "'Location' header is not present in server response")
            reportResult()
            finish()
            return
        }
        
        let responseObj = SKLiveCreateBookingDataModel.init(location: location)
        result().responseObj = responseObj
        
        reportResult()
        finish()
    }
}

// MARK:-

class SKLivePollBookingOperationResult : GenericOperationResult<SKLivePollBookingDataModel> {}

// MARK:-

class SKLivePollBookingParseDataOperation: ParseDataOperation<SKLivePollBookingOperationResult> {
    // MARK: TypeAlias
    typealias DefaultResultType = SKLivePollBookingOperationResult
    
    //MARK: Operation LifeCycle Methods
    
    private override init() {
        super.init()
    }
    
    init(operationDelegate del: ParseBaseOperationDelegate?, completion comp: ((DefaultResultType) -> Void)?) {
        super.init()
        
        name = "SK LivePollBooking Parse JSON Operation"
        operationDelegate = del
        set(completion: comp)
    }
    
    //MARK: Overriden Methods
    
    override func start() {
        super.start()
        
        guard isValid() else {
            cancel()
            return
        }
        
        guard !result().hasError() else {
            CLog(type: .operation, flag: .error, level: .level1, message: "Dependent operation failed, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        guard result().hasData() else {
            CLog(type: .operation, flag: .info, level: .level1, message: "Dependent operation did not injected data, no more processing needed.")
            reportResult()
            finish()
            return
        }
        
        do {
            let decoder = JSONDecoder.init()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.sk_iso8601)
            result().responseObj = try decoder.decode(SKLivePollBookingDataModel.self, from: result().data!)
        } catch {
            result().error = error
        }
        
        reportResult()
        finish()
    }
}
