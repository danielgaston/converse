//
//  SKNetworkDataOperation.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

@testable import converse

class SKNetworkDataOperation: NetworkDataOperation {
    
    override init(request: NetworkDataRequest, operationDelegate: NetworkBaseOperationDelegate?, progress: NetworkOperationProgress?, completion: NetworkOperationCompletion?) {
        super.init(request: request, operationDelegate: operationDelegate, progress: progress, completion: completion)
        name = "Network Data Operation"
    }
}
