//
//  SKProperties.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

enum SKConstants {
    static let apiKey = "ss630745725358065467897349852985"
    static let baseURLString = "http://business.skyscanner.net/" /* @"http://partners.api.skyscanner.net/"*/
    static let version = "v1.0"
    static let country = "UK"
    static let locale = "en-GB"
    static let currency = "EUR"
    static let cabinClass = "economy"
    static let locationSchema = "sky"
    static let adults = 1
    static let children = 0
    static let infants = 0
}
