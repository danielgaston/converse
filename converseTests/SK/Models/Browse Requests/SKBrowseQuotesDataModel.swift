//
//  SKBrowseQuotesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKBrowseQuoteDataModel: Codable {
    var quoteId: Int
    var minPrice: Double
    var direct: Bool
    var outboundLeg: SKBrowseLegDataModel?
    var inboundLeg: SKBrowseLegDataModel?
    var quoteDateTime: Date
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case quoteId = "QuoteId"
        case minPrice = "MinPrice"
        case direct = "Direct"
        case outboundLeg = "OutboundLeg"
        case inboundLeg = "InboundLeg"
        case quoteDateTime = "QuoteDateTime"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(quoteId, forKey: .quoteId)
        try container.encode(minPrice, forKey: .minPrice)
        try container.encode(direct, forKey: .direct)
        try container.encodeIfPresent(outboundLeg, forKey: .outboundLeg)
        try container.encodeIfPresent(inboundLeg, forKey: .inboundLeg)
        try container.encode(quoteDateTime, forKey: .quoteDateTime)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        quoteId = try container.decode(Int.self, forKey: .quoteId)
        minPrice = try container.decode(Double.self, forKey: .minPrice)
        direct = try container.decode(Bool.self, forKey: .direct)
        outboundLeg = try container.decode(SKBrowseLegDataModel.self, forKey: .outboundLeg)
        inboundLeg = try container.decode(SKBrowseLegDataModel.self, forKey: .inboundLeg)
        quoteDateTime = try container.decode(Date.self, forKey: .quoteDateTime)
    }
}

/*
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 }
 */

// MARK: -

struct SKBrowseQuotesDataModel: Codable {
    var quotes: [SKBrowseQuoteDataModel]?
    var places: [SKBrowsePlaceDataModel]?
    var carriers: [SKBrowseCarrierDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case quotes = "Quotes"
        case places = "Places"
        case carriers = "Carriers"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(quotes, forKey: .quotes)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        quotes = try container.decodeIfPresent([SKBrowseQuoteDataModel].self, forKey: .quotes)
        places = try container.decodeIfPresent([SKBrowsePlaceDataModel].self, forKey: .places)
        carriers = try container.decodeIfPresent([SKBrowseCarrierDataModel].self, forKey: .carriers)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
}

/*
 {
 "Quotes": [
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 },
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }*/
