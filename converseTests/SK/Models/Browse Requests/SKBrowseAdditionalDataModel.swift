//
//  SKBrowseAdditionalDataModel.swift
//  converseTests
//
//

import UIKit

struct SKBrowseLegDataModel: Codable {
    var carrierIds: [Int]
    var originId: Int
    var destinationId: Int
    var departureDate: Date
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case carrierIds = "CarrierIds"
        case originId = "OriginId"
        case destinationId = "DestinationId"
        case departureDate = "DepartureDate"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(carrierIds, forKey: .carrierIds)
        try container.encode(originId, forKey: .originId)
        try container.encode(destinationId, forKey: .destinationId)
        try container.encode(departureDate, forKey: .departureDate)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        carrierIds = try container.decode([Int].self, forKey: .carrierIds)
        originId = try container.decode(Int.self, forKey: .originId)
        destinationId = try container.decode(Int.self, forKey: .destinationId)
        departureDate = try container.decode(Date.self, forKey: .departureDate)
    }
}

/*
 {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 }
 */

// MARK: -

struct SKBrowsePlaceDataModel: Codable {
    var placeId: Int
    var name: String
    var type: String
    var skyscannerCode: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case placeId = "PlaceId"
        case name = "Name"
        case type = "Type"
        case skyscannerCode = "SkyscannerCode"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(placeId, forKey: .placeId)
        try container.encode(name, forKey: .name)
        try container.encode(type, forKey: .type)
        try container.encode(skyscannerCode, forKey: .skyscannerCode)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        placeId = try container.decode(Int.self, forKey: .placeId)
        name = try container.decode(String.self, forKey: .name)
        type = try container.decode(String.self, forKey: .type)
        skyscannerCode = try container.decode(String.self, forKey: .skyscannerCode)
    }
}

/*
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 }
 */

// MARK: -

struct SKBrowseCarrierDataModel: Codable {
    var carrierId: Int
    var name: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case carrierId = "CarrierId"
        case name = "Name"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(carrierId, forKey: .carrierId)
        try container.encode(name, forKey: .name)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        carrierId = try container.decode(Int.self, forKey: .carrierId)
        name = try container.decode(String.self, forKey: .name)
    }
}

/*
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 }
 */
