//
//  SKBrowseRoutesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKBrowseRouteDataModel: Codable {
    var originId: Int
    var destinationId: Int
    var quoteIds: [Int]
    var price: Int
    var quoteDateTime: Date
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case originId = "OriginId"
        case destinationId = "DestinationId"
        case quoteIds = "QuoteIds"
        case price = "Price"
        case quoteDateTime = "QuoteDateTime"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(originId, forKey: .originId)
        try container.encode(destinationId, forKey: .destinationId)
        try container.encode(quoteIds, forKey: .quoteIds)
        try container.encode(price, forKey: .price)
        try container.encode(quoteDateTime, forKey: .quoteDateTime)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        originId = try container.decode(Int.self, forKey: .originId)
        destinationId = try container.decode(Int.self, forKey: .destinationId)
        quoteIds = try container.decode([Int].self, forKey: .quoteIds)
        price = try container.decode(Int.self, forKey: .price)
        quoteDateTime = try container.decode(Date.self, forKey: .quoteDateTime)
    }
}

/*
 {
 "OriginId": 1811,
 "DestinationId": 1845,
 "QuoteIds": [
 1,
 2
 ],
 "Price": 326,
 "QuoteDateTime": "2016-11-13T01:30:00"
 }
 */

// MARK: -

struct SKBrowseRoutesDataModel: Codable {
    var routes: [SKBrowseRouteDataModel]?
    var quotes: [SKBrowseQuoteDataModel]?
    var places: [SKBrowsePlaceDataModel]?
    var carriers: [SKBrowseCarrierDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case routes = "Routes"
        case quotes = "Quotes"
        case places = "Places"
        case carriers = "Carriers"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(routes, forKey: .routes)
        try container.encodeIfPresent(quotes, forKey: .quotes)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        routes = try container.decodeIfPresent([SKBrowseRouteDataModel].self, forKey: .routes)
        quotes = try container.decodeIfPresent([SKBrowseQuoteDataModel].self, forKey: .quotes)
        places = try container.decodeIfPresent([SKBrowsePlaceDataModel].self, forKey: .places)
        carriers = try container.decodeIfPresent([SKBrowseCarrierDataModel].self, forKey: .carriers)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
}

/*
 {
 "Routes": [
 {
 "OriginId": 1811,
 "DestinationId": 1845,
 "QuoteIds": [
 1,
 2
 ],
 "Price": 326,
 "QuoteDateTime": "2016-11-13T01:30:00"
 },
 {
 "OriginId": 1811,
 "DestinationId": 929,
 "QuoteIds": [
 3
 ],
 "Price": 150,
 "QuoteDateTime": "2016-11-09T17:44:00"
 },
 ...
 ],
 "Quotes": [
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 },
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }
 */
