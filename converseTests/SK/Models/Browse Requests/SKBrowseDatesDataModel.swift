//
//  SKBrowseDatesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKBrowseInnerDateDataModel: Codable {
    var partialDate: String?
    var quoteIds: [Int]?
    var price: Int
    var quoteDateTime: Date?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case partialDate = "PartialDate"
        case quoteIds = "QuoteIds"
        case price = "Price"
        case quoteDateTime = "QuoteDateTime"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(partialDate, forKey: .partialDate)
        try container.encodeIfPresent(quoteIds, forKey: .quoteIds)
        try container.encode(price, forKey: .price)
        try container.encodeIfPresent(quoteDateTime, forKey: .quoteDateTime)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        partialDate = try container.decodeIfPresent(String.self, forKey: .partialDate)
        quoteIds = try container.decodeIfPresent([Int].self, forKey: .quoteIds)
        price = try container.decode(Int.self, forKey: .price)
        quoteDateTime = try container.decodeIfPresent(Date.self, forKey: .quoteDateTime)
    }
}

// MARK: -

struct SKBrowseDateDataModel: Codable {
    var outboundDates: [SKBrowseInnerDateDataModel]
    var inboundDates: [SKBrowseInnerDateDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case outboundDates = "OutboundDates"
        case inboundDates = "InboundDates"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(outboundDates, forKey: .outboundDates)
        try container.encode(inboundDates, forKey: .inboundDates)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        outboundDates = try container.decode([SKBrowseInnerDateDataModel].self, forKey: .outboundDates)
        inboundDates = try container.decode([SKBrowseInnerDateDataModel].self, forKey: .inboundDates)
    }
}

/*
 {
 "OutboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1,
 2,
 3,
 4,
 5
 ],
 "Price": 66,
 "QuoteDateTime": "2016-11-08T17:28:00"
 },
 ...
 ],
 "InboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1
 ],
 "Price": 93,
 "QuoteDateTime": "2016-11-21T17:19:00"
 },
 ...
 ]
 }
 */

// MARK: -

struct SKBrowseDatesDataModel: Codable {
    var dates: SKBrowseDateDataModel?
    var quotes: [SKBrowseQuoteDataModel]?
    var places: [SKBrowsePlaceDataModel]?
    var carriers: [SKBrowseCarrierDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case dates = "Dates"
        case quotes = "Quotes"
        case places = "Places"
        case carriers = "Carriers"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(dates, forKey: .dates)
        try container.encodeIfPresent(quotes, forKey: .quotes)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dates = try container.decodeIfPresent(SKBrowseDateDataModel.self, forKey: .dates)
        quotes = try container.decodeIfPresent([SKBrowseQuoteDataModel].self, forKey: .quotes)
        places = try container.decodeIfPresent([SKBrowsePlaceDataModel].self, forKey: .places)
        carriers = try container.decodeIfPresent([SKBrowseCarrierDataModel].self, forKey: .carriers)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
}

/*
 {
 "Dates": {
 "OutboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1,
 2,
 3,
 4,
 5
 ],
 "Price": 66,
 "QuoteDateTime": "2016-11-08T17:28:00"
 },
 ...
 ],
 "InboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1
 ],
 "Price": 93,
 "QuoteDateTime": "2016-11-21T17:19:00"
 },
 ...
 ]
 },
 "Quotes": [
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 },
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }*/
