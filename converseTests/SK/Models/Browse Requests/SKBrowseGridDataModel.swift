//
//  SKBrowseGridDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKBrowseGridDateDataModel: Codable {
    var date: String?

    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case date = "DateString"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(date, forKey: .date)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try container.decodeIfPresent(String.self, forKey: .date)
    }
}

// MARK: -

struct SKBrowseGridDataModel: Codable {
    var dates: [[SKBrowseGridDateDataModel?]?]?
    var places: [SKBrowsePlaceDataModel]?
    var carriers: [SKBrowseCarrierDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case dates = "Dates"
        case places = "Places"
        case carriers = "Carriers"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(dates, forKey: .dates)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dates = try container.decodeIfPresent([[SKBrowseGridDateDataModel?]?].self, forKey: .dates)
        places = try container.decodeIfPresent([SKBrowsePlaceDataModel].self, forKey: .places)
        carriers = try container.decodeIfPresent([SKBrowseCarrierDataModel].self, forKey: .carriers)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
}

/*
 
 {
 "Dates": [
 [
 null,
 {
 "DateString": "2017-01"
 },
 {
 "DateString": "2017-02"
 },
 {
 "DateString": "2017-03"
 },
 ...
 ],
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }
 
 */

