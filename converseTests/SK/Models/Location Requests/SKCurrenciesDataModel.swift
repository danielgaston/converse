//
//  SKCurrenciesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKCurrencyDataModel: Codable {
    var code: String
    var symbol: String
    var thousandsSeparator: String
    var decimalSeparator: String
    var symbolOnLeft: Bool
    var spaceBetweenAmountAndSymbol: Bool
    var roundingCoefficient: Int
    var decimalDigits: Int
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case code = "Code"
        case symbol = "Symbol"
        case thousandsSeparator = "ThousandsSeparator"
        case decimalSeparator = "DecimalSeparator"
        case symbolOnLeft = "SymbolOnLeft"
        case spaceBetweenAmountAndSymbol = "SpaceBetweenAmountAndSymbol"
        case roundingCoefficient = "RoundingCoefficient"
        case decimalDigits = "DecimalDigits"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(symbol, forKey: .symbol)
        try container.encode(thousandsSeparator, forKey: .thousandsSeparator)
        try container.encode(decimalSeparator, forKey: .decimalSeparator)
        try container.encode(symbolOnLeft, forKey: .symbolOnLeft)
        try container.encode(spaceBetweenAmountAndSymbol, forKey: .spaceBetweenAmountAndSymbol)
        try container.encode(roundingCoefficient, forKey: .roundingCoefficient)
        try container.encode(decimalDigits, forKey: .decimalDigits)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(String.self, forKey: .code)
        symbol = try container.decode(String.self, forKey: .symbol)
        thousandsSeparator = try container.decode(String.self, forKey: .thousandsSeparator)
        decimalSeparator = try container.decode(String.self, forKey: .decimalSeparator)
        symbolOnLeft = try container.decode(Bool.self, forKey: .symbolOnLeft)
        spaceBetweenAmountAndSymbol = try container.decode(Bool.self, forKey: .spaceBetweenAmountAndSymbol)
        roundingCoefficient = try container.decode(Int.self, forKey: .roundingCoefficient)
        decimalDigits = try container.decode(Int.self, forKey: .decimalDigits)
    }
}

// MARK:-

struct SKCurrenciesDataModel: Codable {
    
    var currencies: [SKCurrencyDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        currencies = try container.decode([SKCurrencyDataModel].self, forKey: .currencies)
    }
}

/*
 
 {
 "Currencies": [
 {
 "Code": "USD",
 "Symbol": "$",
 "ThousandsSeparator": ",",
 "DecimalSeparator": ".",
 "SymbolOnLeft": true,
 "SpaceBetweenAmountAndSymbol": false,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 },
 ...
 ]
 }
 
 */
