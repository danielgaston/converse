//
//  SKMarketsDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKCountryDataModel: Codable {
    var code: String
    var name: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case code = "Code"
        case name = "Name"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(name, forKey: .name)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(String.self, forKey: .code)
        name = try container.decode(String.self, forKey: .name)
    }
}

// MARK:- 

struct SKMarketsDataModel: Codable {
    var countries: [SKCountryDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case countries = "Countries"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(countries, forKey: .countries)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        countries = try container.decode([SKCountryDataModel].self, forKey: .countries)
    }
}

/*
 
 {
 "Countries": [
 {
 "Code": "AD",
 "Name": "Andorra"
 },
 {
 "Code": "AE",
 "Name": "United Arab Emirates"
 },
 {
 "Code": "AF",
 "Name": "Afghanistan"
 },
 ...
 ]
 }
 */
