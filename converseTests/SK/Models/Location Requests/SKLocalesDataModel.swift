//
//  SKLocalesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKLocaleDataModel: Codable {
    var code: String
    var name: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case code = "Code"
        case name = "Name"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(name, forKey: .name)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(String.self, forKey: .code)
        name = try container.decode(String.self, forKey: .name)
    }
}

// MARK: -

struct SKLocalesDataModel: Codable {
    var locales: [SKLocaleDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case locales = "Locales"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(locales, forKey: .locales)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        locales = try container.decode([SKLocaleDataModel].self, forKey: .locales)
    }
}

/*
 {
 "Locales": [
 {
 "Code": "ar-AE",
 "Name": "العربية (الإمارات العربية المتحدة)"
 },
 {
 "Code": "az-AZ",
 "Name": "Azərbaycan­ılı (Azərbaycan)"
 },
 {
 "Code": "bg-BG",
 "Name": "български (България)"
 },
 {
 "Code": "ca-ES",
 "Name": "català (català)"
 },
 */
