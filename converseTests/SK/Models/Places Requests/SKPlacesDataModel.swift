//
//  SKPlacesDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKPlaceDataModel: Codable {
    var placeId: String?
    var placeName: String?
    var regionId: String?
    var cityId: String?
    var countryId: String?
    var countryName: String?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case placeId = "PlaceId"
        case placeName = "PlaceName"
        case regionId = "RegionId"
        case cityId = "CityId"
        case countryId = "CountryId"
        case countryName = "CountryName"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        // encodeIfPresent for Optional values
        try container.encodeIfPresent(placeId, forKey: .placeId)
        try container.encodeIfPresent(placeName, forKey: .placeName)
        try container.encodeIfPresent(regionId, forKey: .regionId)
        try container.encodeIfPresent(cityId, forKey: .cityId)
        try container.encodeIfPresent(countryId, forKey: .countryId)
        try container.encodeIfPresent(countryName, forKey: .countryName)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        // decodeIfPresent for Optional values
        placeId = try container.decodeIfPresent(String.self, forKey: .placeId)
        placeName = try container.decodeIfPresent(String.self, forKey: .placeName)
        regionId = try container.decodeIfPresent(String.self, forKey: .regionId)
        cityId = try container.decodeIfPresent(String.self, forKey: .cityId)
        countryId = try container.decodeIfPresent(String.self, forKey: .countryId)
        countryName = try container.decodeIfPresent(String.self, forKey: .countryName)
    }
}

// MARK:-

struct SKPlacesDataModel: Codable {
    var places: [SKPlaceDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    
    private enum CodingKeys: String, CodingKey {
        case places = "Places"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(places, forKey: .places)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        places = try container.decode([SKPlaceDataModel].self, forKey: .places)
    }
}

/*
 
 {
 "Places": [
 {
 "PlaceId": "PARI-sky",
 "PlaceName": "Paris",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 {
 "PlaceId": "CDG-sky",
 "PlaceName": "Paris Charles de Gaulle",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 {
 "PlaceId": "ORY-sky",
 "PlaceName": "Paris Orly",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 ...
 ]
 }
 */
