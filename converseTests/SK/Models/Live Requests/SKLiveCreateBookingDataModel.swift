//
//  SKLiveCreateBookingDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

// No need for Codable protocol methods. Location and session key will be set later or via init

struct SKLiveCreateBookingDataModel {
    
    static let locationKey: String = "Location"
    public var location: String?
    
    init(location locationURLString: String) {
        location = locationURLString
    }
}
