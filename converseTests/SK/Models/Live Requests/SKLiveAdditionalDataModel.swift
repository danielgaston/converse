//
//  SKLiveAdditionalDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKLiveQueryDataModel: Codable {
    
    var country: String
    var currency: String
    var locale: String
    var locationSchema: String
    var adults: Int
    var children: Int
    var infants: Int
    var originPlace: String
    var destinationPlace: String
    var outboundDate: Date
    var inboundDate: Date?
    var cabinClass: String
    var groupPricing: Bool
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case country = "Country"
        case currency = "Currency"
        case locale = "Locale"
        case locationSchema = "LocationSchema"
        case adults = "Adults"
        case children = "Children"
        case infants = "Infants"
        case originPlace = "OriginPlace"
        case destinationPlace = "DestinationPlace"
        case outboundDate = "OutboundDate"
        case inboundDate = "InboundDate"
        case cabinClass = "CabinClass"
        case groupPricing = "GroupPricing"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(country, forKey: .country)
        try container.encode(currency, forKey: .currency)
        try container.encode(locale, forKey: .locale)
        try container.encode(locationSchema, forKey: .locationSchema)
        try container.encode(adults, forKey: .adults)
        try container.encode(children, forKey: .children)
        try container.encode(infants, forKey: .infants)
        try container.encode(originPlace, forKey: .originPlace)
        try container.encode(destinationPlace, forKey: .destinationPlace)
        try container.encode(outboundDate, forKey: .outboundDate)
        try container.encodeIfPresent(inboundDate, forKey: .inboundDate)
        try container.encode(cabinClass, forKey: .cabinClass)
        try container.encode(groupPricing, forKey: .groupPricing)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        country = try container.decode(String.self, forKey: .country)
        currency = try container.decode(String.self, forKey: .currency)
        locale = try container.decode(String.self, forKey: .locale)
        locationSchema = try container.decode(String.self, forKey: .locationSchema)
        adults = try container.decode(Int.self, forKey: .adults)
        children = try container.decode(Int.self, forKey: .children)
        infants = try container.decode(Int.self, forKey: .infants)
        originPlace = try container.decode(String.self, forKey: .originPlace)
        destinationPlace = try container.decode(String.self, forKey: .destinationPlace)
        cabinClass = try container.decode(String.self, forKey: .cabinClass)
        groupPricing = try container.decode(Bool.self, forKey: .groupPricing)
        
        let dateFormatter = DateFormatter.sk_iso8601_yyyy_MM_dd
        let outboundDateString = try container.decode(String.self, forKey: .outboundDate)
        let inboundDateString = try container.decodeIfPresent(String.self, forKey: .inboundDate)
        let outDate = dateFormatter.date(from: outboundDateString)
        outboundDate = outDate!
        
        if inboundDateString != nil, let inDate = dateFormatter.date(from: inboundDateString!) {
            inboundDate = inDate
        }
    }
}

// MARK:-

struct SKLiveItineraryDataModel: Codable {
    var outboundLegId: String
    var inboundLegId: String?
    var pricingOptions: [SKLivePricingOptionsDataModel]
    var bookingDetailsLink: SKLiveBookingDetailsLinkDataModel
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case outboundLegId = "OutboundLegId"
        case inboundLegId = "InboundLegId"
        case pricingOptions = "PricingOptions"
        case bookingDetailsLink = "BookingDetailsLink"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(outboundLegId, forKey: .outboundLegId)
        try container.encodeIfPresent(inboundLegId, forKey: .inboundLegId)
        try container.encode(pricingOptions, forKey: .pricingOptions)
        try container.encode(bookingDetailsLink, forKey: .bookingDetailsLink)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        outboundLegId = try container.decode(String.self, forKey: .outboundLegId)
        inboundLegId = try container.decodeIfPresent(String.self, forKey: .inboundLegId)
        pricingOptions = try container.decode([SKLivePricingOptionsDataModel].self, forKey: .pricingOptions)
        bookingDetailsLink = try container.decode(SKLiveBookingDetailsLinkDataModel.self, forKey: .bookingDetailsLink)
    }
}

// MARK:-

struct SKLivePricingOptionsDataModel: Codable {
    var agents: [Int]
    var quoteAgeInMinutes: Int
    var price: Double
    var deeplinkUrl: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case agents = "Agents"
        case quoteAgeInMinutes = "QuoteAgeInMinutes"
        case price = "Price"
        case deeplinkUrl = "DeeplinkUrl"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(agents, forKey: .agents)
        try container.encode(quoteAgeInMinutes, forKey: .quoteAgeInMinutes)
        try container.encode(price, forKey: .price)
        try container.encode(deeplinkUrl, forKey: .deeplinkUrl)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        agents = try container.decode([Int].self, forKey: .agents)
        quoteAgeInMinutes = try container.decode(Int.self, forKey: .quoteAgeInMinutes)
        price = try container.decode(Double.self, forKey: .price)
        deeplinkUrl = try container.decode(String.self, forKey: .deeplinkUrl)
    }
}

// MARK:-

struct SKLiveBookingDetailsLinkDataModel: Codable {

    var uri: String
    var body: String
    var method: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case uri = "Uri"
        case body = "Body"
        case method = "Method"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(uri, forKey: .uri)
        try container.encode(body, forKey: .body)
        try container.encode(method, forKey: .method)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        uri = try container.decode(String.self, forKey: .uri)
        body = try container.decode(String.self, forKey: .body)
        method = try container.decode(String.self, forKey: .method)
    }
}

// MARK:-

struct SKLiveLegDataModel: Codable {
    
    var id: String
    var segmentIds: [Int]
    var originStation: Int
    var destinationStation: Int
    var departure: Date
    var arrival: Date
    var duration: Int
    var journeyMode: String
    var stops: [Int]
    var carriers: [Int]
    var operatingCarriers: [Int]
    var directionality: String
    var flightNumbers: [SKLiveFlightNumberDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case segmentIds = "SegmentIds"
        case originStation = "OriginStation"
        case destinationStation = "DestinationStation"
        case departure = "Departure"
        case arrival = "Arrival"
        case duration = "Duration"
        case journeyMode = "JourneyMode"
        case stops = "Stops"
        case carriers = "Carriers"
        case operatingCarriers = "OperatingCarriers"
        case directionality = "Directionality"
        case flightNumbers = "FlightNumbers"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(segmentIds, forKey: .segmentIds)
        try container.encode(originStation, forKey: .originStation)
        try container.encode(destinationStation, forKey: .destinationStation)
        try container.encode(departure, forKey: .departure)
        try container.encode(arrival, forKey: .arrival)
        try container.encode(duration, forKey: .duration)
        try container.encode(journeyMode, forKey: .journeyMode)
        try container.encode(stops, forKey: .stops)
        try container.encode(carriers, forKey: .carriers)
        try container.encode(operatingCarriers, forKey: .operatingCarriers)
        try container.encode(directionality, forKey: .directionality)
        try container.encode(flightNumbers, forKey: .flightNumbers)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        segmentIds = try container.decode([Int].self, forKey: .segmentIds)
        originStation = try container.decode(Int.self, forKey: .originStation)
        destinationStation = try container.decode(Int.self, forKey: .destinationStation)
        departure = try container.decode(Date.self, forKey: .departure)
        arrival = try container.decode(Date.self, forKey: .arrival)
        duration = try container.decode(Int.self, forKey: .duration)
        journeyMode = try container.decode(String.self, forKey: .journeyMode)
        stops = try container.decode([Int].self, forKey: .stops)
        carriers = try container.decode([Int].self, forKey: .carriers)
        operatingCarriers = try container.decode([Int].self, forKey: .operatingCarriers)
        directionality = try container.decode(String.self, forKey: .directionality)
        flightNumbers = try container.decode([SKLiveFlightNumberDataModel].self, forKey: .flightNumbers)
    }
}

// MARK:-

struct SKLiveFlightNumberDataModel: Codable {
    
    var flightNumber: String
    var carrierId: Int
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case flightNumber = "FlightNumber"
        case carrierId = "CarrierId"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(flightNumber, forKey: .flightNumber)
        try container.encode(carrierId, forKey: .carrierId)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flightNumber = try container.decode(String.self, forKey: .flightNumber)
        carrierId = try container.decode(Int.self, forKey: .carrierId)
    }
}

// MARK:-

struct SKLiveSegmentDataModel: Codable {
    
    var id: Int
    var originStation: Int
    var destinationStation: Int
    var departureDateTime: Date
    var arrivalDateTime: Date
    var carrier: Int
    var operatingCarrier: Int
    var duration: Int
    var flightNumber: String
    var journeyMode: String
    var directionality: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case originStation = "OriginStation"
        case destinationStation = "DestinationStation"
        case departureDateTime = "DepartureDateTime"
        case arrivalDateTime = "ArrivalDateTime"
        case carrier = "Carrier"
        case operatingCarrier = "OperatingCarrier"
        case duration = "Duration"
        case flightNumber = "FlightNumber"
        case journeyMode = "JourneyMode"
        case directionality = "Directionality"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(originStation, forKey: .originStation)
        try container.encode(destinationStation, forKey: .destinationStation)
        try container.encode(departureDateTime, forKey: .departureDateTime)
        try container.encode(arrivalDateTime, forKey: .arrivalDateTime)
        try container.encode(carrier, forKey: .carrier)
        try container.encode(operatingCarrier, forKey: .operatingCarrier)
        try container.encode(duration, forKey: .duration)
        try container.encode(flightNumber, forKey: .flightNumber)
        try container.encode(journeyMode, forKey: .journeyMode)
        try container.encode(directionality, forKey: .directionality)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        originStation = try container.decode(Int.self, forKey: .originStation)
        destinationStation = try container.decode(Int.self, forKey: .destinationStation)
        departureDateTime = try container.decode(Date.self, forKey: .departureDateTime)
        arrivalDateTime = try container.decode(Date.self, forKey: .arrivalDateTime)
        carrier = try container.decode(Int.self, forKey: .carrier)
        operatingCarrier = try container.decode(Int.self, forKey: .operatingCarrier)
        duration = try container.decode(Int.self, forKey: .duration)
        flightNumber = try container.decode(String.self, forKey: .flightNumber)
        journeyMode = try container.decode(String.self, forKey: .journeyMode)
        directionality = try container.decode(String.self, forKey: .directionality)
    }
}

// MARK:-

struct SKLiveCarrierDataModel: Codable {
    
    var id: Int
    var code: String
    var name: String
    var imageUrl: String
    var displayCode: String?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case code = "Code"
        case name = "Name"
        case imageUrl = "ImageUrl"
        case displayCode = "DisplayCode"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(code, forKey: .code)
        try container.encode(name, forKey: .name)
        try container.encode(imageUrl, forKey: .imageUrl)
        try container.encodeIfPresent(displayCode, forKey: .displayCode)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        code = try container.decode(String.self, forKey: .code)
        name = try container.decode(String.self, forKey: .name)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        displayCode = try container.decodeIfPresent(String.self, forKey: .displayCode)
    }
}

// MARK:-

struct SKLiveAgentDataModel: Codable {
    
    var id: Int
    var name: String
    var imageUrl: String
    var status: String?
    var optimisedForMobile: Bool
    var bookingNumber: String?
    var type: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case imageUrl = "ImageUrl"
        case status = "Status"
        case optimisedForMobile = "OptimisedForMobile"
        case bookingNumber = "BookingNumber"
        case type = "Type"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(imageUrl, forKey: .imageUrl)
        try container.encodeIfPresent(status, forKey: .status)
        try container.encode(optimisedForMobile, forKey: .optimisedForMobile)
        try container.encodeIfPresent(bookingNumber, forKey: .bookingNumber)
        try container.encode(type, forKey: .type)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        optimisedForMobile = try container.decode(Bool.self, forKey: .optimisedForMobile)
        bookingNumber = try container.decodeIfPresent(String.self, forKey: .bookingNumber)
        type = try container.decode(String.self, forKey: .type)
    }
}

// MARK:-

struct SKLivePlaceDataModel: Codable {
    
    var id: Int
    var parentId: Int?
    var code: String
    var type: String
    var name: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case parentId = "ParentId"
        case code = "Code"
        case type = "Type"
        case name = "Name"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encodeIfPresent(parentId, forKey: .parentId)
        try container.encode(code, forKey: .code)
        try container.encode(type, forKey: .type)
        try container.encode(name, forKey: .name)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        parentId = try container.decodeIfPresent(Int.self, forKey: .parentId)
        code = try container.decode(String.self, forKey: .code)
        type = try container.decode(String.self, forKey: .type)
        name = try container.decode(String.self, forKey: .name)
    }
}

// MARK:-

struct SKLiveBookingOptionsDataModel: Codable {
    
    var bookingItems: [SKLiveBookingItemDataModel]
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case bookingItems = "BookingItems"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(bookingItems, forKey: .bookingItems)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        bookingItems = try container.decode([SKLiveBookingItemDataModel].self, forKey: .bookingItems)
    }
}

// MARK:-

struct SKLiveBookingItemDataModel: Codable {
    
    var agentID: Int
    var alternativeCurrency: String?
    var alternativePrice: Double
    var deeplink: String
    var price: Double
    var segmentIds: [Int]
    var status: String
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case agentID = "AgentID"
        case alternativeCurrency = "AlternativeCurrency"
        case alternativePrice = "AlternativePrice"
        case deeplink = "Deeplink"
        case price = "Price"
        case segmentIds = "SegmentIds"
        case status = "Status"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(agentID, forKey: .agentID)
        try container.encodeIfPresent(alternativeCurrency, forKey: .alternativeCurrency)
        try container.encode(alternativePrice, forKey: .alternativePrice)
        try container.encode(deeplink, forKey: .deeplink)
        try container.encode(price, forKey: .price)
        try container.encode(segmentIds, forKey: .segmentIds)
        try container.encode(status, forKey: .status)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        agentID = try container.decode(Int.self, forKey: .agentID)
        alternativeCurrency = try container.decodeIfPresent(String.self, forKey: .alternativeCurrency)
        alternativePrice = try container.decode(Double.self, forKey: .alternativePrice)
        deeplink = try container.decode(String.self, forKey: .deeplink)
        price = try container.decode(Double.self, forKey: .price)
        segmentIds = try container.decode([Int].self, forKey: .segmentIds)
        status = try container.decode(String.self, forKey: .status)
    }
}

/*
 
 {
 "AgentID": 4499219,
 "AlternativeCurrency": "GBP",
 "AlternativePrice": 98.89,
 "Deeplink": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=I9spmrlz4bVow9vdJX3j0WMrCxxCUhsNO5J1RF40GM%2fKfTXl0DhNzAsMKYVFEgRZ&url=http%3a%2f%2fwww.apideeplink.com%2ftransport_deeplink%2f4.0%2fUS%2fen-gb%2fUSD%2fxpus%2f2%2f4698.10413.2018-04-03%2c10413.4698.2018-04-04%2fair%2ftrava%2fflights%3fitinerary%3dflight%7c-32356%7c8327%7c13542%7c2018-04-03T18%3a00%7c10413%7c2018-04-03T20%3a20%7c80%2cflight%7c-32356%7c2432%7c10413%7c2018-04-04T08%3a55%7c13771%7c2018-04-04T09%3a10%7c75%26carriers%3d-32356%26operators%3d-32356%2c-32356%26passengers%3d1%26channel%3ddataapi%26cabin_class%3deconomy%26facilitated%3dfalse%26ticket_price%3d136.48%26is_npt%3dfalse%26is_multipart%3dfalse%26client_id%3dskyscanner_b2b%26request_id%3da44078a0-32a2-48e1-87c8-63be822b2665%26deeplink_ids%3deu-central-1.prod_f1aee5dc5ae3dda0a81b765d31f988a8%26commercial_filters%3dfalse%26q_datetime_utc%3d2018-03-05T17%3a04%3a53",
 "Price": 136.48,
 "SegmentIds": [
 1,
 2
 ],
 "Status": "Current"
 }
 
 */


