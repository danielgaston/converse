//
//  SKLiveCreateSessionDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

// MARK: -

// No need for Codable protocol methods. Location and session key will be set later or via init

struct SKLiveCreateSessionDataModel {
    
    static let locationKey: String = "Location"
    public var location: String?
    public var sessionKey: String?
    
    init(location locationURLString: String) {
        location = locationURLString
        extractSessionKey()
    }
    
    mutating func extractSessionKey() {
        // "http://partners.api.skyscanner.net/apiservices/pricing/uk2/v1.0/b8ed5fd66e764f0f9b44ff32827ab1c1_rrsqbjcb_cd8283034710069eed7028483edbcb3e"
        
        let components: [String]? = location?.components(separatedBy: CharacterSet.init(charactersIn: "/"))
        sessionKey = components?.last
    }
}
