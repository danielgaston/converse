//
//  SKLivePollBookingDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

struct SKLivePollBookingDataModel: Codable {
    
    var bookingOptions: SKLiveBookingOptionsDataModel
    var query: SKLiveQueryDataModel
    var segments: [SKLiveSegmentDataModel]?
    var carriers: [SKLiveCarrierDataModel]?
    var places: [SKLivePlaceDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case bookingOptions = "BookingOptions"
        case query = "Query"
        case segments = "Segments"
        case carriers = "Carriers"
        case places = "Places"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(bookingOptions, forKey: .bookingOptions)
        try container.encode(query, forKey: .query)
        try container.encodeIfPresent(segments, forKey: .segments)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        bookingOptions = try container.decode(SKLiveBookingOptionsDataModel.self, forKey: .bookingOptions)
        query = try container.decode(SKLiveQueryDataModel.self, forKey: .query)
        segments = try container.decodeIfPresent([SKLiveSegmentDataModel].self, forKey: .segments)
        carriers = try container.decodeIfPresent([SKLiveCarrierDataModel].self, forKey: .carriers)
        places = try container.decodeIfPresent([SKLivePlaceDataModel].self, forKey: .places)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
    
    public func isCompleted() -> Bool {
//        return status == "UpdatesComplete"
        return true
    }
}


/*
 {
 "Segments": [
 {
 "Id": 1,
 "OriginStation": 11235,
 "DestinationStation": 13554,
 "DepartureDateTime": "2017-05-30T19:25:00",
 "ArrivalDateTime": "2017-05-30T20:55:00",
 "Carrier": 881,
 "OperatingCarrier": 881,
 "Duration": 90,
 "FlightNumber": "1463",
 "JourneyMode": "Flight",
 "Directionality": "Outbound"
 },
 ...
 ],
 "BookingOptions": [
 {
 "BookingItems": [
 {
 "AgentID": 4499211,
 "Status": "Current",
 "Price": 83.41,
 "Deeplink": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=jzj5DawL5[...]",
 "SegmentIds": [
 1,
 2
 ]
 }
 ]
 },
 ],
 "Places": [
 {
 "Id": 13554,
 "ParentId": 4698,
 "Code": "LHR",
 "Type": "Airport",
 "Name": "London Heathrow"
 },
 ...
 ],
 "Carriers": [
 {
 "Id": 881,
 "Code": "BA",
 "Name": "British Airways",
 "ImageUrl": "http://s1.apideeplink.com/images/airlines/BA.png"
 }
 ],
 "Query": {
 "Country": "GB",
 "Currency": "GBP",
 "Locale": "en-gb",
 "Adults": 1,
 "Children": 0,
 "Infants": 0,
 "OriginPlace": "2343",
 "DestinationPlace": "13554",
 "OutboundDate": "2017-05-30",
 "InboundDate": "2017-06-02",
 "LocationSchema": "Default",
 "CabinClass": "Economy",
 "GroupPricing": false
 }
 }
 */
