//
//  SKLivePollSessionDataModel.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import UIKit

// MARK: -

struct SKLivePollSessionDataModel: Codable {
    
    var sessionKey: String
    var status: String
    var query: SKLiveQueryDataModel
    var itineraries: [SKLiveItineraryDataModel]?
    var legs: [SKLiveLegDataModel]?
    var segments: [SKLiveSegmentDataModel]?
    var carriers: [SKLiveCarrierDataModel]?
    var agents: [SKLiveAgentDataModel]?
    var places: [SKLivePlaceDataModel]?
    var currencies: [SKCurrencyDataModel]?
    
    // MARK: Codable Protocol Methods (Encodable & Decodable & CodingKey)
    private enum CodingKeys: String, CodingKey {
        case sessionKey = "SessionKey"
        case status = "Status"
        case query = "Query"
        case itineraries = "Itineraries"
        case legs = "Legs"
        case segments = "Segments"
        case carriers = "Carriers"
        case agents = "Agents"
        case places = "Places"
        case currencies = "Currencies"
    }
    
    public func encode (to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(sessionKey, forKey: .sessionKey)
        try container.encode(status, forKey: .status)
        try container.encode(query, forKey: .query)
        try container.encodeIfPresent(itineraries, forKey: .itineraries)
        try container.encodeIfPresent(legs, forKey: .legs)
        try container.encodeIfPresent(segments, forKey: .segments)
        try container.encodeIfPresent(carriers, forKey: .carriers)
        try container.encodeIfPresent(agents, forKey: .agents)
        try container.encodeIfPresent(places, forKey: .places)
        try container.encodeIfPresent(currencies, forKey: .currencies)
    }
    
    public init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        sessionKey = try container.decode(String.self, forKey: .sessionKey)
        status = try container.decode(String.self, forKey: .status)
        query = try container.decode(SKLiveQueryDataModel.self, forKey: .query)
        itineraries = try container.decodeIfPresent([SKLiveItineraryDataModel].self, forKey: .itineraries)
        legs = try container.decodeIfPresent([SKLiveLegDataModel].self, forKey: .legs)
        segments = try container.decodeIfPresent([SKLiveSegmentDataModel].self, forKey: .segments)
        carriers = try container.decodeIfPresent([SKLiveCarrierDataModel].self, forKey: .carriers)
        agents = try container.decodeIfPresent([SKLiveAgentDataModel].self, forKey: .agents)
        places = try container.decodeIfPresent([SKLivePlaceDataModel].self, forKey: .places)
        currencies = try container.decodeIfPresent([SKCurrencyDataModel].self, forKey: .currencies)
    }
    
    public func isCompleted() -> Bool {
        return status == "UpdatesComplete"
    }
}

/*
 {
 "SessionKey": "ab5b948d616e41fb954a4a2f6b8dde1a_ecilpojl_7CAAD17D0CFC34BFDE68DEBFDFD548C7",
 "Query": {
 "Country": "GB",
 "Currency": "GBP",
 "Locale": "en-gb",
 "Adults": 1,
 "Children": 0,
 "Infants": 0,
 "OriginPlace": "2343",
 "DestinationPlace": "13554",
 "OutboundDate": "2017-05-30",
 "InboundDate": "2017-06-02",
 "LocationSchema": "Default",
 "CabinClass": "Economy",
 "GroupPricing": false
 },
 "Status": "UpdatesComplete",
 "Itineraries": [
 {
 "OutboundLegId": "11235-1705301925--32480-0-13554-1705302055",
 "InboundLegId": "13554-1706020700--32480-0-11235-1706020820",
 "PricingOptions": [
 {
 "Agents": [
 4499211
 ],
 "QuoteAgeInMinutes": 0,
 "Price": 83.41,
 "DeeplinkUrl": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=jzj5DawL5zJyT%2bnfe1..."
 },
 ...
 ],
 "BookingDetailsLink": {
 "Uri": "/apiservices/pricing/v1.0/ab5b948d616e41fb954a4a2f6b8dde1a_ecilpojl_7CAAD17D0CFC34BFDE68DEBFDFD548C7/booking",
 "Body": "OutboundLegId=11235-1705301925--32480-0-13554-1705302055&InboundLegId=13554-1706020700--32480-0-11235-1706020820",
 "Method": "PUT"
 }
 },
 ...
 ],
 "Legs": [
 {
 "Id": "11235-1705300650--32302,-32480-1-13554-1705301100",
 "SegmentIds": [
 0,
 1
 ],
 "OriginStation": 11235,
 "DestinationStation": 13554,
 "Departure": "2017-05-30T06:50:00",
 "Arrival": "2017-05-30T11:00:00",
 "Duration": 250,
 "JourneyMode": "Flight",
 "Stops": [
 13880
 ],
 "Carriers": [
 885,
 881
 ],
 "OperatingCarriers": [
 885,
 881
 ],
 "Directionality": "Outbound",
 "FlightNumbers": [
 {
 "FlightNumber": "290",
 "CarrierId": 885
 },
 {
 "FlightNumber": "1389",
 "CarrierId": 881
 }
 ]
 },
 ...
 ],
 "Segments": [
 {
 "Id": 0,
 "OriginStation": 11235,
 "DestinationStation": 13880,
 "DepartureDateTime": "2017-05-30T06:50:00",
 "ArrivalDateTime": "2017-05-30T07:55:00",
 "Carrier": 885,
 "OperatingCarrier": 885,
 "Duration": 65,
 "FlightNumber": "290",
 "JourneyMode": "Flight",
 "Directionality": "Outbound"
 },
 ...
 ],
 "Carriers": [
 {
 "Id": 885,
 "Code": "BE",
 "Name": "Flybe",
 "ImageUrl": "http://s1.apideeplink.com/images/airlines/BE.png",
 "DisplayCode": "BE"
 },
 ...
 ],
 "Agents": [
 {
 "Id": 1963108,
 "Name": "Mytrip",
 "ImageUrl": "http://s1.apideeplink.com/images/websites/at24.png",
 "Status": "UpdatesComplete",
 "OptimisedForMobile": true,
 "BookingNumber": "+448447747881",
 "Type": "TravelAgent"
 },
 ...
 ],
 "Places": [
 {
 "Id": 11235,
 "ParentId": 2343,
 "Code": "EDI",
 "Type": "Airport",
 "Name": "Edinburgh"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "GBP",
 "Symbol": "£",
 "ThousandsSeparator": ",",
 "DecimalSeparator": ".",
 "SymbolOnLeft": true,
 "SpaceBetweenAmountAndSymbol": false,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 },
 ...
 ]
 }
 */
