//
//  SKConverseIntegrationTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

/*!
* @warning Make sure in simulator 'Hardware->Keyboard->Connect Hardware keyboard' is disabled
*/
class SKConverseIntegrationTests: XCTestCase {
    
    var groupOp: GroupBaseOperationPr?
    
    override class func setUp() {
        super.setUp()
        
        CLogStart()
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        if (testManager == nil) {
            testManager = OperationManager.init()
        }
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("************************** START ******************************")
        print("---------------------------------------------------------------")
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("---------------------------------------------------------------")
        print("*************************** END *******************************")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
    }

    
    func test_SK_LocalizationStep1_Locales() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.getLocales(completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let localeDataModel = result.responseObj?.locales.first
            XCTAssertNotNil(localeDataModel, "LocaleDataModel not obtained");

            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(localeDataModel)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_LocalizationStep2_Currencies() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.getCurrencies(completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let currencyDataModel = result.responseObj?.currencies.first
            XCTAssertNotNil(currencyDataModel, "CurrencyDataModel not obtained");
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(currencyDataModel)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_LocalizationStep3_Markets() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.getMarkets(locale: "en-US", completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let countryDataModel = result.responseObj?.countries.first
            XCTAssertNotNil(countryDataModel, "CountryDataModel not obtained");
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(countryDataModel)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_PlacesStep1_AllPlaces() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.getPlaces(country: "AR", currency: "EUR", locale: "ES-ar", query: "bueno", completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let placeDataModel = result.responseObj?.places.first
            XCTAssertNotNil(placeDataModel, "PlaceDataModel not obtained");
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(placeDataModel)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_PlacesStep2_SinglePlace() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.getPlace(country: "AR", currency: "EUR", locale: "ES-ar", placeId: "BUEA-sky", completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let placeDataModel = result.responseObj?.places.first
            XCTAssertNotNil(placeDataModel, "PlaceDataModel not obtained");
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(placeDataModel)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_BrowseStep1_BrowseQuotes() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.browseQuotes(originId: "VIE-sky", destinationId: "BCN-sky", departDate: departDateString(), returnDate: returnDateString(), completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let browseQuotes = result.responseObj
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(browseQuotes)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_BrowseStep2_BrowseRoutes() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.browseRoutes(originId: "VIE-sky", destinationId: "BCN-sky", departDate: departDateString(), returnDate: returnDateString(), completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let browseRoutes = result.responseObj
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(browseRoutes)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_BrowseStep3_BrowseDates() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.browseDates(originId: "VIE-sky", destinationId: "BCN-sky", departDate: departDateString(), returnDate: returnDateString(), completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let browseDates = result.responseObj
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(browseDates)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_BrowseStep4_BrowseGrid() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.browseGrid(originId: "VIE-sky", destinationId: "BCN-sky", departDate: departDateString(), returnDate: returnDateString(), completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            let browseGrid = result.responseObj
            
            let jsonEncoder = JSONEncoder()
            do {
                let jsonData = try jsonEncoder.encode(browseGrid)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Debug JSON String : " + jsonString!)
            } catch { }
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    static var locationKey: String?
    static var sessionKey: String?
    
    func test_SK_LiveStep1_LiveCreateSession() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        
        var params = SKLiveCreateSessionRequestParameters.init()
        params.cabinClass = "Economy"
        params.country = "UK"
        params.currency = "GBP"
        params.locale = "en-GB"
        params.locationSchema = "iata"
        params.originPlace = "EDI"
        params.destinationPlace = "LHR"
        params.outboundDate = departDate()
        params.inboundDate = returnDate()
        params.addKeys()
        
        // <1MB
        groupOp = testManager?.createLiveSession(params: params, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            SKConverseIntegrationTests.locationKey = result.responseObj?.location
            SKConverseIntegrationTests.sessionKey = result.responseObj?.sessionKey
    
            XCTAssertNotNil(SKConverseIntegrationTests.locationKey, "locationKey should not be nil");
            XCTAssertNotNil(SKConverseIntegrationTests.sessionKey, "sessionKey should not be nil");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    static var outboundLegIdKey: String?
    static var inboundLegIdKey: String?
    static var bookingDetails: SKLiveBookingDetailsLinkDataModel?
    
    func test_SK_LiveStep2_LivePollSession() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        
        let params = SKLivePollSessionRequestParameters.init()
        
        guard let location = SKConverseIntegrationTests.locationKey else {
            XCTFail("'locationKey' coming from 'LiveCreateSession' cannot be nil. Execute first 'LiveCreateSession' request.")
            expectation.fulfill()
            return
        }
        
        // <1MB
        groupOp = testManager?.pollLiveSession(location: location, params: params, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            if result.hasError() || !result.hasData() {
                expectation.fulfill()
            }
            
            if let responseObj = result.responseObj, responseObj.isCompleted() {
                let itinerary = responseObj.itineraries?.first
                SKConverseIntegrationTests.outboundLegIdKey = itinerary?.outboundLegId
                SKConverseIntegrationTests.inboundLegIdKey = itinerary?.inboundLegId
                SKConverseIntegrationTests.bookingDetails = itinerary?.bookingDetailsLink
                
                XCTAssertNotNil(SKConverseIntegrationTests.outboundLegIdKey, "outboundLegId should not be nil")
                XCTAssertNotNil(SKConverseIntegrationTests.inboundLegIdKey, "inboundLegId should not be nil")
                XCTAssertNotNil(SKConverseIntegrationTests.bookingDetails, "bookingDetails should not be nil")
                expectation.fulfill()
            }
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_LiveStep3_LiveCreateBooking() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
                
        guard let bookingDetails = SKConverseIntegrationTests.bookingDetails else {
            XCTFail("'bookingDetails' coming from 'LivePollSession' cannot be nil. Execute first 'LivePollSession' request.")
            expectation.fulfill()
            return
        }
        
        let uri = bookingDetails.uri
        let body = bookingDetails.body
        
        // <1MB
        groupOp = testManager?.createLiveBooking(uri: uri, body: body, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_SK_LiveStep4_LivePollBooking() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        
        let params = SKApiKeyRequestParameters.init()
        
        guard let sessionKey = SKConverseIntegrationTests.sessionKey else {
            XCTFail("'sessionKey' cannot be nil.")
            expectation.fulfill()
            return
        }
        
        guard let outboundLegId = SKConverseIntegrationTests.outboundLegIdKey else {
            XCTFail("'outboundLegId' cannot be nil.")
            expectation.fulfill()
            return
        }
        
        guard let inboundLegId = SKConverseIntegrationTests.inboundLegIdKey else {
            XCTFail("'inboundLegId' cannot be nil.")
            expectation.fulfill()
            return
        }
        
        // <1MB
        groupOp = testManager?.pollLiveBooking(sessionKey: sessionKey, outboundLegId: outboundLegId, inboundLegId: inboundLegId, params: params, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            if result.hasError() || !result.hasData() {
                expectation.fulfill()
            }
            
            if let responseObj = result.responseObj, responseObj.isCompleted() {
                expectation.fulfill()
            }
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:- Performance
    
    func test_performance_cleanCaches() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            cleanCaches()
        }
    }
    
    // MARK:- Helper Methods
    
    private func departDate() -> Date {
        // adds 30 days to today date
        return Date.init().addingTimeInterval(TimeInterval.init(30 * 24 * 60 * 60))
    }
    
    private func returnDate() -> Date {
        // adds 40 days to today date
        return Date.init().addingTimeInterval(TimeInterval.init(40 * 24 * 60 * 60))
    }
    
    private func departDateString() -> String {
        // adds 30 days to today date
        let date = departDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    private func returnDateString() -> String {
        // adds 40 days to today date
        let date = returnDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    private func cleanCaches() {
        let cacheManager = CacheManager.init();
        let caches: [Cache] = cacheManager.cache(forType: .CacheTypeALL)
        for cache in caches {
            cache.wipeNonLockedFiles(callback: nil, on: nil)
        }
    }
    
    func areOperationsDeallocated() -> Bool {
        let totalOpsInQueues: Int = testManager?.currentConcurrentOpsInQueues() ?? 0
        //        in completionBlock the last op might still be running
        print("Current operations in queues: \(totalOpsInQueues)")
        return (totalOpsInQueues <= 1)
    }
}
