//
//  ConverseIntegrationTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//


/*!
 * @warning Make sure in simulator 'Hardware->Keyboard->Connect Hardware keyboard' is disabled
 */
import XCTest
@testable import converse

var testManager: OperationManager?

class ConverseIntegrationTests: XCTestCase {
    
    var groupOp: GroupBaseOperationPr?
    
    override class func setUp() {
        super.setUp()
        
        CLogStart()
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        if (testManager == nil) {
            
            let launchEnvironment = ProcessInfo.processInfo.environment
            let urlSessionMode = launchEnvironment["CONVERSE_URL_SESSION"]
            
            switch urlSessionMode {
            case "custom_default":   // custom default session
                testManager = OperationManager.initWith(defaultConfig: URLSessionConfiguration.default, delegate: nil)
            case "custom_background":   // custom background session
                testManager = OperationManager.initWith(backgroundConfig: URLSessionConfiguration.background(withIdentifier: "CNVBackgroundSession"), delegate: nil)
            default:    // default session
                testManager = OperationManager.init()
            }
        }
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("************************** START ******************************")
        print("---------------------------------------------------------------")
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("---------------------------------------------------------------")
        print("*************************** END *******************************")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
    }
    
    //MARK:- OPERATION MANAGER
    
    func test_operation_manager() {
        cleanCaches()
        
        let manager = OperationManager.init()
        XCTAssertNotNil(manager, "manager should not be nil");
    }
    
    func test_operation_manager_default() {
        cleanCaches()
        
        let manager = OperationManager.initWith(defaultConfig: URLSessionConfiguration.default , delegate: nil)
        XCTAssertNotNil(manager, "manager should not be nil");
    }
    
    func test_operation_manager_background() {
        cleanCaches()
        
        let manager = OperationManager.initWith(backgroundConfig: URLSessionConfiguration.default , delegate: nil)
        XCTAssertNotNil(manager, "manager should not be nil");
    }

    
    //MARK:- URL TESTS
    //MARK:-- Data
    
    func test_data() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.data("https://api.ipify.org?format=json") {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_data_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.data("https://nonexistingurl.com") {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_data_cached() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.data("https://api.ipify.org?format=json", useCache: true, locked: false, progress: nil) {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_data_cached_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.data("https://nonexistingurl.com", useCache: true, locked: false, progress: nil) {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- Upload
    
    // Upload Test Server: http://ptsv2.com/t/converse/post
    // If it fails, perhaps it is necessary first to create a 'converse' toilet in http://ptsv2.com (typing 'converse')
    func test_uploadFile() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFile_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://notExistingURL.com" /*404*/, progress: nil, completion: {[weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
                
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFile_inBackground() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", inBackground: true, progress: nil, completion: {[weak self] (result) in
            
            /// UITest is double checking the background task output is as expected
            XCTAssertNotNil(result.error, "error will be nil. XCTest and background URLSession does not play well together");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadFile_inBackground_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://notExistingURL.com" /*404*/, inBackground: true,  progress: nil, completion: {[weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadData() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let url = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        guard let data = try? Data.init(contentsOf: url) else {
            XCTFail("There is no Upload Data")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileData: data, toURLString: "http://ptsv2.com/t/converse/post", progress: nil, completion: {[weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_uploadData_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        guard let url = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        
        guard let data = try? Data.init(contentsOf: url) else {
            XCTFail("There is no Upload Data")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileData: data, toURLString: "http://notExistingURL.com" /*404*/, progress: nil, completion: {[weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- Download JSON
    
    func test_downloadJSON() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://api.ipify.org?format=json", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadJSON_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://wrongURLforJSON.com", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadJSON_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://api.ipify.org?format=json", useCache: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadJSON_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://wrongURLforJSON.com", useCache: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadJSON_withoutCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://api.ipify.org?format=json", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadJSON_withoutCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.downloadJSON("https://wrongURLforJSON.com", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- Download Image
    
    func test_downloadImage() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://upload.wikimedia.org/wikipedia/commons/9/9c/Blue_Planet_Aquarium_Copenhagen%29.jpg", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadDataImage_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://upload.wikimedia.org/wikipedia/commons/9/9c/Blue_Planet_Aquarium_Copenhagen%29.jpg", useCache: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadDataImage_withoutCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://upload.wikimedia.org/wikipedia/commons/9/9c/Blue_Planet_Aquarium_Copenhagen%29.jpg", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadDataImage_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://thisisawrongurlimage.jpeg", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadDataImage_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://thisisawrongurlimage.jpeg", useCache: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadDataImage_withoutCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadDataImage("https://thisisawrongurlimage.jpeg", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileImage_withCache() {
       cleanCaches()
       let expectation = self.expectation(description: #function)
       
       // ~8.3MB
       groupOp = testManager?.downloadFileImage("http://geocatalog.webservice-energy.org:80/geonetwork/srv/eng/resources.get?uuid=106362a24e637149ff7246a44de25b441228412e&fname=106362a24e637149ff7246a44de25b441228412e.png", useCache: true, progress: nil, completion: { [weak self] (result) in
           
           XCTAssertNil(result.error, "error should be nil");
           XCTAssertNotNil(result.data, "data should not be nil");
           XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
           XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
           
           XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
           
           expectation.fulfill()
       })
       
       self.waitForExpectations(timeout: 90) {[weak self] (error) in
           if error != nil {
               print(error!.localizedDescription)
           }
           
           self?.groupOp?.cancel()
       }
    }
    
    func test_downloadFileImage_withoutCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadFileImage("http://geocatalog.webservice-energy.org:80/geonetwork/srv/eng/resources.get?uuid=106362a24e637149ff7246a44de25b441228412e&fname=106362a24e637149ff7246a44de25b441228412e.png", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileImage_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadFileImage("https://thisisawrongurlimage.jpeg", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileImage_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadFileImage("https://thisisawrongurlimage.jpeg", useCache: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFileImage_withoutCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~8.3MB
        groupOp = testManager?.downloadFileImage("https://thisisawrongurlimage.jpeg", useCache: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- Download File
    
    func test_downloadFile() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", progress: nil, completion: { [weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFile_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("https://wrongURLforFILE.pdf", progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFile_inBackground_notInDoc() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }

    func test_downloadFile_inBackground_notInDoc_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("https://wrongURLforFILE.pdf", inBackground: true, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFile_inBackground_inDoc() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: true, toDocumentsFolder: true, progress: nil, completion: { [weak self] (result) in
            
            /// UITest is double checking the background task output is as expected
            XCTAssertNotNil(result.error, "error will be nil. XCTest and background URLSession does not play well together");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }

    func test_downloadFile_inBackground_inDoc_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("https://wrongURLforFILE.pdf", inBackground: true, toDocumentsFolder: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFile_notInBackground_notInDoc() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }

    func test_downloadFile_notInBackground_notInDoc_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("https://wrongURLforFILE.pdf", inBackground: false, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_downloadFile_notInBackground_inDoc() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: true, progress: nil, completion: { [weak self] (result) in
            
            /// UITest is double checking the background task output is as expected
            XCTAssertNotNil(result.error, "error will be nil. XCTest and background URLSession does not play well together");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 120) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }

    func test_downloadFile_notInBackground_inDoc_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // ~30MB
        groupOp = testManager?.downloadFile("https://wrongURLforFILE.pdf", inBackground: false, toDocumentsFolder: true, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:- HTTP TESTS
    // https://github.com/typicode/jsonplaceholder#how-to
    
    // MARK:-- GET Request
    
    func test_requestGET() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.GET("https://jsonplaceholder.typicode.com/posts", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestGET_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.GET("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestGET_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.GET("https://jsonplaceholder.typicode.com/posts", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestGET_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.GET("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- HEAD Request
    
    func test_requestHEAD() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.HEAD("https://jsonplaceholder.typicode.com/posts", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestHEAD_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.HEAD("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }

    func test_requestHEAD_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.HEAD("https://jsonplaceholder.typicode.com/posts", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestHEAD_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.HEAD("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- POST Request
    
    func test_requestPOST() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_body() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, body: { (multipartFormData) in
            // Left blank intentionally
        }, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_body_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, body: { (multipartFormData) in
            // Left blank intentionally
        }, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_body_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/posts", parameters: params, body: { (multipartFormData) in
            // Left blank intentionally
        }, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPOST_body_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.POST("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, body: { (multipartFormData) in
            // Left blank intentionally
        }, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- PUT Request
    
    func test_requestPUT() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "id", value: "1")
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.PUT("https://jsonplaceholder.typicode.com/posts/1", parameters: params, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPUT_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.PUT("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPUT_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "id", value: "1")
        params.add(key: "title", value: "foo")
        params.add(key: "body", value: "bar")
        params.add(key: "userId", value: "1")
        
        // <1MB
        groupOp = testManager?.PUT("https://jsonplaceholder.typicode.com/posts/1", parameters: params, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPUT_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.PUT("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    
    // MARK:-- PATCH Request
    
    func test_requestPATCH() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        
        // <1MB
        groupOp = testManager?.PATCH("https://jsonplaceholder.typicode.com/posts/1", parameters: params, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPATCH_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.PATCH("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPATCH_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        var params = NetworkRequestParameters.init()
        params.add(key: "title", value: "foo")
        
        // <1MB
        groupOp = testManager?.PATCH("https://jsonplaceholder.typicode.com/posts/1", parameters: params, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestPATCH_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        // <1MB
        groupOp = testManager?.PATCH("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:-- DELETE Request
    
    func test_requestDELETE() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.DELETE("https://jsonplaceholder.typicode.com/posts/1", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestDELETE_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.DELETE("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestDELETE_withCache() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.DELETE("https://jsonplaceholder.typicode.com/posts/1", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func test_requestDELETE_withCache_withFailure() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        // <1MB
        groupOp = testManager?.DELETE("https://jsonplaceholder.typicode.com/WRONG_PATH", parameters: nil, useCache: true, progress: nil, completion: {[weak self] (result) in
            XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    // MARK:- Concurrent Operations
    
    func test_concurrentOperations() {
        let concurrentOperations = 5
        let expectation = self.expectation(description: #function)
        // number of times fulfill must be called before the expectation is completely fulfilled
        expectation.expectedFulfillmentCount = concurrentOperations
        
        var ops: [BaseOperation] = []
        
        for _ in 0..<concurrentOperations {
            ops.append(BlockBaseOperation.init(withBlock: { block in
                print("This is a test completion block")
                expectation.fulfill() }))
        }
        
        groupOp = testManager?.runConcurrentOperations(operations: ops)
        self.waitForExpectations(timeout: 30) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    
    // MARK:- Cancel Operations
    
    /*!
     * @brief Cancel Consecutive Download File operations sharing equivalent APIRequest.
     * @discussion Triggers several DownloadFile tasks in a row. When adding the taks to the queue, it cancels any equivalent request that might be running in the system. Last operation should be the only that gets fully executed.
     * @warning Do note rely on cancelling when dealing with Background Queue (Queues defined in the corresponding GroupOperation)
     */
    
    typealias downloadExpectationHelper = (expectationId: String, expectation: XCTestExpectation)
    func test_cancelDownloadFileOperations() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        let timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(cancelDownloadFileOperationsHelper), userInfo: downloadExpectationHelper(expectation.description, expectation), repeats: true)
        
        let delayInSeconds: Double = 4
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
            timer.invalidate()
        }
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func cancelDownloadFileOperationsHelper(timer: Timer) {
        
        let expectation = (timer.userInfo as! downloadExpectationHelper).expectation
        
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: false, progress: nil, completion: { [weak self] (result) in
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
    }
    
    /*!
     * @brief Cancel Consecutive Upload File operations sharing equivalent APIRequest.
     * @discussion Triggers several UploadFile tasks in a row. When adding the taks to the queue, it cancels any equivalent request that might be running in the system. Last operation should be the only that gets fully executed.
     * @warning Do note rely on cancelling when dealing with Background Queue (Queues defined in the corresponding GroupOperation)
     */
    
    typealias uploadExpectationHelper = (expectationId: String, expectation: XCTestExpectation)
    func test_cancelUploadFileOperations() {
        cleanCaches()
        let expectation = self.expectation(description: #function)
        
        let timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(cancelDownloadFileOperationsHelper), userInfo: uploadExpectationHelper(expectation.description, expectation), repeats: true)
        
        let delayInSeconds: Double = 4
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
            timer.invalidate()
        }
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func cancelUploadFileOperationsHelper(timer: Timer) {
        
        let expectation = (timer.userInfo as! uploadExpectationHelper).expectation
        
        guard let fileURL = Bundle.init(for: type(of: self)).url(forResource: "converse_test", withExtension: "txt") else {
            XCTFail("There is no Upload Data URL")
            expectation.fulfill()
            return
        }
        
        groupOp = testManager?.upload(fileURL: fileURL, toURLString: "http://ptsv2.com/t/converse/post", progress: nil, completion: {[weak self] (result) in
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNil(result.responseObj, "responseObj must be nil");
            XCTAssertNotNil(result.extraInfo, "extraInfo should not be nil");
            
            XCTAssertTrue(self?.areOperationsDeallocated() ?? false, "operations not deallocated");
            
            expectation.fulfill()
        })
    }
    
    /*!
    * @brief Cancel Very Consecutive Download File operations to later on cancel all of them at once.
    * @discussion Triggers several DownloadFile tasks in a row.
    * @warning Do note rely on cancelling when dealing with Background Queue (Queues defined in the corresponding GroupOperation)
    */
    
    func test_cancelAllOperations() {
        cleanCaches()
        
        let expectation = self.expectation(description: #function)
        
        let timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(cancelAllOperationsHelper), userInfo: nil, repeats: true)
        
        let delayInSeconds: Double = 2
        let waitForCompletionInSeconds: Double = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
            timer.invalidate()
            testManager?.cancelAllRequests()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds + waitForCompletionInSeconds) {
            XCTAssertTrue(self.areOperationsDeallocated(), "operations not deallocated");
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 90) {[weak self] (error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            self?.groupOp?.cancel()
        }
    }
    
    func cancelAllOperationsHelper(timer: Timer) {
                
        // ~30MB
        groupOp = testManager?.downloadFile("http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf", inBackground: false, toDocumentsFolder: false, progress: nil, completion: {(result) in
            // Left blank intentionally
        })
    }
    
    // MARK:- Reset
    
    func test_reset() {
        testManager?.reset(completion: {
            // nothing to be checked
            XCTAssertTrue(true)
        })
    }
    
    // MARK:- Performance
    
    func test_performance_cleanCaches() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            cleanCaches()
        }
    }
    
    // MARK:- Helper Methods
    
    func cleanCaches() {
        let cacheManager = CacheManager.init();
        let caches: [Cache] = cacheManager.cache(forType: .CacheTypeALL)
        for cache in caches {
            cache.wipeNonLockedFiles(callback: nil, on: nil)
        }
    }
    
    func areOperationsDeallocated() -> Bool {
        let totalOpsInQueues: Int = testManager?.currentConcurrentOpsInQueues() ?? 0
        //        in completionBlock the last op might still be running
        print("Current operations in queues: \(totalOpsInQueues)")
        
        // INFO: 'useCache' tests when executed isolated, usually fwith more test, it fails as there is 2 opearations in queue
        return (totalOpsInQueues <= 3)
    }
}
