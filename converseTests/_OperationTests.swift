//
//  _OperationTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _OperationTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: BASE OPERATION
    
    func test_baseOperation_init() {
        
        let op = GenericBaseOperation<AnyOperationResult>.init()

        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_baseOperation_finish() {
        
        let op = GenericBaseOperation<AnyOperationResult>.init()
        
        XCTAssertFalse(op.isFinished, "op should not be finished")
        
        op.start()
        op.finish()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isCancelled, "op should not be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isValid(), "op should be valid")
    }
    
    func test_baseOperation_executing() {
        
        let op = GenericBaseOperation<AnyOperationResult>.init()
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        
        // immediately finishes the op
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertFalse(op.isCancelled, "op should not be finished")
        XCTAssertTrue(op.isExecuting, "op should be executing")
        XCTAssertTrue(op.isValid(), "op should be valid")
    }
    
    func test_baseOperation_asynchronous() {
        
        let op = GenericBaseOperation<AnyOperationResult>.init()

        XCTAssertTrue(op.isAsynchronous, "op should be asynchronous")
    }
    
    func test_baseOperation_cancel() {
        
        let op = GenericBaseOperation<AnyOperationResult>.init()
        
        XCTAssertFalse(op.isCancelled, "op should not be cancelled")
        
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    

    // MARK: ADAPTER BASE OPERATION
    
    func test_adapterOperation_init() {
       
        let fromWrappedOp = WrappedBaseOperation<AnyOperationResult>.init(GenericBaseOperation.init())
        let toWrappedOp = WrappedBaseOperation<AnyOperationResult>.init(GenericBaseOperation.init())
        let op = AdapterBaseOperation.init(fromOperation: fromWrappedOp, toOperation: toWrappedOp)
        
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_adapterOperation_UUID() {
       
        let fromWrappedOp = WrappedBaseOperation<AnyOperationResult>.init(GenericBaseOperation.init())
        let toWrappedOp = WrappedBaseOperation<AnyOperationResult>.init(GenericBaseOperation.init())
        
        let op = AdapterBaseOperation.init(fromOperation: fromWrappedOp, toOperation: toWrappedOp)
        
        _ = op.opUUID()
        let newUuid = NSUUID.init()
        
        op.set(opUUID: newUuid)
        let storedUuid = op.opUUID()
        
        XCTAssertTrue(storedUuid.uuidString == op.opUUID().uuidString, "Operations UUIDs should match")
    }
    
    // MARK: CACHE BASE OPERATION
    
    func test_cacheImageOperation_init() {
       
        let op = CacheImageOperation.init(URLString: "test", operationDelegate: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_cacheFileOperation_init() {
       
        let op = CacheFileOperation.init(URLString: "test", operationDelegate: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_cacheJSONOperation_init() {
       
        let op = CacheJSONOperation.init(URLString: "test", operationDelegate: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_cacheVideoOperation_init() {
       
        let op = CacheVideoOperation.init(URLString: "test", operationDelegate: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    // MARK: NETWORK BASE OPERATION
    
    func test_networkOperation_init() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkOperation_cancel() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkOperation_cancelBeforeStart() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkOperation_restart() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        op.restart()

        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isCancelled, "op should not be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isValid(), "op should be valid")
    }
    
    func test_networkOperation_isValid() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkOperation_error() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkOperation_data() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkOperation_noData() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkOperation_processStart() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkOperation_processExtraInfo() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_processNoExtraInfo() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkOperation_processValidity_nil() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_processValidity_nil_withTask() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_processValidity_URLResponse() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_processValidity_HTTPURLResponse() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_processValidity_URLSessionDataTask() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkOperation_alert() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkOperation_repeatRequest() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        var op: NetworkBaseOperation!
        op = NetworkBaseOperation.init(request: request, operationDelegate: nil, progress: nil, completion: nil)
        
        op.start()
        op.repeatRequest()
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    
    func test_networkOperation_reportResult() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkBaseOperation.init(request: request, operationDelegate: nil, progress: nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    // MARK: NETWORK DATA OPERATION
    
    func test_networkDataOperation_init() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkDataOperation_cancel() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDataOperation_cancelBeforeStart() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDataOperation_isValid() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkDataOperation_error() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkDataOperation_data() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDataOperation_noData() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDataOperation_processStart() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDataOperation_processExtraInfo() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_processNoExtraInfo() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkDataOperation_processValidity_nil() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_processValidity_nil_withTask() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_processValidity_URLResponse() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_processValidity_HTTPURLResponse() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_processValidity_URLSessionDataTask() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDataOperation_alert() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDataOperation_reportResult() {
       
        let request = DataNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDataOperation.init(request: request, operationDelegate: nil, progress: nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    // MARK: NETWORK UPLOAD OPERATION
    
    func test_networkUploadOperation_init() {
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkUploadOperation_init_bg() {
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkUploadOperation_cancel() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkUploadOperation_cancel_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }

    func test_networkUploadOperation_cancelBeforeStart() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkUploadOperation_cancelBeforeStart_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkUploadOperation_isValid() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkUploadOperation_isValid_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkUploadOperation_error() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkUploadOperation_error_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkUploadOperation_data() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_data_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_noData() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_noData_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_processStart() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_processStart_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_processExtraInfo() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processExtraInfo_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processNoExtraInfo() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkUploadOperation_processNoExtraInfo_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkUploadOperation_processValidity_nil() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_nil_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_nil_withTask() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_nil_withTask_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_URLResponse() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_URLResponse_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_HTTPURLResponse() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_HTTPURLResponse_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_URLSessionDataTask() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_processValidity_URLSessionDataTask_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkUploadOperation_alert() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_alert_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkUploadOperation_reportResult() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(request: request, operationDelegate: nil, progress:nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    func test_networkUploadOperation_reportResult_bg() {
       
        let request = UploadNetworkRequest.init(toURL: URL.init(string: "test_networkUpload")!, fromData: Data.init(count: 10), parameters: nil, body: nil)
        let op = NetworkUploadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    // MARK: NETWORK DOWNLOAD OPERATION
    
    func test_networkDownloadOperation_init() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkDownloadOperation_init_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_networkDownloadOperation_cancel() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDownloadOperation_cancel_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDownloadOperation_cancelBeforeStart() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDownloadOperation_cancelBeforeStart_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
    }
    
    func test_networkDownloadOperation_isValid() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkDownloadOperation_isValid_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_networkDownloadOperation_error() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkDownloadOperation_error_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_networkDownloadOperation_data() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_data_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        op.result().data = Data.init(count: 10)
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_noData() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_noData_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        // forces "data = nil" side case
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_processStart() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_processStart_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        // op finishes immediately
        op.start()
        op.processStart()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_processExtraInfo() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processExtraInfo_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processNoExtraInfo() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkDownloadOperation_processNoExtraInfo_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        op.result().extraInfo = nil
        op.processExtraInfo()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNil(op.result().extraInfo, "extraInfo should be nil")
    }
    
    func test_networkDownloadOperation_processValidity_nil() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_nil_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_nil_withTask() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_nil_withTask_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        
        let validity = op.processResponseValidity(response: nil)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_URLResponse() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_URLResponse_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = URLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_HTTPURLResponse() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_HTTPURLResponse_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let response = HTTPURLResponse.init()

        let validity = op.processResponseValidity(response: response)
        
        XCTAssertFalse(validity, "validity should be false")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_URLSessionDataTask() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_processValidity_URLSessionDataTask_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
        
        let urlSession = URLSession.init(configuration: URLSessionConfiguration.default)
        op.task = urlSession.dataTask(with: URL.init(string: "https://api.ipify.org")!)
        let response = HTTPURLResponse.init()
        
        let validity = op.processResponseValidity(response: response)
        
        XCTAssertTrue(validity, "validity should be true")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().extraInfo, "extraInfo must not be nil")
    }
    
    func test_networkDownloadOperation_alert() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_alert_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil, completion: nil)
    
        op.showAuthenticationAlertController(withCompletion: { (authChallengeDisposition, URLCredential) in
            
            XCTAssertTrue(authChallengeDisposition == URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, "it should be cancelAuth as from this UnitTest bundle we cannot access any UIKit Window view")
        })
        
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isValid(), "op should be valid")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
    
    func test_networkDownloadOperation_reportResult() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(request: request, operationDelegate: nil, progress:nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    func test_networkDownloadOperation_reportResult_bg() {
       
        let request = DownloadNetworkRequest.init(withURLString: "https://api.ipify.org?format=json")
        let op = NetworkDownloadOperation.init(backgroundRequest: request, operationDelegate: nil, progress:nil) { (result) in
            
            XCTAssertNotNil(result, "result must not be nil")
        }
    
        op.start()
        op.reportResult()
    }
    
    // MARK: CONVERSION OPERATION
    
    func test_conversionImageOperation_init() {
       
        let op = ConversionImageOperation.init(operationDelegate: nil) { (result) in
           // left blank intentionally
        }
        XCTAssertNotNil(op, "Operations should exist")
    }
    
    func test_conversionImageOperation_isValid() {
       
        let op = ConversionImageOperation.init(operationDelegate: nil) { (result) in
            XCTAssertNil(result.responseObj, "responseObject should be nil")
            XCTAssertFalse(result.hasData(), "has data should be false")
            XCTAssertNil(result.extraInfo, "extraInfo should be nil")
            XCTAssertFalse(result.hasError(), "has error should be false")
        }
        
        // op cancelled immediately
        op.cancel()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertFalse(op.isFinished, "op should not be finished as it has not started")
        XCTAssertTrue(op.isCancelled, "op should be cancelled")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertFalse(op.isValid(), "op should not be valid")
    }
    
    func test_conversionImageOperation_error() {
       
        let op = ConversionImageOperation.init(operationDelegate: nil) { (result) in
            XCTAssertNil(result.responseObj, "responseObject should be nil")
            XCTAssertFalse(result.hasData(), "has data should be false")
            XCTAssertTrue(result.hasError(), "has error should be true")
        }
        op.result().error = NetworkBaseOperationError.emptyURLResponseError
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().error, "error must not be nil")
    }
    
    func test_conversionImageOperation_nodata() {
       
        let op = ConversionImageOperation.init(operationDelegate: nil) { (result) in
            XCTAssertNil(result.responseObj, "responseObject should be nil")
            XCTAssertFalse(result.hasData(), "has data should be false")
            XCTAssertFalse(result.hasError(), "has error should be false")
        }
        op.result().data = nil
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNil(op.result().data, "data must not be nil")
    }
    
    func test_conversionImageOperation_data() {
       
        guard let imageURL = Bundle.init(for: type(of: self)).url(forResource: "converse_image", withExtension: "png") else {
            XCTFail("There is no Upload Data URL")
            return
        }
        
        let imageData = try! Data.init(contentsOf: imageURL, options: .mappedIfSafe)
        
        let op = ConversionImageOperation.init(operationDelegate: nil) { (result) in
            XCTAssertNotNil(result.responseObj, "responseObject should not be nil")
            XCTAssertTrue(result.hasData(), "has data should not be false")
            XCTAssertFalse(result.hasError(), "has error should be false")
        }
        op.result().data = imageData
        
        // op finishes immediately
        op.start()
        
        XCTAssertTrue(op.isReady, "op should be ready")
        XCTAssertTrue(op.isFinished, "op should be finished")
        XCTAssertFalse(op.isExecuting, "op should not be executing")
        XCTAssertNotNil(op.result().data, "data must not be nil")
    }
}
