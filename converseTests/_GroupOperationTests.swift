//
//  _GroupOperationTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _GroupOperationTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // MARK: GROUP BASE OPERATION
    
    func test_groupGeneralOperation_runAdHocWithOperations() {
        
        let op1 = BlockBaseOperation.init { (block) in
            print("OP1")
        }
        let op2 = BlockBaseOperation.init { (block) in
            print("OP2")
        }
        
        let queueController = QueueController()
        let operationController = OperationController()
        
        let groupOp = GroupGeneralOperation.init(queueController: queueController, customOperationController: operationController)
        groupOp.runAdHocWithOperations(operations: [op1, op2])
        
        sleep(1)
        
        XCTAssertTrue(op1.isReady)
        XCTAssertFalse(op1.isCancelled)
        
        XCTAssertTrue(op2.isReady)
        XCTAssertFalse(op2.isCancelled)
    }
    
    func test_groupGeneralOperation_runWithOperations() {
        
        let op1 = BlockBaseOperation.init { (block) in
            print("OP1")
        }
        let op2 = BlockBaseOperation.init { (block) in
            print("OP2")
        }
        
        let queueController = QueueController()
        let operationController = OperationController()
        
        let groupOp = GroupGeneralOperation.init(queueController: queueController, customOperationController: operationController)
        groupOp.runWithOperations(operations: [op1, op2])
        
        sleep(1)
        
        XCTAssertTrue(op1.isReady)
        XCTAssertFalse(op1.isCancelled)
        
        XCTAssertTrue(op2.isReady)
        XCTAssertFalse(op2.isCancelled)
    }
    
    func test_groupGeneralOperation_cancel() {
        
        let op1 = BlockBaseOperation.init { (block) in
            print("OP1")
        }
        let op2 = BlockBaseOperation.init { (block) in
            print("OP2")
        }
        
        let queueController = QueueController()
        let operationController = OperationController()
        
        let groupOp = GroupGeneralOperation.init(queueController: queueController, customOperationController: operationController)
        groupOp.runAdHocWithOperations(operations: [op1, op2])
        groupOp.cancel()
        
        sleep(1)
        
        XCTAssertTrue(op1.isReady)
        XCTAssertTrue(op1.isCancelled)
        
        XCTAssertTrue(op2.isReady)
        XCTAssertTrue(op2.isCancelled)
    }
}
