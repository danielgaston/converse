//
//  _NetworkTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _NetworkTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: NETWORK COMPONENTS

    func test_networkMethod() {
        
        let methodGET = NetworkMethod.init(rawValue: "GET")
        let methodPOST = NetworkMethod.init(rawValue: "POST")
        let methodPUT = NetworkMethod.init(rawValue: "PUT")
        let methodHEAD = NetworkMethod.init(rawValue: "HEAD")
        let methodDELETE = NetworkMethod.init(rawValue: "DELETE")
        let methodPATCH = NetworkMethod.init(rawValue: "PATCH")
        let methodTRACE = NetworkMethod.init(rawValue: "TRACE")
        let methodOPTIONS = NetworkMethod.init(rawValue: "OPTIONS")
        let methodCONNECT = NetworkMethod.init(rawValue: "CONNECT")
        
        XCTAssertTrue(methodGET?.prefersQueryParameters ?? false, "should be true")
        XCTAssertFalse(methodPOST?.prefersQueryParameters ?? true, "should be false")
        XCTAssertFalse(methodPUT?.prefersQueryParameters ?? true, "should be false")
        XCTAssertTrue(methodHEAD?.prefersQueryParameters ?? false, "should be true")
        XCTAssertTrue(methodDELETE?.prefersQueryParameters ?? false, "should be true")
        XCTAssertFalse(methodPATCH?.prefersQueryParameters ?? true, "should be false")
        XCTAssertFalse(methodTRACE?.prefersQueryParameters ?? true, "should be false")
        XCTAssertFalse(methodOPTIONS?.prefersQueryParameters ?? true, "should be false")
        XCTAssertFalse(methodCONNECT?.prefersQueryParameters ?? true, "should be false")
    }
}
