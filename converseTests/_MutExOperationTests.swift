//
//  _MutExOperationTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _MutExOperationTests: XCTestCase {

    var mutexAlertManager: AlertManager!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mutexAlertManager = AlertManager.init()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: MUTUAL EXCLUSIVE OPERATIONS
    
    func test_single_mutex_alert() {

        let title = "Single Alert"
        let message = "Single Alert Op is queued and executed"
        
        var mutExAlertData = MutExAlertData.init(title: title, message: message)
        mutExAlertData.addButton(withTitle: "Close Alert", handler: {
            
        })
        
        mutexAlertManager.alert(withData: mutExAlertData)
        
        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_multiple_mutex_alerts_withWaiting() {
        
        var mutexAlertDatas = [MutExAlertData]()
        let title = "Serial Alert"
        let message = "3 Alert Ops are queued and executed serially"
        
        var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
        mutexAlertData1.addButton(withTitle: "Finish 1st Alert Op, 2nd & 3rd are pending", handler: {
            
        })
        var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
        mutexAlertData2.addButton(withTitle: "Finish 2nd Alert Op, 3rd is pending", handler: {
            
        })
        var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
        mutexAlertData3.addButton(withTitle: "Finish 3rd Alert Op", handler: {
            
        })
        
        mutexAlertDatas.append(mutexAlertData1)
        mutexAlertDatas.append(mutexAlertData2)
        mutexAlertDatas.append(mutexAlertData3)
        
        mutexAlertManager.alerts(withData: mutexAlertDatas)
        
        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_multiple_mutex_alerts_withDiscarding() {
            
        let title = "Serial Mutual Exclusive Alert"
        let message = "3 MutEx Alert Ops are queued, but since one is being executed, the rest are discarded"
        
        var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
        mutexAlertData1.addButton(withTitle: "Finish 1st Alert Op, 2nd & 3rd have been discarded", handler: {
            
        })
        var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
        mutexAlertData2.addButton(withTitle: "it will never be executed", handler: {
            
        })
        var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
        mutexAlertData3.addButton(withTitle: "it will never be executed", handler: {
            
        })
        
        mutexAlertManager.alertSingleInstance(withData: mutexAlertData1)
        mutexAlertManager.alertSingleInstance(withData: mutexAlertData2)
        mutexAlertManager.alertSingleInstance(withData: mutexAlertData3)

        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_single_mutex_sheet() {
        
        let title = "Single Sheet"
        let message = "Single Sheet Op is queued and executed"
        var mutExAlertData = MutExAlertData.init(title: title, message: message)
        
        mutExAlertData.addButton(withTitle: "Close Sheet", handler: {
            
        })
        
        mutexAlertManager.sheet(withData: mutExAlertData)
        
        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_multiple_mutex_sheets_withWaiting() {
    
        var mutexAlertDatas = [MutExAlertData]()
        let title = "Serial Sheet"
        let message = "3 Sheet Ops are queued and executed serially"
        
        var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
        mutexAlertData1.addButton(withTitle: "Finish 1st Sheet Op, 2nd & 3rd are pending", handler: {
            
        })
        var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
        mutexAlertData2.addButton(withTitle: "Finish 2nd Sheet Op, 3rd is pending", handler: {
            
        })
        var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
        mutexAlertData3.addButton(withTitle: "Finish 3rd Sheet Op", handler: {
            
        })
        
        mutexAlertDatas.append(mutexAlertData1)
        mutexAlertDatas.append(mutexAlertData2)
        mutexAlertDatas.append(mutexAlertData3)
        
        mutexAlertManager.sheets(withData: mutexAlertDatas)
        
        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_multiple_mutex_sheets_withDiscarding() {
        
        let title = "Serial Mutual Exclusive Sheet"
        let message = "3 MutEx Sheet Ops are queued, but since one is being executed, the rest are discarded"
        
        var mutexAlertData1 = MutExAlertData.init(title: title, message: message)
        mutexAlertData1.addButton(withTitle: "Finish 1st Sheet Op, 2nd & 3rd have been discarded", handler: {
            
        })
        var mutexAlertData2 = MutExAlertData.init(title: title, message: message)
        mutexAlertData2.addButton(withTitle: "it will never be executed", handler: {
            
        })
        var mutexAlertData3 = MutExAlertData.init(title: title, message: message)
        mutexAlertData3.addButton(withTitle: "it will never be executed", handler: {
            
        })
        
        mutexAlertManager.sheetSingleInstance(withData: mutexAlertData1)
        mutexAlertManager.sheetSingleInstance(withData: mutexAlertData2)
        mutexAlertManager.sheetSingleInstance(withData: mutexAlertData3)
        
        XCTAssertFalse(mutexAlertManager.internalQueue.isSuspended)
    }
    
    func test_single_mutex_alert_buttons() {

        let title = "Single Alert"
        let message = "Single Alert Op is queued and executed"
        
        var mutExAlertData = MutExAlertData.init(title: title, message: message)
        // add one button
        mutExAlertData.addButton(withTitle: "Close Alert", handler: {
            // left blank intentionally
        })
        // add multiple buttons
        mutExAlertData.addButtons(withTitles: ["OK", "OK2"], handlers: [{},{}])
        // add cancel button
        mutExAlertData.addCancelButton {
            // left blank intentionally
        }
        // add cancel button
        mutExAlertData.addCancelButton(withTitle: "cancel") {
            // left blank intentionally
        }
        // add destructive button
        mutExAlertData.addDestructiveButton(withTitle: "destructive") {
            // left blank intentionally
        }
        
        XCTAssertTrue(mutExAlertData.actionsArray.count == 6)
    }
}
