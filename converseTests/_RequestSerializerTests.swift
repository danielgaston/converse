//
//  _RequestSerializerTests.swift
//  converseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

import XCTest
@testable import converse

class _RequestSerializerTests: XCTestCase {
    
    var serializer: HTTPRequestSerializer!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        serializer = HTTPRequestSerializer()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: HTTPRequestSerializer
    
    func test_HTTPRequestSerializationSerializesPOSTRequestsProperly() {

        var request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        request.httpMethod = "POST"

        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: ["key":"value"])
        let contentType = serializedRequest.allHTTPHeaderFields!["Content-Type"]
    
        XCTAssertNotNil(contentType)
        XCTAssertTrue(contentType == "application/x-www-form-urlencoded")

        XCTAssertNotNil(serializedRequest.httpBody)
        XCTAssertEqual(serializedRequest.httpBody, "key=value".data(using: .utf8))
    }
    
    func test_HTTPRequestSerializationSerializesPOSTRequestsProperlyWhenNoParameterIsProvided() {
        var request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        request.httpMethod = "POST"

        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: nil)
        let contentType = serializedRequest.allHTTPHeaderFields!["Content-Type"]

        XCTAssertNotNil(contentType)
        XCTAssertTrue(contentType == "application/x-www-form-urlencoded")

        XCTAssertNotNil(serializedRequest.httpBody)
        XCTAssertEqual(serializedRequest.httpBody, "".data(using: .utf8))
    }

    func test_HTTPRequestSerialiationSerializesQueryParametersCorrectly() {
        let request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: ["key":"value"])

        XCTAssertTrue(serializedRequest.url?.query == "key=value")
    }

    func test_ThatEmptyDictionaryParametersAreProperlyEncoded() {
        let request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: [:])
        XCTAssertFalse((serializedRequest.url?.absoluteString.hasSuffix("?")) ?? true);
    }

    func test_HTTPRequestSerialiationSerializesURLEncodableQueryParametersCorrectly() {
        let request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: ["key":" :#[]@!$&'()*+,;=/?"])

        XCTAssertTrue(serializedRequest.url?.query == "key=%20%3A%23%5B%5D%40%21%24%26%27%28%29%2A%2B%2C%3B%3D/?")
    }

    func test_HTTPRequestSerialiationSerializesURLEncodedQueryParametersCorrectly() {
        let request = URLRequest.init(url: URL.init(string: "http://example.com")!)
        let serializedRequest = try! serializer.request(bySerializingRequest: request, withParameters: ["key":"%20%21%22%23%24%25%26%27%28%29%2A%2B%2C%2F"])

        XCTAssertTrue(serializedRequest.url?.query == "key=%2520%2521%2522%2523%2524%2525%2526%2527%2528%2529%252A%252B%252C%252F")
    }
}
